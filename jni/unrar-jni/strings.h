#ifndef STRINGS_H
#define STRINGS_H
 
#include <string.h>
 
class Strings
{
	public:

	Strings();
	Strings(const char *str, Strings *parent = NULL);
	~Strings();

	void allocateString(const char *str);
	void setString(const char *str) { m_str = (char*)str; }
	void setNext(Strings *next) { m_next = next; }

	const char* getString() const { return m_str; }
	Strings* getNext() const { return m_next; }

	bool isLast() const { return m_next == NULL; }
	size_t size();

	void addString(const char *str);

	private:

	char *m_str;
	Strings *m_next;
};

#endif
