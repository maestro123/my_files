#include "strings.h"

Strings::Strings()
{
	setString(NULL);
	setNext(NULL);
}

Strings::~Strings()
{
	if (m_next) delete m_next;
	if (m_str) delete [] m_str;
}

void Strings::allocateString(const char *str)
{
	if (str)
	{
		m_str = new char[strlen(str)+1];
		strcpy(m_str, str);
	}
	else
	{
		m_str = NULL;
	}
}

size_t Strings::size()
{
	size_t count = 0;
	Strings *tmp = this;

	while(tmp)
	{
		if (tmp->getString()) ++count;

		tmp = tmp->getNext();
	}

	return count;
}

void Strings::addString(const char *str)
{
	// don't do anything if str is NULL
	if (str == NULL) return;

	if (m_str == NULL)
	{
		allocateString(str);
	}
	else
	{
		Strings *tmp = this;

		while(!tmp->isLast()) tmp = tmp->getNext();

		Strings *next = new Strings();
		next->allocateString(str);

		if (tmp) tmp->setNext(next);
	}
}
