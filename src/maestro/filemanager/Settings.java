package maestro.filemanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by artyom on 21.4.15.
 */
public class Settings {

    private static Context mContext;
    private static SharedPreferences mPreferences;
    private static SharedPreferences.Editor mEditor;
    private static ArrayList<OnSettingChangeListener> mChangeListeners = new ArrayList<>();

    public static void initialize(Context context) {
        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPreferences.edit();
    }

    public interface OnSettingChangeListener {
        void onSettingChange(String pref);
    }

    public enum SORT_TYPE {
        TITLE, DATE, SIZE, TYPE
    }

    public static final String PREF_SHOW_HIDEN_FOLDERS = "show_hiden_folders";
    public static final String PREF_SORT_TYPE = "sort_type";
    public static final String PREF_SORT_INVERSE = "sort_inverse";
    public static final String PREF_FOLDER_FIRST = "folder_first";
    public static final String PREF_FIRST_TIME_CACHE_ADD = "first_time_cache_add";

    private static final String PREF_SAVE_DIRECTORY = "cloud_save_directory_";
    private static final String PREF_SAVE_DIRECTORY_ONE_DRIVE = PREF_SAVE_DIRECTORY + "onedrive";
    private static final String PREF_SAVE_DIRECTORY_DROP_BOX = PREF_SAVE_DIRECTORY + "dropbox";
    private static final String PREF_SAVE_DIRECTORY_YANDEX_DRIVE = PREF_SAVE_DIRECTORY + "yandexdrive";
    private static final String PREF_SAVE_DIRECTORY_BOX = PREF_SAVE_DIRECTORY + "box";

    private static final String DEF_SAVE_DIRECTORY_ONE_DRIVE = "OneDrive";
    private static final String DEF_SAVE_DIRECTORY_DROP_BOX = "DropBox";
    private static final String DEF_SAVE_DIRECTORY_YANDEX_DRIVE = "Yandex";
    private static final String DEF_SAVE_DIRECTORY_BOX = "Box";

    private static File mOneDriveDirectory;
    private static File mDropBoxDirectory;
    private static File mYandexDriveDirectory;
    private static File mBoxDirectory;

    private static AtomicBoolean isShowHideFolders;
    private static AtomicBoolean isSortInverse;
    private static AtomicBoolean isFolderFirst;
    private static AtomicBoolean isFirstTimeCacheAdd;
    private static SORT_TYPE mSortType;

    public static void attachListener(OnSettingChangeListener listener) {
        synchronized (mChangeListeners) {
            mChangeListeners.add(listener);
        }
    }

    public static void detachListener(OnSettingChangeListener listener) {
        synchronized (mChangeListeners) {
            mChangeListeners.remove(listener);
        }
    }

    private static void notifyListeners(String pref) {
        synchronized (mChangeListeners) {
            for (OnSettingChangeListener listener : mChangeListeners) {
                listener.onSettingChange(pref);
            }
        }
    }

    public static boolean isShowHidden() {
        return isShowHideFolders != null ? isShowHideFolders.get()
                : (isShowHideFolders = new AtomicBoolean(mPreferences.getBoolean(PREF_SHOW_HIDEN_FOLDERS, true))).get();
    }

    public static void setShowHideFolders(boolean param) {
        if (isShowHideFolders == null) {
            isShowHidden();
        }
        if (isShowHideFolders.get() != param) {
            isShowHideFolders.set(param);
            mEditor.putBoolean(PREF_SHOW_HIDEN_FOLDERS, param).apply();
            notifyListeners(PREF_SHOW_HIDEN_FOLDERS);
        }
    }

    public static void setOneDriveDirectory(String path) {
        mEditor.putString(PREF_SAVE_DIRECTORY_ONE_DRIVE, path).apply();
        mOneDriveDirectory = new File(path);
    }

    public static void setDropBoxDirectory(String path) {
        mEditor.putString(PREF_SAVE_DIRECTORY_DROP_BOX, path).apply();
        mDropBoxDirectory = new File(path);
    }

    public static void setYandexDriveDirectory(String path) {
        mEditor.putString(PREF_SAVE_DIRECTORY_YANDEX_DRIVE, path).apply();
        mYandexDriveDirectory = new File(path);
    }

    public static void setBoxDirectory(String path) {
        mEditor.putString(PREF_SAVE_DIRECTORY_BOX, path).apply();
        mBoxDirectory = new File(path);
    }

    public static File getOneDriveDirectory() {
        if (mOneDriveDirectory != null && mOneDriveDirectory.exists()) {
            return mOneDriveDirectory;
        }
        return (mOneDriveDirectory = getDefaultDirectoryFile(DEF_SAVE_DIRECTORY_ONE_DRIVE));
    }

    public static File getDropBoxDirectory() {
        if (mDropBoxDirectory != null && mDropBoxDirectory.exists()) {
            return mDropBoxDirectory;
        }
        return (mDropBoxDirectory = getDefaultDirectoryFile(DEF_SAVE_DIRECTORY_DROP_BOX));
    }

    public static File getYandexDriveDirectory() {
        if (mYandexDriveDirectory != null && mYandexDriveDirectory.exists()) {
            return mYandexDriveDirectory;
        }
        return (mYandexDriveDirectory = getDefaultDirectoryFile(DEF_SAVE_DIRECTORY_YANDEX_DRIVE));
    }

    public static File getBoxDirectory() {
        if (mBoxDirectory != null && mBoxDirectory.exists()) {
            return mBoxDirectory;
        }
        return (mBoxDirectory = getDefaultDirectoryFile(DEF_SAVE_DIRECTORY_BOX));
    }

    public static SORT_TYPE getSortType() {
        if (mSortType == null) {
            mSortType = SORT_TYPE.valueOf(mPreferences.getString(PREF_SORT_TYPE, SORT_TYPE.TITLE.name()));
        }
        return mSortType;
    }

    public static void setSortType(SORT_TYPE sortType) {
        if (mSortType == null) {
            getSortType();
        }
        if (mSortType != sortType) {
            mSortType = sortType;
            mEditor.putString(PREF_SORT_TYPE, sortType.name()).apply();
            notifyListeners(PREF_SORT_TYPE);
        }
    }

    public static boolean isSortInverse() {
        if (isSortInverse == null) {
            isSortInverse = new AtomicBoolean(mPreferences.getBoolean(PREF_SORT_INVERSE, false));
        }
        return isSortInverse.get();
    }

    public static void setSortInverse(boolean inverse) {
        if (isSortInverse == null) {
            isSortInverse();
        }
        if (isSortInverse.get() != inverse) {
            isSortInverse.set(inverse);
            mEditor.putBoolean(PREF_SORT_INVERSE, inverse).apply();
            notifyListeners(PREF_SORT_INVERSE);
        }
    }

    public static boolean isFolderFirst() {
        if (isFolderFirst == null) {
            isFolderFirst = new AtomicBoolean(mPreferences.getBoolean(PREF_FOLDER_FIRST, true));
        }
        return isFolderFirst.get();
    }

    public static void setFolderFirst(boolean first) {
        if (isFolderFirst == null) {
            isFolderFirst();
        }
        if (isFolderFirst.get() != first) {
            isFolderFirst.set(first);
            mEditor.putBoolean(PREF_FOLDER_FIRST, first).apply();
            notifyListeners(PREF_FOLDER_FIRST);
        }

    }

    public static boolean isFirstTimeCacheAdd() {
        if (isFirstTimeCacheAdd == null) {
            isFirstTimeCacheAdd = new AtomicBoolean(mPreferences.getBoolean(PREF_FIRST_TIME_CACHE_ADD, true));
        }
        return isFirstTimeCacheAdd.get();
    }

    public static void setFirstTimeCacheAdd(boolean first) {
        if (isFirstTimeCacheAdd == null) {
            isFirstTimeCacheAdd();
        }
        if (isFirstTimeCacheAdd.get() != first) {
            isFirstTimeCacheAdd.set(first);
            mEditor.putBoolean(PREF_FIRST_TIME_CACHE_ADD, first).apply();
            notifyListeners(PREF_FIRST_TIME_CACHE_ADD);
        }
    }

    private static final File getDefaultDirectoryFile(String name) {
        File rootFile = Environment.getExternalStorageDirectory();
        return new File(rootFile, name);
    }

    public static boolean isSortPreference(String pref) {
        return pref.equals(PREF_SORT_TYPE) || pref.equals(PREF_SORT_INVERSE)
                || pref.equals(PREF_SHOW_HIDEN_FOLDERS) || pref.equals(PREF_FOLDER_FIRST);
    }

}
