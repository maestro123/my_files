/*
 * Лицензионное соглашение на использование набора средств разработки
 * «SDK Яндекс.Диска» доступно по адресу: http://legal.yandex.ru/sdk_agreement
 *
 */

package maestro.filemanager.drives.yandexdrive
;

public class UnknownServerWebdavException extends WebdavException {
    public UnknownServerWebdavException(Exception ex) {
        super(ex);
    }

    public UnknownServerWebdavException(String message) {
        super(message);
    }
}
