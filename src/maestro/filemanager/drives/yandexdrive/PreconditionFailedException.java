package maestro.filemanager.drives.yandexdrive
;

public class PreconditionFailedException extends WebdavException {
    public PreconditionFailedException(String message) {
        super(message);
    }
}
