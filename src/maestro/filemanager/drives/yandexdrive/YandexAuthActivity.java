package maestro.filemanager.drives.yandexdrive;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import maestro.filemanager.R;
import maestro.filemanager.drives.DriveConfig;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artyom on 3/22/2015.
 */
public class YandexAuthActivity extends Activity {

    private static final String AUTH_URL = "https://oauth.yandex.ru/authorize?response_type=token&client_id=" + DriveConfig.YANDEX_DRIVE_KEY;

    public static final String TAG = YandexAuthActivity.class.getSimpleName();

    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_auth_view);

        mWebView = (WebView) findViewById(R.id.web_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.animate().alpha(1f).start();
                Log.e(TAG, "onPageStarted = " + url);

                if (url.startsWith("myfiles://")) {
                    Pattern pattern = Pattern.compile("access_token=(.*?)(&|$)");
                    Matcher matcher = pattern.matcher(url);
                    if (matcher.find()) {
                        final String token = matcher.group(1);
                        Log.e(TAG, "token = " + token);
                        if (!TextUtils.isEmpty(token)) {
                            Log.d(TAG, "onLogin: token: " + token);
                            Intent intent = new Intent();
                            intent.putExtra(AccountManager.KEY_AUTHTOKEN, token);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Log.w(TAG, "onRegistrationSuccess: empty token");
                        }
                    } else {
                        Log.w(TAG, "onRegistrationSuccess: token not found in return url");
                    }
                    setResult(RESULT_CANCELED);
                    finish();
                }

                //https://oauth.yandex.ru/verification_code#access_token=b27f7c6b58be4c5495e6d2c1ad9e299a&token_type=bearer&expires_in=31536000

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.e(TAG, "onPageFinished");
                mProgressBar.animate().alpha(0f).start();
            }

        });

        mWebView.loadUrl(AUTH_URL);

    }
}