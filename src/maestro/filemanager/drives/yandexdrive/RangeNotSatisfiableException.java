package maestro.filemanager.drives.yandexdrive
;

public class RangeNotSatisfiableException extends WebdavException {

    public RangeNotSatisfiableException(String msg) {
        super(msg);
    }
}
