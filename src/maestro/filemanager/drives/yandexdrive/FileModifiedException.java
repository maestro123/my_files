package maestro.filemanager.drives.yandexdrive
;

public class FileModifiedException extends WebdavException {

    public FileModifiedException(String msg) {
        super(msg);
    }

}
