package maestro.filemanager.drives.yandexdrive
;

public class WebdavSharingForbiddenException extends WebdavException{

    public WebdavSharingForbiddenException(String detailMessage) {
        super(detailMessage);
    }
}
