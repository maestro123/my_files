/*
 * Лицензионное соглашение на использование набора средств разработки
 * «SDK Яндекс.Диска» доступно по адресу: http://legal.yandex.ru/sdk_agreement
 *
 */

package maestro.filemanager.drives.yandexdrive
;

public class WebdavForbiddenException extends WebdavException {
}
