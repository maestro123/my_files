package maestro.filemanager.drives;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.drives.boxdrive.BoxAuthorizationActivity;
import maestro.filemanager.drives.boxdrive.BoxDriveApi;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.StorageInfo;
import maestro.requestor.Requestor;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * Created by artyom on 29.3.15.
 */
public class BoxDriveManager extends BaseDriveManager {

    public static final String TAG = BoxDriveManager.class.getSimpleName();

    private static volatile BoxDriveManager instance;

    public static synchronized BoxDriveManager getInstance() {
        return instance != null ? instance : (instance = new BoxDriveManager());
    }

    public static final int REQ_CODE_AUTHORIZE = 3004;

    private static final String PARAM_USER_NAME = "access_user_name_box";
    private static final String PARAM_ACCESS_TOKEN = "access_token_box";

    private FileManagerApplication mApplication;
    private Fragment mFragment;
    private String mUserName;
    private BoxDriveApi.TokenHolder mTokenHolder;

    @Override
    public void connect(Fragment fragment) {
        mFragment = fragment;
        mApplication = (FileManagerApplication) fragment.getActivity().getApplication();

        mUserName = PreferenceManager.getDefaultSharedPreferences(mApplication).getString(PARAM_USER_NAME, null);

        final String accessToken = PreferenceManager.getDefaultSharedPreferences(mApplication).getString(PARAM_ACCESS_TOKEN, null);
        if (accessToken == null) {
            fragment.startActivityForResult(new Intent(fragment.getActivity(), BoxAuthorizationActivity.class), REQ_CODE_AUTHORIZE);
        } else {
            try {
                mTokenHolder = new BoxDriveApi.TokenHolder(new JSONObject(accessToken));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_AUTHORIZE) {
            if (data != null && data.hasExtra(BoxAuthorizationActivity.KEY_CODE)) {
                BoxDriveApi.getToken(data.getStringExtra(BoxAuthorizationActivity.KEY_CODE)).setListener(mTokenResponseListener).async();
            } else {
                notifyListeners(MSG_DISCONNECT);
            }
        }
    }

    @Override
    public boolean isConnected() {
        return mTokenHolder != null;
    }

    @Override
    public void getUserInfo() {
        BoxDriveApi.getUserInfo().setListener(new Requestor.ResponseListener() {
            @Override
            public void onResponse(Requestor.Request request, Object object) {
                if (object instanceof StorageInfo) {
                    notifyListeners(MSG_GET_USER_INFO, object);
                } else {
                    notifyListeners(MSG_GET_USER_INFO);
                }
            }

            @Override
            public void onError(Requestor.Request request, Requestor.REQUEST_ERROR error) {
                notifyListeners(MSG_GET_USER_INFO);
            }
        }).async();
    }

    @Override
    public int getRequestCode() {
        return REQ_CODE_AUTHORIZE;
    }

    @Override
    public Object createFolder(String name, String parentId) {
        if (isConnected()) {
            return BoxDriveApi.createFolder(name, parentId);
        } else {
            return DriveError.NOT_CONNECTED;
        }
    }

    @Override
    public Object deleteFolder(String folderId) {
        if (isConnected()) {
            return BoxDriveApi.deleteFolder(folderId);
        } else {
            return DriveError.NOT_CONNECTED;
        }
    }

    @Override
    public Object renameFolder(String folderId, String name) {
        if (isConnected()) {
            return BoxDriveApi.renameFolder(folderId, name);
        } else {
            return DriveError.NOT_CONNECTED;
        }
    }

    @Override
    public Object deleteFile(Object object) {
        if (object instanceof BoxFile || object instanceof String) {
            return BoxDriveApi.deleteFile(object instanceof String ? (String) object : ((BoxFile) object).getId());
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public Object renameFile(Object object) {
        return null;
    }

    @Override
    public boolean isRenameEnable() {
        return false;
    }

    @Override
    public boolean isStreamAvailable() {
        return true;
    }

    @Override
    public void upload(DriveDUObject duObject) {
        BoxDriveApi.uploadFile(duObject);
    }

    @Override
    public void download(DriveDUObject duObject) {
        BoxDriveApi.downloadFile(duObject);
    }

    @Override
    public InputStream getObjectStream(Object object) {
        return null;
    }

    @Override
    public String getName() {
        return "Box drive";
    }

    @Override
    public String getHomeFolderPath() {
        return "0";
    }

    @Override
    public Object loadList(String path) {
        return null;
    }

    public String getToken() {
        return mTokenHolder != null ? mTokenHolder.token() : null;
    }

    public void setTokenHolder(BoxDriveApi.TokenHolder holder) {
        mTokenHolder = holder;
        PreferenceManager.getDefaultSharedPreferences(mApplication).edit().putString(PARAM_ACCESS_TOKEN, mTokenHolder.toString()).apply();
    }

    private final Requestor.ResponseListener mTokenResponseListener = new Requestor.ResponseListener() {
        @Override
        public void onResponse(Requestor.Request request, Object object) {
            if (object instanceof BoxDriveApi.TokenHolder) {
                setTokenHolder((BoxDriveApi.TokenHolder) object);
                notifyListeners(MSG_CONNECT);
            } else {
                notifyListeners(MSG_DISCONNECT);
            }
        }

        @Override
        public void onError(Requestor.Request request, Requestor.REQUEST_ERROR error) {
            notifyListeners(MSG_DISCONNECT);
        }
    };

    public String getRefreshToken() {
        return mTokenHolder.refreshToken();
    }

    @Override
    public void prepareInfo(TextView userNamePlaceHolder, TextView sizePlaceHolder) {

    }

    @Override
    public void prepareIcon(ImageView placeHolder) {

    }

    @Override
    public void cancel() {

    }
}
