package maestro.filemanager.drives.boxdrive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import maestro.filemanager.R;
import maestro.filemanager.drives.DriveConfig;

/**
 * Created by artyom on 29.3.15.
 */
public class BoxAuthorizationActivity extends FragmentActivity {

    public static final String TAG = BoxAuthorizationActivity.class.getSimpleName();
    public static final String KEY_CODE = "key_code";

    private static final String AUTH_URL = "https://app.box.com/api/oauth2/authorize?response_type=code&client_id=" + DriveConfig.BOX_DRIVE_KEY + "&redirect_uri=https://myfiles" + "&state=security_token%" + DriveConfig.BOX_DRIVE_SECRET;

    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_auth_view);

        mWebView = (WebView) findViewById(R.id.web_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.animate().alpha(1f).start();
                if (url.startsWith("https://myfiles")) {
                    if (url.contains("code=")) {
                        final String code = url.substring(url.indexOf("code=") + "code=".length());
                        if (!TextUtils.isEmpty(code)) {
                            Intent intent = new Intent();
                            intent.putExtra(KEY_CODE, code);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Log.e(TAG, "onRegistrationSuccess: empty token");
                        }
                    } else {
                        Log.e(TAG, "onRegistrationSuccess: token not found in return url");
                    }
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.animate().alpha(0f).start();
            }

        });

        mWebView.loadUrl(AUTH_URL);

    }

}