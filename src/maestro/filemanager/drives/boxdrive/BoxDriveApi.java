package maestro.filemanager.drives.boxdrive;

import android.text.TextUtils;
import android.util.Log;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BoxDriveManager;
import maestro.filemanager.drives.DriveConfig;
import maestro.filemanager.drives.DriveDUObject;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.StorageInfo;
import maestro.filemanager.utils.Utils;
import maestro.requestor.ClientGetter;
import maestro.requestor.Requestor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by artyom on 29.3.15.
 */
public class BoxDriveApi {

    public static final String TAG = BoxDriveApi.class.getSimpleName();

    private static final String BASE_URL = "https://api.box.com/oauth2/";
    private static final int BASE_LENGTH = BASE_URL.length();

    public static final String REQUEST_GET_USER_INFO = "https://api.box.com/2.0/users/me";
    public static final String REQUEST_LIST = "https://api.box.com/2.0/folders/%s/items?fields=name,id,created_at,size";
    public static final String REQUEST_THUMBNAIL = "https://api.box.com/2.0/files/%s/thumbnail.png?min_height=%d&min_width=%d";
    public static final String REQUEST_CRETE_FOLDER = "https://api.box.com/2.0/folders";
    public static final String REQUEST_DELETE_FOLDER = "https://api.box.com/2.0/folders/%s?recursive=true";
    public static final String REQUEST_RENAME_FOLDER = "https://api.box.com/2.0/folders/%s";
    public static final String REQUEST_DELETE_FILE = "https://api.box.com/2.0/files/%s";

    private static final String MODE_GET_TOKEN = "token";

    private static final String PARAM_GRANT_TYPE = "grant_type=";
    private static final String GRANT_TYPE_AUTH_CODE = PARAM_GRANT_TYPE + "authorization_code";
    private static final String GRANT_TYPE_REFRESH_TOKEN = PARAM_GRANT_TYPE + "refresh_token";

    private static final String PARAM_ACCESS_CODE = "code=";
    private static final String PARAM_CLIENT_ID = "client_id=";
    private static final String PARAM_CLIENT_SECRET = "client_secret=";
    private static final String PARAM_REFRESH_TOKEN = "refresh_token=";

    private static final String PARAM_ERROR = "error";

    public enum SIZE {
        SMALL, MEDIUM, LARGE, XLARGE;

        public static int sizeOf(SIZE size) {
            return size == SMALL ? 32 : size == MEDIUM ? 64 : size == LARGE ? 128 : 256;
        }

        public static SIZE of(int width) {
            return width < 32 ? SMALL : width < 64 ? MEDIUM : width < 128 ? LARGE : XLARGE;
        }
    }

    private static final ClientGetter mClientGetter = new BoxApiClientGetter();

    public static final class TokenHolder {

        private JSONObject jObj;

        public TokenHolder(JSONObject jsonObject) {
            jObj = jsonObject;
        }

        public String token() {
            return jObj.optString("access_token");
        }

        public String refreshToken() {
            return jObj.optString("refresh_token");
        }

        @Override
        public String toString() {
            return jObj.toString();
        }
    }

    public static final class ErrorHolder {

        private JSONObject jObj;

        public ErrorHolder(JSONObject jsonObject) {
            jObj = jsonObject;
        }

        public String type() {
            return jObj.optString("type");
        }

        public int status() {
            return jObj.optInt("status");
        }

        public String code() {
            return jObj.optString("code");
        }

        public String message() {
            return jObj.optString("message");
        }

        public String error() {
            return jObj.optString("error");
        }

    }

    public static Requestor.Request getToken(String accessCode) {
        return getTokenRequest(new StringBuilder().append(GRANT_TYPE_AUTH_CODE)
                .append("&").append(PARAM_ACCESS_CODE).append(accessCode)
                .append("&").append(PARAM_CLIENT_ID).append(DriveConfig.BOX_DRIVE_KEY)
                .append("&").append(PARAM_CLIENT_SECRET).append(DriveConfig.BOX_DRIVE_SECRET).toString());
    }

    public static Requestor.Request refreshToken(String token) {
        return getTokenRequest(new StringBuilder().append(GRANT_TYPE_REFRESH_TOKEN)
                .append("&").append(PARAM_REFRESH_TOKEN).append(token)
                .append("&").append(PARAM_CLIENT_ID).append(DriveConfig.BOX_DRIVE_KEY)
                .append("&").append(PARAM_CLIENT_SECRET).append(DriveConfig.BOX_DRIVE_SECRET).toString());
    }

    public static Object list(String folderId) {
        Object result = get(String.format(REQUEST_LIST, folderId));
        if (result instanceof String) {
            try {
                JSONObject jsonObject = new JSONObject((String) result);
                if (jsonObject.has(PARAM_ERROR)) {
                    return new ErrorHolder(jsonObject);
                } else {
                    JSONArray entries = jsonObject.optJSONArray("entries");
                    if (entries != null && entries.length() > 0) {
                        final int size = entries.length();
                        ArrayList<BoxFile> files = new ArrayList<>();
                        ArrayList<BoxFile> folders = new ArrayList<>();
                        for (int i = 0; i < size; i++) {
                            BoxFile boxFile = new BoxFile(entries.optJSONObject(i));
                            if (!Settings.isShowHidden() && Utils.isHidden(boxFile.getName()))
                                continue;
                            if (boxFile.isFolder()) {
                                folders.add(boxFile);
                            } else {
                                files.add(boxFile);
                            }
                        }
                        Loaders.sort(files, folders);
                        return folders;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Object createFolder(String name, String parentId) {
        try {
            JSONObject contentObject = new JSONObject();
            contentObject.put("name", name);
            JSONObject parentObject = new JSONObject();
            parentObject.put("id", TextUtils.isEmpty(parentId) ? "0" : parentId);
            contentObject.put("parent", parentObject);
            Object result = postRequest(REQUEST_CRETE_FOLDER, contentObject.toString(), false, false);
            if (result instanceof String) {
                try {
                    JSONObject jsonObject = new JSONObject((String) result);
                    if (jsonObject.has(PARAM_ERROR)) {
                        return new ErrorHolder(jsonObject);
                    } else {
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object deleteFile(String id) {
        try {
            Object result = deleteRequest(String.format(REQUEST_DELETE_FILE, id), false, false);
            if (result instanceof String) {
                try {
                    JSONObject jsonObject = new JSONObject((String) result);
                    if (jsonObject.has(PARAM_ERROR)) {
                        return new ErrorHolder(jsonObject);
                    } else {
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object deleteFolder(String id) {
        try {
            Object result = deleteRequest(String.format(REQUEST_DELETE_FOLDER, id), false, false);
            if (result instanceof String) {
                try {
                    JSONObject jsonObject = new JSONObject((String) result);
                    if (jsonObject.has(PARAM_ERROR)) {
                        return new ErrorHolder(jsonObject);
                    } else {
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object renameFolder(String folderId, String name) {
        try {
            JSONObject contentObject = new JSONObject();
            contentObject.put("name", name);
            Object result = putRequest(String.format(REQUEST_RENAME_FOLDER, folderId), contentObject.toString(), false, false);
            if (result instanceof String) {
                JSONObject jsonObject = new JSONObject((String) result);
                if (jsonObject.has("error")) {
                    return new ErrorHolder(jsonObject);
                } else {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Requestor.Request getTokenRequest(String params) {
        StringBuilder builder = new StringBuilder(BASE_URL);
        builder.append(MODE_GET_TOKEN);

        Requestor.Request request = new Requestor.Request(builder.toString()).setType(Requestor.Request.REQUEST_TYPE.POST);
        request.setPostProcessor(new Requestor.RequestPostProcessor() {
            @Override
            public Object process(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if ((jsonObject.has("type") && jsonObject.getString("type").equals("error")) || jsonObject.has("error")) {
                        return new ErrorHolder(jsonObject);
                    } else if (jsonObject.has("access_token")) {
                        return new TokenHolder(jsonObject);
                    }
                    return jsonObject;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        try {
            StringEntity entity = new StringEntity(params);
            request.setStringEntity(entity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return request.setClientGetter(new BoxApiClientGetter());
    }

    public static InputStream getThumbnail(String id, SIZE size) {
        final int imgSize = SIZE.sizeOf(size);
        Object result = getRequest(String.format(REQUEST_THUMBNAIL, id, imgSize, imgSize), false, true);
        if (result instanceof InputStream) {
            return (InputStream) result;
        }
        return null;
    }

    private static final Object get(String url) {
        return getRequest(url, false, false);
    }

    private static final Object getRequest(String url, boolean refreshed, boolean stream) {
        HttpClient client = mClientGetter.getClient();
        HttpGet get = new HttpGet(url);
        get.addHeader("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());
        try {
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                if (stream) {
                    if (response.containsHeader("Content-Length")) {
                        return new SizeKnownStream(response.getEntity().getContent(), Integer.valueOf(response.getHeaders("Content-Length")[0].getValue()));
                    }
                    return response.getEntity().getContent();
                }
                return Requestor.convertStreamToString(response.getEntity().getContent());
            } else if (response.getStatusLine().getStatusCode() == 401 && !refreshed) {
                if (refreshToken()) {
                    return getRequest(url, true, stream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static final Object postRequest(String url, String content, boolean refreshed, boolean stream) {
        HttpClient client = mClientGetter.getClient();
        HttpPost post = new HttpPost(url);
        try {

            post.setEntity(new StringEntity(content));
            post.addHeader("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());

            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK || response.getStatusLine().getStatusCode() == 201) {
                if (stream)
                    return response.getEntity().getContent();
                return Requestor.convertStreamToString(response.getEntity().getContent());
            } else if (response.getStatusLine().getStatusCode() == 401 && !refreshed) {
                if (refreshToken()) {
                    return postRequest(url, content, true, stream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static final Object deleteRequest(String url, boolean refreshed, boolean stream) {
        HttpClient client = mClientGetter.getClient();
        HttpDelete delete = new HttpDelete(url);
        try {
            delete.addHeader("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());
            HttpResponse response = client.execute(delete);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK || response.getStatusLine().getStatusCode() == 201) {
                if (stream)
                    return response.getEntity().getContent();
                return Requestor.convertStreamToString(response.getEntity().getContent());
            } else if (response.getStatusLine().getStatusCode() == 401 && !refreshed) {
                if (refreshToken()) {
                    return deleteRequest(url, true, stream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static final Object putRequest(String url, String content, boolean refreshed, boolean stream) {
        HttpClient client = mClientGetter.getClient();
        HttpPut put = new HttpPut(url);
        try {
            put.setEntity(new StringEntity(content));
            put.addHeader("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());
            HttpResponse response = client.execute(put);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK || response.getStatusLine().getStatusCode() == 201) {
                if (stream)
                    return response.getEntity().getContent();
                return Requestor.convertStreamToString(response.getEntity().getContent());
            } else if (response.getStatusLine().getStatusCode() == 401 && !refreshed) {
                if (refreshToken()) {
                    return putRequest(url, content, true, stream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    //https://upload.box.com/api/2.0/files/content

    public static Object uploadFile(DriveDUObject duObject) {
        try {
            Object object = duObject.getObject();
            InputStream inputStream = null;
            String name = null;
            String dir = (String) duObject.getTarget();
            if (object instanceof String) {
                File file = new File((String) object);
                inputStream = new FileInputStream(file);
                name = file.getName();
            } else if (object instanceof File) {
                File file = (File) object;
                inputStream = new FileInputStream(file);
                name = file.getName();
            } else if (object instanceof MJavaFile) {
                MJavaFile mJavaFile = (MJavaFile) object;
                inputStream = new FileInputStream(mJavaFile.getOriginalObject());
                name = mJavaFile.getTitle();
            } else if (object instanceof MArchiveEntryFile) {
                MArchiveEntryFile entryFile = (MArchiveEntryFile) object;
                inputStream = (entryFile.getInputStream());
                name = entryFile.getTitle();
            }

            URL url = new URL("https://upload.box.com/api/2.0/files/content");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());

            JSONObject contentObject = new JSONObject();
            contentObject.put("name", name);
            JSONObject parentObject = new JSONObject();
            parentObject.put("id", dir);
            contentObject.put("parent", parentObject);

            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(contentObject.toString() + "\r\n");
            final int uploadSize = inputStream.available();
            int bufferSize = Math.min(uploadSize, 8124);
            final byte[] buffer = new byte[bufferSize];
            int complete = 0;
            while ((inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bufferSize);
                complete += bufferSize;
                bufferSize = Math.min(uploadSize - complete, bufferSize);
                duObject.setProgress(Math.round((float) complete / uploadSize * 100));
            }

            Log.e(TAG, "responseMessage: " + connection.getResponseMessage());
            if (connection.getResponseCode() == 200) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getStream(BoxFile file) {
        return getStream(file.getId());
    }

    public static Object getStream(String id) {
        return getRequest(String.format("https://api.box.com/2.0/files/%s/content", id), false, true);
    }

    public static Object downloadFile(DriveDUObject duObject) {
        try {
            BoxFile boxFile = (BoxFile) duObject.getObject();
            Object result = getRequest(String.format("https://api.box.com/2.0/files/%s/content", boxFile.getId()), false, true);
            if (result instanceof InputStream) {
                Object target = duObject.getTarget();
                File targetFile = null;
                if (target instanceof String) {
                    targetFile = new File((String) target);
                } else if (target instanceof File) {
                    targetFile = (File) target;
                }
                targetFile = new File(targetFile, boxFile.getName());

                BufferedInputStream in = null;
                FileOutputStream fout = null;
                try {
                    in = new BufferedInputStream((InputStream) result);
                    fout = new FileOutputStream(targetFile);
                    final boolean sizeKnown = result instanceof SizeKnownStream;
                    final int downloadSize = sizeKnown ? in.available() : 8124;
                    int complete = 0;

                    final byte data[] = new byte[1024];
                    int count;
                    while ((count = in.read(data, 0, 1024)) != -1) {
                        fout.write(data, 0, count);
                        complete += count;
                        if (sizeKnown) {
                            duObject.setProgress(Math.round((float) complete / downloadSize * 100));
                        }
                    }
                } finally {
                    if (in != null) {
                        in.close();
                    }
                    if (fout != null) {
                        fout.close();
                    }
                }

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class SizeKnownStream extends InputStream {

        private int available;
        private InputStream inputStream;

        public SizeKnownStream(InputStream inputStream, int available) {
            this.inputStream = inputStream;
            this.available = available;
        }

        @Override
        public int read() throws IOException {
            return inputStream.read();
        }

        @Override
        public int read(byte[] buffer) throws IOException {
            return inputStream.read(buffer);
        }

        @Override
        public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
            return inputStream.read(buffer, byteOffset, byteCount);
        }

        @Override
        public int available() throws IOException {
            return available;
        }

        @Override
        public void close() throws IOException {
            inputStream.close();
        }

        @Override
        public synchronized void reset() throws IOException {
            inputStream.reset();
        }

        @Override
        public boolean markSupported() {
            return inputStream.markSupported();
        }

        @Override
        public void mark(int readlimit) {
            inputStream.mark(readlimit);
        }

        @Override
        public long skip(long byteCount) throws IOException {
            return inputStream.skip(byteCount);
        }

    }

    private static boolean refreshToken() throws IOException, JSONException {
        StringBuilder builder = new StringBuilder(BASE_URL).append(MODE_GET_TOKEN);
        StringBuilder entity = new StringBuilder().append(GRANT_TYPE_REFRESH_TOKEN)
                .append("&").append(PARAM_REFRESH_TOKEN).append(BoxDriveManager.getInstance().getRefreshToken())
                .append("&").append(PARAM_CLIENT_ID).append(DriveConfig.BOX_DRIVE_KEY)
                .append("&").append(PARAM_CLIENT_SECRET).append(DriveConfig.BOX_DRIVE_SECRET);

        HttpClient client = mClientGetter.getClient();
        HttpPost get = new HttpPost(builder.toString());
        get.setEntity(new StringEntity(entity.toString()));

        HttpResponse httpResponse = client.execute(get);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            String response = Requestor.convertStreamToString(httpResponse.getEntity().getContent());
            JSONObject jsonObject = new JSONObject(response);
            if ((jsonObject.has("type") && jsonObject.getString("type").equals(PARAM_ERROR)) || jsonObject.has(PARAM_ERROR)) {
                return false;
            } else if (jsonObject.has("access_token")) {
                BoxDriveManager.getInstance().setTokenHolder(new TokenHolder(jsonObject));
                return true;
            }
        }
        return false;

    }

    public static Requestor.Request getUserInfo() {
        return new Requestor.Request(REQUEST_GET_USER_INFO).setType(Requestor.Request.REQUEST_TYPE.GET)
                .setPostProcessor(new Requestor.RequestPostProcessor() {
                    @Override
                    public Object process(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            StorageInfo info = new StorageInfo();
                            info.Name = jsonObject.getString("name");
                            info.UsedSpace = jsonObject.getLong("space_used");
                            info.TotalSpace = jsonObject.getLong("space_amount");
                            return info;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }).setClientGetter(new BoxApiClientGetter());
    }

    public static final class BoxApiClientGetter extends ClientGetter {

        @Override
        public HttpRequestBase buildRequest(HttpRequestBase baseRequest, String format) {
            HttpRequestBase base = super.buildRequest(baseRequest, format);
            base.addHeader("Authorization", "Bearer " + BoxDriveManager.getInstance().getToken());
            return base;
        }
    }

}
