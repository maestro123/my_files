// ------------------------------------------------------------------------------
// Copyright (c) 2014 Microsoft Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
// ------------------------------------------------------------------------------

package maestro.filemanager.drives.onedrive;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.drives.DriveConfig;

import java.util.Arrays;

public class MicrosoftSignInActivity extends Activity {

    private FileManagerApplication mApp;
    private LiveAuthClient mAuthClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = (FileManagerApplication) getApplication();
        mAuthClient = new LiveAuthClient(mApp, DriveConfig.MICROSOFT_CLIENT_ID);
        mApp.setAuthClient(mAuthClient);

        mAuthClient.login(MicrosoftSignInActivity.this, Arrays.asList(DriveConfig.MICROSOFT_SCOPES), new LiveAuthListener() {
            @Override
            public void onAuthComplete(LiveStatus status, LiveConnectSession session, Object userState) {
                if (status == LiveStatus.CONNECTED) {
                    onSuccessLogin(session);
                } else {
                    finish();
                }
            }

            @Override
            public void onAuthError(LiveAuthException exception, Object userState) {
                showToast(exception.getMessage());
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuthClient.initialize(Arrays.asList(DriveConfig.MICROSOFT_SCOPES), new LiveAuthListener() {
            @Override
            public void onAuthError(LiveAuthException exception, Object userState) {
                finish();
            }

            @Override
            public void onAuthComplete(LiveStatus status, LiveConnectSession session, Object userState) {
                if (status == LiveStatus.CONNECTED) {
                    onSuccessLogin(session);
                }
            }
        });
    }

    public void onSuccessLogin(LiveConnectSession session){
        mApp.setSession(session);
        mApp.setConnectClient(new LiveConnectClient(session));
        setResult(RESULT_OK);
        finish();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}