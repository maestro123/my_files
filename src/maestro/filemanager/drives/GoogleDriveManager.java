package maestro.filemanager.drives;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

/**
 * Created by Artyom on 5/26/2015.
 */
public class GoogleDriveManager extends BaseDriveManager {//implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = GoogleDriveManager.class.getSimpleName();

    private static volatile GoogleDriveManager instance;

    public static synchronized GoogleDriveManager getInstance() {
        return instance != null ? instance : (instance = new GoogleDriveManager());
    }

    public static final int REQ_CODE_AUTHORIZE = 3005;

//    private GoogleApiClient mGoogleApiClient;
//    private boolean isOnConnect;
//    private Fragment mFragment;

    @Override
    public void connect(Fragment fragment) {
//        mFragment = fragment;
//        if (mGoogleApiClient == null) {
//            mGoogleApiClient = new GoogleApiClient.Builder(fragment.getActivity().getApplicationContext())
//                    .addApi(Drive.API)
//                    .addApi(Plus.API)
//                    .addScope(Drive.SCOPE_FILE)
//                    .addScope(Plus.SCOPE_PLUS_LOGIN)
//                    .addScope(Plus.SCOPE_PLUS_PROFILE)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this)
//                    .build();
//        }
//        isOnConnect = true;
//        mGoogleApiClient.connect();
    }

    @Override
    public void disconnect() {
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (isOnConnect) {
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public boolean isConnected() {
//        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
        return false;
    }

    @Override
    public void getUserInfo() {

    }

    @Override
    public int getRequestCode() {
        return REQ_CODE_AUTHORIZE;
    }

    @Override
    public Object createFolder(String name, String parentId) {
        return null;
    }

    @Override
    public Object deleteFolder(String folderId) {
        return null;
    }

    @Override
    public Object renameFolder(String folderId, String name) {
        return null;
    }

    @Override
    public Object deleteFile(Object object) {
        return null;
    }

    @Override
    public Object renameFile(Object object) {
        return null;
    }

    @Override
    public boolean isRenameEnable() {
        return false;
    }

    @Override
    public boolean isStreamAvailable() {
        return false;
    }

    @Override
    public void upload(DriveDUObject duObject) {

    }

    @Override
    public void download(DriveDUObject duObject) {

    }

    @Override
    public InputStream getObjectStream(Object object) {
        return null;
    }

    @Override
    public String getName() {
        return "Google drive";
    }

    @Override
    public String getHomeFolderPath() {
        return "/";
    }

    @Override
    public Object loadList(String path) {
//        Log.e(TAG, "loadList");
//        if (!TextUtils.isEmpty(path) && path.equals("/")) {
//            path = Drive.DriveApi.getRootFolder(mGoogleApiClient).getDriveId().encodeToString();
//        }
//        DriveApi.MetadataBufferResult result = Drive.DriveApi.getFolder(mGoogleApiClient, DriveId.decodeFromString(path)).listChildren(mGoogleApiClient).await();
//        Log.e(TAG, "result: " + result.getMetadataBuffer());
//        Metadata[] metadatas = new Metadata[result.getMetadataBuffer().getCount()];
//        for (int i = 0; i < result.getMetadataBuffer().getCount(); i++) {
//            metadatas[i] = result.getMetadataBuffer().get(i);
//        }
//        return metadatas;
        return null;
    }

    @Override
    public void prepareInfo(TextView userNamePlaceHolder, TextView sizePlaceHolder) {

    }

    @Override
    public void prepareIcon(ImageView placeHolder) {

    }

    @Override
    public void cancel() {

    }

//    @Override
//    public void onConnected(Bundle bundle) {
//        isOnConnect = false;
//        notifyListeners(MSG_CONNECT);
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//        if (!result.hasResolution()) {
//            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), mFragment.getActivity(), 0).show();
//            isOnConnect = false;
//            return;
//        }
//        try {
//            result.startResolutionForResult(mFragment.getActivity(), REQ_CODE_AUTHORIZE);
//        } catch (IntentSender.SendIntentException e) {
//            Log.e(TAG, "Exception while starting resolution activity", e);
//        }
//    }
}
