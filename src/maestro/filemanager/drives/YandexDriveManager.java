package maestro.filemanager.drives;

import android.accounts.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.yandexdrive.*;
import maestro.filemanager.files.MArchiveFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.StorageInfo;
import maestro.filemanager.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Artyom on 3/22/2015.
 */
public class YandexDriveManager extends BaseDriveManager {

    private static volatile YandexDriveManager instance;

    public static synchronized YandexDriveManager getInstance() {
        return instance != null ? instance : (instance = new YandexDriveManager());
    }

    public static final String TAG = YandexDriveManager.class.getSimpleName();
    public static final int REQ_CODE_AUTHORIZE = 3003;

    private static final String ACCOUNT_TYPE = "com.yandex";
    private static final String AUTH_URL = "https://oauth.yandex.ru/authorize?response_type=token&client_id=" + DriveConfig.YANDEX_DRIVE_KEY;
    private static final String ACTION_ADD_ACCOUNT = "com.yandex.intent.ADD_ACCOUNT";
    private static final String KEY_CLIENT_SECRET = "clientSecret";
    private static final String PARAM_USER_NAME = "access_user_name_yandex";
    private static final String PARAM_ACCESS_TOKEN = "access_token_yandex";

    private FileManagerApplication mApplication;
    private Fragment mFragment;
    private String mAccessToken;
    private String mUserName = "";
    private boolean infoGettingCanceled = false;

    @Override
    public void connect(Fragment fragment) {
        mFragment = fragment;
        mApplication = (FileManagerApplication) fragment.getActivity().getApplication();

        mUserName = PreferenceManager.getDefaultSharedPreferences(mApplication).getString(PARAM_USER_NAME, null);
        mAccessToken = PreferenceManager.getDefaultSharedPreferences(mApplication).getString(PARAM_ACCESS_TOKEN, null);
        if (mAccessToken == null) {

            AccountManager accountManager = AccountManager.get(fragment.getActivity());
            Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
            Log.e(TAG, "accounts: " + (accounts != null ? accounts.length : null));

            if (accounts != null && accounts.length > 0) {
                // get the first account, for example (you must show the list and allow user to choose)
                Account account = accounts[0];
                Log.e(TAG, "account: " + account);
                getAuthToken(account);
                return;
            }

            Log.e(TAG, "No such accounts: " + ACCOUNT_TYPE);
            for (AuthenticatorDescription authDesc : accountManager.getAuthenticatorTypes()) {
                Log.e(TAG, "authDesc = " + authDesc);
                if (ACCOUNT_TYPE.equals(authDesc.type)) {
                    Log.e(TAG, "Starting " + ACTION_ADD_ACCOUNT);
                    Intent intent = new Intent(ACTION_ADD_ACCOUNT);
                    fragment.startActivityForResult(intent, REQ_CODE_AUTHORIZE);
                    return;
                }
            }
            fragment.startActivityForResult(new Intent(fragment.getActivity(), YandexAuthActivity.class), REQ_CODE_AUTHORIZE);
            // no account manager for com.yandex
//            new AuthDialogFragment().show(fragment.getFragmentManager(), "auth");
//            notifyListeners(MSG_DISCONNECT);
        }
    }

    @Override
    public void disconnect() {
        if (mApplication != null) {
            AccountManager systemAccountManager = AccountManager.get(mApplication);
            systemAccountManager.invalidateAuthToken(ACCOUNT_TYPE, mAccessToken);
        }
        mUserName = null;
        mAccessToken = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getRequestCode()) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Log.e(TAG, "data has token = " + data.hasExtra(AccountManager.KEY_AUTHTOKEN));
                if (data.hasExtra(AccountManager.KEY_AUTHTOKEN)) {
                    mAccessToken = data.getStringExtra(AccountManager.KEY_AUTHTOKEN);
                    PreferenceManager.getDefaultSharedPreferences(mApplication).edit().putString(PARAM_ACCESS_TOKEN, mAccessToken).apply();
                    notifyListeners(MSG_CONNECT);
                } else {
                    Bundle bundle = data.getExtras();
                    String name = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
                    String type = bundle.getString(AccountManager.KEY_ACCOUNT_TYPE);
                    Log.d(TAG, "GET_ACCOUNT_CREDS_INTENT: name=" + name + " type=" + type);
                    Account account = new Account(name, type);
                    getAuthToken(account);
                }
            } else {
                notifyListeners(MSG_DISCONNECT);
            }
        }
    }

    @Override
    public boolean isConnected() {
        return mAccessToken != null;
    }

    @Override
    public void getUserInfo() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    Object result = TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).getDriveQuota();
                    if (result instanceof long[]) {
                        long[] quotas = (long[]) result;
                        StorageInfo info = new StorageInfo();
                        info.Name = mUserName;
                        info.UsedSpace = quotas[0];
                        info.TotalSpace = quotas[1];
                        notifyListeners(MSG_GET_USER_INFO, info);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (FileNotModifiedException e) {
                    e.printStackTrace();
                } catch (RemoteFileNotFoundException e) {
                    e.printStackTrace();
                } catch (RangeNotSatisfiableException e) {
                    e.printStackTrace();
                } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
                    webdavUserNotInitialized.printStackTrace();
                } catch (UnknownServerWebdavException e) {
                    e.printStackTrace();
                } catch (PreconditionFailedException e) {
                    e.printStackTrace();
                } catch (WebdavNotAuthorizedException e) {
                    e.printStackTrace();
                } catch (ServerWebdavException e) {
                    e.printStackTrace();
                } catch (WebdavClientInitException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public int getRequestCode() {
        return REQ_CODE_AUTHORIZE;
    }

    @Override
    public Object createFolder(String name, String parentId) {
        try {
            TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).makeFolder(parentId + "/" + name);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DuplicateFolderException e) {
            e.printStackTrace();
        } catch (IntermediateFolderNotExistException e) {
            e.printStackTrace();
        } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
            webdavUserNotInitialized.printStackTrace();
        } catch (PreconditionFailedException e) {
            e.printStackTrace();
        } catch (WebdavNotAuthorizedException e) {
            e.printStackTrace();
        } catch (ServerWebdavException e) {
            e.printStackTrace();
        } catch (UnsupportedMediaTypeException e) {
            e.printStackTrace();
        } catch (UnknownServerWebdavException e) {
            e.printStackTrace();
        } catch (WebdavClientInitException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Object deleteFolder(String folderId) {
        return deleteFile(folderId);
    }

    @Override
    public Object renameFolder(String folderId, String name) {
        return null;
    }

    @Override
    public Object deleteFile(Object object) {
        try {
            if (object instanceof ListItem || object instanceof String) {
                TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).delete(object instanceof String ? (String) object : ((ListItem) object).getFullPath());
            }
        } catch (WebdavClientInitException e) {
            e.printStackTrace();
        } catch (PreconditionFailedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnknownServerWebdavException e) {
            e.printStackTrace();
        } catch (WebdavFileNotFoundException e) {
            e.printStackTrace();
        } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
            webdavUserNotInitialized.printStackTrace();
        } catch (WebdavNotAuthorizedException e) {
            e.printStackTrace();
        } catch (ServerWebdavException e) {
            e.printStackTrace();
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public Object renameFile(Object object) {
        return null;
    }

    @Override
    public boolean isRenameEnable() {
        return false;
    }

    @Override
    public boolean isStreamAvailable() {
        return false;
    }

    @Override
    public void upload(final DriveDUObject duObject) {
        if (duObject.getObject() instanceof MFile) {
            File file = duObject.getObject() instanceof MJavaFile ? ((MJavaFile) duObject.getObject()).getOriginalObject() :
                    ((MArchiveFile) duObject.getObject()).getOriginalObject();
            try {
                TransportClient.getInstance(mApplication, new Credentials("", mAccessToken))
                        .uploadFile(file.getPath(), (String) duObject.getTarget(), new ProgressListener() {
                            @Override
                            public void updateProgress(long loaded, long total) {
                                duObject.setProgress(loaded, total);
                            }

                            @Override
                            public boolean hasCancelled() {
                                return false;
                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UnknownServerWebdavException e) {
                e.printStackTrace();
            } catch (PreconditionFailedException e) {
                e.printStackTrace();
            } catch (IntermediateFolderNotExistException e) {
                e.printStackTrace();
            } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
                webdavUserNotInitialized.printStackTrace();
            } catch (ServerWebdavException e) {
                e.printStackTrace();
            } catch (WebdavNotAuthorizedException e) {
                e.printStackTrace();
            } catch (WebdavClientInitException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void download(final DriveDUObject duObject) {
        try {
            ListItem driveObject = (ListItem) duObject.getObject();
            Object target = duObject.getTarget();
            File targetFile = null;
            if (target instanceof String) {
                targetFile = new File((String) target);
            } else if (target instanceof File) {
                targetFile = (File) target;
            }
            targetFile = new File(targetFile, driveObject.getName());

            TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).downloadFile(driveObject.getFullPath(), targetFile, new ProgressListener() {
                @Override
                public void updateProgress(long loaded, long total) {
                    Log.e(TAG, "updateProgress");
                    duObject.setProgress(loaded, total);
                }

                @Override
                public boolean hasCancelled() {
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public InputStream getObjectStream(Object object) {
        return null;
    }

    @Override
    public String getName() {
        return "Yandex drive";
    }

    @Override
    public String getHomeFolderPath() {
        return "/";
    }

    @Override
    public Object loadList(String path) {
        try {
            final ArrayList<ListItem> items = new ArrayList<>();
            final ArrayList<ListItem> folders = new ArrayList<>();
            TransportClient mClient = TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken));
            mClient.getList(path, new ListParsingHandler() {

                boolean ignoreFirstItem = true;

                @Override
                public boolean handleItem(ListItem item) {
                    if (ignoreFirstItem) {
                        ignoreFirstItem = false;
                        return false;
                    } else {
                        if (!Settings.isShowHidden() && Utils.isHidden(item.getName()))
                            return false;
                        if (item.isCollection()) {
                            folders.add(item);
                        } else {
                            items.add(item);
                        }
                        return true;
                    }
                }

            });
            Loaders.sort(items, folders);
            return folders;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public InputStream getThumbnailStream(String path, int width, int height) {
        try {
            return TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).getPreviewStream(TransportClient.makePreviewPath(path, width, height));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
            webdavUserNotInitialized.printStackTrace();
        } catch (PreconditionFailedException e) {
            e.printStackTrace();
        } catch (WebdavNotAuthorizedException e) {
            e.printStackTrace();
        } catch (ServerWebdavException e) {
            e.printStackTrace();
        } catch (CancelledDownloadException e) {
            e.printStackTrace();
        } catch (UnknownServerWebdavException e) {
            e.printStackTrace();
        } catch (FileNotModifiedException e) {
            e.printStackTrace();
        } catch (DownloadNoSpaceAvailableException e) {
            e.printStackTrace();
        } catch (RemoteFileNotFoundException e) {
            e.printStackTrace();
        } catch (RangeNotSatisfiableException e) {
            e.printStackTrace();
        } catch (FileModifiedException e) {
            e.printStackTrace();
        } catch (WebdavClientInitException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getAuthToken(Account account) {
        if (mFragment != null) {
            AccountManager systemAccountManager = AccountManager.get(mApplication);
            Bundle options = new Bundle();
            options.putString(KEY_CLIENT_SECRET, DriveConfig.YANDEX_DRIVE_SECRET);
            systemAccountManager.getAuthToken(account, DriveConfig.YANDEX_DRIVE_KEY, options, mFragment.getActivity(), new GetAuthTokenCallback(), null);
        }
    }

    @Override
    public void prepareInfo(final TextView userNamePlaceHolder, final TextView sizePlaceHolder) {
        infoGettingCanceled = false;
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    Object result = TransportClient.getInstance(mApplication, new Credentials(mUserName, mAccessToken)).getDriveQuota();
                    if (result instanceof long[]) {
                        final long[] quotas = (long[]) result;
                        if (!infoGettingCanceled)
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    userNamePlaceHolder.setText(mUserName);
                                    sizePlaceHolder.setText(Utils.readableFileSize(quotas[0]) + "/" + Utils.readableFileSize(quotas[1]));
                                }
                            });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (FileNotModifiedException e) {
                    e.printStackTrace();
                } catch (RemoteFileNotFoundException e) {
                    e.printStackTrace();
                } catch (RangeNotSatisfiableException e) {
                    e.printStackTrace();
                } catch (WebdavUserNotInitialized webdavUserNotInitialized) {
                    webdavUserNotInitialized.printStackTrace();
                } catch (UnknownServerWebdavException e) {
                    e.printStackTrace();
                } catch (PreconditionFailedException e) {
                    e.printStackTrace();
                } catch (WebdavNotAuthorizedException e) {
                    e.printStackTrace();
                } catch (ServerWebdavException e) {
                    e.printStackTrace();
                } catch (WebdavClientInitException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void prepareIcon(ImageView placeHolder) {

    }

    @Override
    public void cancel() {
        infoGettingCanceled = true;
    }

    private class GetAuthTokenCallback implements AccountManagerCallback<Bundle> {
        public void run(AccountManagerFuture<Bundle> result) {
            Log.e(TAG, "run");
            try {
                Bundle bundle = result.getResult();
                Log.e(TAG, "bundle: " + bundle);

                String message = (String) bundle.get(AccountManager.KEY_ERROR_MESSAGE);
                if (message != null) {
                    Toast.makeText(mApplication, message, Toast.LENGTH_LONG).show();
                }

                Intent intent = (Intent) bundle.get(AccountManager.KEY_INTENT);
                Log.e(TAG, "intent: " + intent);
                if (intent != null) {
                    // User input required
                    mFragment.startActivityForResult(intent, REQ_CODE_AUTHORIZE);
                } else {
                    mUserName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
                    mAccessToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                    Log.e(TAG, "GetAuthTokenCallback: token=" + mAccessToken);
                    PreferenceManager.getDefaultSharedPreferences(mApplication).edit().putString(PARAM_ACCESS_TOKEN, mAccessToken);
                    notifyListeners(MSG_CONNECT);
                }
                return;
            } catch (OperationCanceledException ex) {
                Log.d(TAG, "GetAuthTokenCallback", ex);
                Toast.makeText(mApplication, ex.getMessage(), Toast.LENGTH_LONG).show();
            } catch (AuthenticatorException ex) {
                Log.d(TAG, "GetAuthTokenCallback", ex);
                Toast.makeText(mApplication, ex.getMessage(), Toast.LENGTH_LONG).show();
            } catch (IOException ex) {
                Log.d(TAG, "GetAuthTokenCallback", ex);
                Toast.makeText(mApplication, ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            notifyListeners(MSG_DISCONNECT);
        }
    }

    public static class AuthDialogFragment extends DialogFragment {

        public AuthDialogFragment() {
            super();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.example_auth_title)
                    .setMessage(R.string.example_auth_message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AUTH_URL)));
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    })
                    .create();
        }
    }

}