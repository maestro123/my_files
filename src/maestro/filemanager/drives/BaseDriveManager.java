package maestro.filemanager.drives;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.dropbox.client2.DropboxAPI;
import maestro.filemanager.drives.yandexdrive.ListItem;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.files.onedrive.BaseOneDriveObject;
import maestro.filemanager.utils.IStorageInfoGetter;

import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by artyom on 9/12/14.
 */
public abstract class BaseDriveManager implements IStorageInfoGetter {

    public static final String TAG = BaseDriveManager.class.getSimpleName();

    public final int MSG_CONNECT = 1;
    public final int MSG_DISCONNECT = 2;
    public final int MSG_GET_USER_INFO = 3;
    private HashMap<Object, OnDriveManagerEventListener> mListeners = new HashMap<>();
    private final Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e(TAG, "handleMessage = " + msg.what + ", obj = " + msg.obj);
            switch (msg.what) {
                case MSG_CONNECT:
                    synchronized (mListeners) {
                        for (OnDriveManagerEventListener listener : mListeners.values()) {
                            listener.onConnect();
                        }
                    }
                    break;
                case MSG_DISCONNECT:
                    synchronized (mListeners) {
                        for (OnDriveManagerEventListener listener : mListeners.values()) {
                            listener.onDisconnet();
                        }
                    }
                    break;
                case MSG_GET_USER_INFO:
                    synchronized (mListeners) {
                        for (OnDriveManagerEventListener listener : mListeners.values()) {
                            listener.onUserInfoTookEvent(msg.obj);
                        }
                    }
                    break;
            }

        }
    };

    public void attachListener(Object key, OnDriveManagerEventListener listener) {
        synchronized (mListeners) {
            mListeners.remove(key);
            mListeners.put(key, listener);
        }
    }

    public void detachListener(Object key) {
        synchronized (mListeners) {
            mListeners.remove(key);
        }
    }

    protected void notifyListeners(int msg, Object object) {
        uiHandler.obtainMessage(msg, object).sendToTarget();
    }

    protected void notifyListeners(int msg) {
        uiHandler.sendEmptyMessage(msg);
    }

    public abstract void connect(Fragment fragment);

    public abstract void disconnect();

    public abstract void onActivityResult(int requestCode, int resultCode, Intent data);

    public abstract boolean isConnected();

    public abstract void getUserInfo();

    public abstract int getRequestCode();

    public abstract Object createFolder(String name, String parentId);

    public abstract Object deleteFolder(String folderId);

    public abstract Object renameFolder(String folderId, String name);

    public abstract Object deleteFile(Object object);

    public abstract Object renameFile(Object object);

    public abstract boolean isRenameEnable();

    public abstract boolean isStreamAvailable();

    public abstract void upload(DriveDUObject duObject);

    public abstract void download(DriveDUObject duObject);

    public abstract InputStream getObjectStream(Object object);

    public abstract String getName();

    public void onPause() {
    }

    public void onResume() {
    }

    public abstract String getHomeFolderPath();

    public abstract Object loadList(String path);

    public InputStream getThumbnailStream(String path, int width, int height) {
        return null;
    }

    public enum DriveError {
        NOT_CONNECTED
    }

    /*interfaces*/

    public interface OnDriveManagerEventListener {

        public void onConnect();

        public void onDisconnet();

        public void onUserInfoTookEvent(Object object);

    }

    public static BaseDriveManager get(Object mObject) {
        if (mObject instanceof BaseOneDriveObject) {
            return OneDriveManager.getInstance();
        } else if (mObject instanceof DropboxAPI.Entry) {
            return DropBoxManager.getInstance();
        } else if (mObject instanceof ListItem) {
            return YandexDriveManager.getInstance();
        } else if (mObject instanceof BoxFile) {
            return BoxDriveManager.getInstance();
        }
        return null;
    }

}
