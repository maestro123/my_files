package maestro.filemanager.drives;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.onedrive.*;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.onedrive.BaseOneDriveObject;
import maestro.filemanager.files.onedrive.OneDriveAlbum;
import maestro.filemanager.files.onedrive.OneDriveFolder;
import maestro.filemanager.utils.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Artyom on 3/18/2015.
 */
public class OneDriveManager extends BaseDriveManager {

    public static final String TAG = OneDriveManager.class.getSimpleName();
    public static final int REQ_CODE_AUTHORIZE = 3001;
    private StorageInfo mStorageInfo;

    private static volatile OneDriveManager instance;

    public static synchronized OneDriveManager getInstance() {
        return instance != null ? instance : (instance = new OneDriveManager());
    }

    private FileManagerApplication mApplication;
    private ImageLoadObject mUserIconLoadObject;
    private boolean infoGettingCanceled = false;

    @Override
    public void connect(final Fragment fragment) {
        mApplication = (FileManagerApplication) fragment.getActivity().getApplication();
        if (isConnected()) {
            notifyListeners(MSG_CONNECT);
        } else {
            final LiveAuthClient mAuthClient = new LiveAuthClient(mApplication, DriveConfig.MICROSOFT_CLIENT_ID);
            mApplication.setAuthClient(mAuthClient);
            mAuthClient.initialize(Arrays.asList(DriveConfig.MICROSOFT_SCOPES),
                    new LiveAuthListener() {
                        @Override
                        public void onAuthError(LiveAuthException exception, Object userState) {
                            notifyListeners(MSG_DISCONNECT);
                        }

                        @Override
                        public void onAuthComplete(LiveStatus status,
                                                   LiveConnectSession session, Object userState) {
                            if (session == null) {
                                fragment.startActivityForResult(new Intent(fragment.getActivity(), MicrosoftSignInActivity.class), REQ_CODE_AUTHORIZE);
                            } else if (status == LiveStatus.CONNECTED) {
                                mApplication.setSession(session);
                                mApplication.setConnectClient(new LiveConnectClient(session));
                                notifyListeners(MSG_CONNECT);
//                                ensureAccount();
//                                notifyOnEvent(EVENT.CONNECT);
//                                doOtherDownloads();
//                                doOtherUploads();
                            }
                        }
                    }
            );

        }
    }

    @Override
    public void disconnect() {
        mApplication.getAuthClient().logout(null);
        notifyListeners(MSG_DISCONNECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getRequestCode()) {
            if (resultCode == Activity.RESULT_OK) {
                notifyListeners(MSG_CONNECT);
            } else {
                notifyListeners(MSG_DISCONNECT);
            }
        }
    }

    @Override
    public boolean isConnected() {
        return mApplication != null && mApplication.getAuthClient() != null && mApplication.getConnectClient() != null && mApplication.getSession() != null;
    }

    @Override
    public void getUserInfo() {
        if (isConnected()) {
            mApplication.getConnectClient().getAsync("me",
                    new LiveOperationListener() {
                        @Override
                        public void onError(LiveOperationException exception, LiveOperation operation) {

                        }

                        @Override
                        public void onComplete(LiveOperation operation) {
                            JSONObject result = operation.getResult();
                            Log.e(TAG, "me result: " + result);
                            if (!result.has(JSONKeys.ERROR) && mApplication != null) {
                                if (mStorageInfo == null) {
                                    mStorageInfo = new StorageInfo();
                                }
                                mStorageInfo.Name = result.optString("name");
                                notifyListeners(MSG_GET_USER_INFO, mStorageInfo);
                            }
                        }
                    }
            );
            mApplication.getConnectClient().getAsync("me/picture", new LiveOperationListener() {
                @Override
                public void onComplete(LiveOperation operation) {
                    Log.e(TAG, "complete: " + operation.getResult());
                }

                @Override
                public void onError(LiveOperationException exception, LiveOperation operation) {
                    Log.e(TAG, "onError: " + exception.getMessage());
                }
            });
            mApplication.getConnectClient().getAsync("/me/skydrive/quota", new LiveOperationListener() {
                @Override
                public void onComplete(LiveOperation operation) {

                    JSONObject result = operation.getResult();
                    if (result != null && !result.has(JSONKeys.ERROR)) {
                        if (mStorageInfo == null) {
                            mStorageInfo = new StorageInfo();
                        }
                        mStorageInfo.TotalSpace = result.optLong(JSONKeys.AVAILABLE);
                        mStorageInfo.UsedSpace = result.optLong(JSONKeys.QUOTA);
                        notifyListeners(MSG_GET_USER_INFO, mStorageInfo);
                    } else {
                        notifyListeners(MSG_GET_USER_INFO);
                    }
                }

                @Override
                public void onError(LiveOperationException exception, LiveOperation operation) {
                    notifyListeners(MSG_GET_USER_INFO);
                }
            });
        }
    }

    @Override
    public int getRequestCode() {
        return REQ_CODE_AUTHORIZE;
    }

    @Override
    public Object createFolder(String name, String parentId) {
        return null;
    }

    @Override
    public Object deleteFolder(String folderId) {
        return deleteFile(folderId);
    }

    @Override
    public Object renameFolder(String folderId, String name) {
        return null;
    }

    @Override
    public Object deleteFile(Object object) {
        if (object instanceof BaseOneDriveObject || object instanceof String) {
            try {
                LiveOperation operation = mApplication.getConnectClient().delete(object instanceof String ? (String) object : ((BaseOneDriveObject) object).getId());
                if (!operation.getResult().has(JSONKeys.ERROR)) {
                    return GlobalCache.ACTION_RESULT.OK;
                }
            } catch (LiveOperationException e) {
                e.printStackTrace();
            }
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public Object renameFile(Object object) {
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public boolean isRenameEnable() {
        return false;
    }

    @Override
    public boolean isStreamAvailable() {
        return true;
    }

    @Override
    public void upload(final DriveDUObject duObject) {
        try {
            MFile file = (MFile) duObject.getObject();
            mApplication.getConnectClient().upload((String) duObject.getTarget(), file.getTitle(), new FileInputStream((File) file.getOriginalObject()), new EntityEnclosingApiRequest.UploadProgressListener() {
                @Override
                public void onProgress(long totalBytes, long numBytesWritten) {
                    duObject.setProgress(numBytesWritten, totalBytes);
                }
            });
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(DriveDUObject duObject) {
        try {
            BaseOneDriveObject driveObject = (BaseOneDriveObject) duObject.getObject();
            Object target = duObject.getTarget();
            File targetFile = null;
            if (target instanceof String) {
                targetFile = new File((String) target);
            } else if (target instanceof File) {
                targetFile = (File) target;
            }
            targetFile = new File(targetFile, driveObject.getName());
            LiveDownloadOperation downloadOperation = mApplication.getConnectClient().download(driveObject.getId() + "/content");

            BufferedInputStream in = null;
            FileOutputStream fout = null;
            try {
                in = new BufferedInputStream(downloadOperation.getStream());
                fout = new FileOutputStream(targetFile);
                final int downloadSize = downloadOperation.getContentLength();
                int complete = 0;

                final byte data[] = new byte[1024];
                int count;
                while ((count = in.read(data, 0, 1024)) != -1) {
                    fout.write(data, 0, count);
                    complete += count;
                    duObject.setProgress(Math.round((float) complete / downloadSize * 100));
                }
            } finally {
                if (in != null) {
                    in.close();
                }
                if (fout != null) {
                    fout.close();
                }
            }
            Log.e(TAG, "exists: " + targetFile.exists());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public InputStream getObjectStream(Object object) {
        String path = object instanceof String ? (String) object : object instanceof BaseOneDriveObject ? ((BaseOneDriveObject) object).getId() + "/content" : null;
        if (path != null) {
            try {
                if (path.indexOf("/content") == -1)
                    path += "/content";
                return mApplication.getConnectClient().download(path).getStream();
            } catch (LiveOperationException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return "One drive";
    }

    @Override
    public String getHomeFolderPath() {
        return "me/skydrive";
    }

    @Override
    public Object loadList(String path) {
        if (mApplication == null) {
            return Utils.ERROR.NOT_AUTHORIZED;
        }
        try {
            LiveOperation operation = mApplication.getConnectClient().get(path);
            JSONObject result = operation.getResult();
            if (!result.has(JSONKeys.ERROR)) {
                ArrayList<BaseOneDriveObject> mFolderObjects = new ArrayList<>();
                ArrayList<BaseOneDriveObject> mObjects = new ArrayList<>();
                JSONArray data = result.optJSONArray(JSONKeys.DATA);
                if (data != null && data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        BaseOneDriveObject driveObject = BaseOneDriveObject.create(data.optJSONObject(i));
                        if (driveObject != null && !(!Settings.isShowHidden() && Utils.isHidden(driveObject.getName()))) {
                            if (driveObject instanceof OneDriveFolder || driveObject instanceof OneDriveAlbum) {
                                mFolderObjects.add(driveObject);
                            } else {
                                mObjects.add(driveObject);
                            }
                        }
                    }
                    Loaders.sort(mObjects, mFolderObjects);
                }
                return mFolderObjects;
            }
        } catch (LiveOperationException e) {
            e.printStackTrace();
        }
        return Utils.ERROR.UNKNOWN;
    }

    @Override
    public void prepareInfo(final TextView userNamePlaceHolder, final TextView sizePlaceHolder) {
        infoGettingCanceled = false;

        mApplication.getConnectClient().getAsync("/me/skydrive/quota", new LiveOperationListener() {
            @Override
            public void onComplete(LiveOperation operation) {
                if (!infoGettingCanceled) {
                    JSONObject result = operation.getResult();
                    if (result != null && !result.has(JSONKeys.ERROR)) {
                        final long quota = result.optLong(JSONKeys.QUOTA);
                        final long available = result.optLong(JSONKeys.AVAILABLE);
                        sizePlaceHolder.setText(Utils.readableFileSize(quota - available) + "/" + Utils.readableFileSize(quota));
                    }
                }
            }

            @Override
            public void onError(LiveOperationException exception, LiveOperation operation) {
                notifyListeners(MSG_GET_USER_INFO);
            }
        });

        mApplication.getConnectClient().getAsync("me",
                new LiveOperationListener() {
                    @Override
                    public void onError(LiveOperationException exception, LiveOperation operation) {

                    }

                    @Override
                    public void onComplete(LiveOperation operation) {
                        if (!infoGettingCanceled) {
                            JSONObject result = operation.getResult();
                            if (!result.has(JSONKeys.ERROR) && mApplication != null) {
                                userNamePlaceHolder.setText(result.optString("name"));
                            }
                        }
                    }
                }
        );

    }

    @Override
    public void prepareIcon(final ImageView placeHolder) {
        mApplication.getConnectClient().getAsync("me/picture", new LiveOperationListener() {
            @Override
            public void onComplete(LiveOperation operation) {
                if (!infoGettingCanceled) {
                    mUserIconLoadObject = MIM.by(Utils.MIM_STORAGE_ICON).to(placeHolder, operation.getResult().optString("location")).async();
                }
            }

            @Override
            public void onError(LiveOperationException exception, LiveOperation operation) {
            }
        });
    }

    @Override
    public void cancel() {
        infoGettingCanceled = true;
        if (mUserIconLoadObject != null) {
            mUserIconLoadObject.cancel();
        }
    }

    public class AZComparator implements Comparator<BaseOneDriveObject> {

        @Override
        public int compare(BaseOneDriveObject lhs, BaseOneDriveObject rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    }

}
