package maestro.filemanager.drives;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Artyom on 5/4/2015.
 */
public class DriveDUObject {

    public static final String TAG = DriveDUObject.class.getSimpleName();

    private static final String PARAM_TYPE = "type";

    private static final int MSG_START = 0x00;
    private static final int MSG_PROGRESS = 0x01;
    private static final int MSG_FINISH = 0x02;

    public enum TYPE {
        DOWNLOAD, UPLOAD
    }

    public enum STATE {
        IDLE, PROCESS, FINISH
    }

    private TYPE type;
    private STATE state = STATE.IDLE;
    private BaseDriveManager mDriveManager;
    private Object object;
    private Object target;
    private String key;
    private int progress;
    private ArrayList<DriveDUListener> mListeners = new ArrayList<>();

    public DriveDUObject(TYPE type, BaseDriveManager driveManager, Object object, Object target, String key) {
        this.type = type;
        this.mDriveManager = driveManager;
        this.object = object;
        this.target = target;
        this.key = key;
    }

    public TYPE getType() {
        return type;
    }

    public STATE getState() {
        return state;
    }

    public Object getObject() {
        return object;
    }

    public Object getTarget() {
        return target;
    }

    public String getKey() {
        return key;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void setProgress(long complete, long total) {
        progress = Math.round((float) complete / total * 100);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DriveDUObject) {
            return ((DriveDUObject) o).key.equals(key);
        }
        return super.equals(o);
    }

    public void upload() {
        Thread thread = new ProgressNotifyThread(this);
        thread.start();
        state = STATE.PROCESS;
        mDriveManager.upload(this);
        DriveDUManager.getInstance().remove(this);
        state = STATE.FINISH;
        notify(MSG_FINISH, this);
        thread.interrupt();
    }

    public void download() {
        Thread thread = new ProgressNotifyThread(this);
        thread.start();
        state = STATE.PROCESS;
        mDriveManager.download(this);
        DriveDUManager.getInstance().remove(this);
        state = STATE.FINISH;
        notify(MSG_FINISH, this);
        thread.interrupt();
    }

    public void addListener(DriveDUListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void removeListener(DriveDUListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    public void notify(int msg, Object object) {
        Message message = uiHandler.obtainMessage(msg, object);
        message.getData().putSerializable(PARAM_TYPE, type);
        message.sendToTarget();
    }

    private Handler uiHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TYPE type = (TYPE) msg.getData().getSerializable(PARAM_TYPE);
            switch (msg.what) {
                case MSG_START:
                    synchronized (mListeners) {
                        for (DriveDUListener listener : mListeners) {
                            listener.onDUStart(type);
                        }
                    }
                    break;
                case MSG_PROGRESS:
                    synchronized (mListeners) {
                        Log.e(TAG, "listener size: " + mListeners.size());
                        for (DriveDUListener listener : mListeners) {
                            listener.onDUProgress(type, (Integer) msg.obj);
                        }
                    }
                    break;
                case MSG_FINISH:
                    synchronized (mListeners) {
                        for (DriveDUListener listener : mListeners) {
                            listener.onDUFinish(type, msg.obj);
                        }
                    }
                    break;
            }
        }
    };

    private static final class ProgressNotifyThread extends Thread {

        private DriveDUObject duObject;
        private int prevProgress = -1;

        public ProgressNotifyThread(DriveDUObject object) {
            duObject = object;
        }

        @Override
        public void run() {
            super.run();
            while (!isInterrupted() && duObject.getState() != STATE.FINISH) {
                Log.e(TAG, "notify: " + duObject.getProgress());
                if (prevProgress != duObject.getProgress() && duObject.getProgress() > 0) {
                    prevProgress = duObject.getProgress();
                    duObject.notify(MSG_PROGRESS, new Integer(prevProgress));
                }
                if (!isInterrupted()) {
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public interface DriveDUListener {

        void onDUStart(TYPE type);

        void onDUProgress(TYPE type, int progress);

        void onDUFinish(TYPE type, Object error);

    }

    public static class DriveDUListenerAdapter implements DriveDUListener {
        @Override
        public void onDUStart(TYPE type) {

        }

        @Override
        public void onDUProgress(TYPE type, int progress) {

        }

        @Override
        public void onDUFinish(TYPE type, Object error) {

        }
    }

}
