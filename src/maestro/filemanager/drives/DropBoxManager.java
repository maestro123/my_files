package maestro.filemanager.drives;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.Settings;
import maestro.filemanager.files.MFile;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.StorageInfo;
import maestro.filemanager.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Artyom on 3/21/2015.
 */
public class DropBoxManager extends BaseDriveManager {

    public static final String TAG = DropBoxManager.class.getSimpleName();
    public static final int REQ_CODE_AUTHORIZE = 3002;

    private static volatile DropBoxManager instance;

    public static synchronized DropBoxManager getInstance() {
        return instance != null ? instance : (instance = new DropBoxManager());
    }

    private static final String PARAM_ACCESS_TOKEN = "access_token_drop_box";

    private FileManagerApplication mApplication;
    private DropboxAPI<AndroidAuthSession> mDBApi;
    private String mAccessToken;
    private boolean isOnLogin;
    private boolean infoGettingCanceled;

    @Override
    public void connect(Fragment fragment) {
        mApplication = (FileManagerApplication) fragment.getActivity().getApplication();
        AppKeyPair appKeys = new AppKeyPair(DriveConfig.DROP_BOX_KEY, DriveConfig.DROP_BOX_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        mAccessToken = PreferenceManager.getDefaultSharedPreferences(mApplication).getString(PARAM_ACCESS_TOKEN, null);
        Log.e(TAG, "mAccessToken = " + mAccessToken);
        if (mAccessToken == null) {
            mDBApi.getSession().startOAuth2Authentication(fragment.getActivity());
        } else {
            mDBApi.getSession().setOAuth2AccessToken(mAccessToken);
            notifyListeners(MSG_CONNECT);
        }
        isOnLogin = true;
    }

    @Override
    public void disconnect() {
        if (isConnected()) {
            mDBApi.getSession().unlink();
        }
        if (mApplication != null)
            PreferenceManager.getDefaultSharedPreferences(mApplication).edit().remove(PARAM_ACCESS_TOKEN).apply();
        notifyListeners(MSG_DISCONNECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getRequestCode()) {
            if (resultCode == Activity.RESULT_OK) {
                notifyListeners(MSG_CONNECT);
            } else {
                notifyListeners(MSG_DISCONNECT);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isOnLogin)
            if (mDBApi.getSession().authenticationSuccessful()) {
                try {
                    mDBApi.getSession().finishAuthentication();
                    mAccessToken = mDBApi.getSession().getOAuth2AccessToken();
                    Log.e(TAG, "mAccessToken = " + mAccessToken);
                    PreferenceManager.getDefaultSharedPreferences(mApplication).edit().putString(PARAM_ACCESS_TOKEN, mAccessToken).apply();
                    notifyListeners(MSG_CONNECT);
                    isOnLogin = false;
                } catch (IllegalStateException e) {
                    Log.i("DbAuthLog", "Error authenticating", e);
                    disconnect();
                }
            } else {
                disconnect();
            }
    }

    @Override
    public boolean isConnected() {
        return mAccessToken != null;
    }

    @Override
    public void getUserInfo() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    DropboxAPI.Account account = mDBApi.accountInfo();
                    StorageInfo info = new StorageInfo();
                    info.Name = TextUtils.isEmpty(account.displayName) ? account.email : account.displayName;
                    info.TotalSpace = account.quotaNormal;
                    info.UsedSpace = account.quota;
                    Log.e(TAG, "getUserInfo = " + info);
                    notifyListeners(MSG_GET_USER_INFO, info);
                } catch (Exception e) {
                    e.printStackTrace();
                    notifyListeners(MSG_GET_USER_INFO);
                }
            }
        }.start();
    }

    @Override
    public void prepareInfo(final TextView userNamePlaceHolder, final TextView sizePlaceHolder) {
        infoGettingCanceled = false;
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    final DropboxAPI.Account account = mDBApi.accountInfo();
                    if (!infoGettingCanceled) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                userNamePlaceHolder.setText(account.displayName);
                                sizePlaceHolder.setText(Utils.readableFileSize(account.quota) + "/" + Utils.readableFileSize(account.quotaNormal));
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void prepareIcon(ImageView placeHolder) {

    }

    @Override
    public void cancel() {
        infoGettingCanceled = true;
    }

    @Override
    public int getRequestCode() {
        return REQ_CODE_AUTHORIZE;
    }

    @Override
    public Object createFolder(String name, String parentId) {
        try {
            if (mDBApi.createFolder(parentId + "/" + name) != null) {
                return GlobalCache.ACTION_RESULT.OK;
            }
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public Object deleteFolder(String path) {
        return deleteFile(path);
    }

    @Override
    public Object renameFolder(String folderId, String name) {
        return null;
    }

    @Override
    public Object deleteFile(Object object) {
        if (object instanceof DropboxAPI.Entry || object instanceof String) {
            try {
                mDBApi.delete(object instanceof String ? (String) object : ((DropboxAPI.Entry) object).path);
                return GlobalCache.ACTION_RESULT.OK;
            } catch (DropboxException e) {
                e.printStackTrace();
            }
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public Object renameFile(Object object) {
        return null;
    }

    @Override
    public boolean isRenameEnable() {
        return false;
    }

    @Override
    public boolean isStreamAvailable() {
        return false;
    }

    @Override
    public void upload(final DriveDUObject duObject) {
        try {
            if (duObject.getObject() instanceof MFile) {
                MFile file = (MFile) duObject.getObject();
                mDBApi.putFile((String)duObject.getTarget() + "/" + file.getTitle(), file.getInputStream(), file.getSize(), null, new ProgressListener() {
                    @Override
                    public void onProgress(long l, long l1) {
                        duObject.setProgress(l, l1);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(final DriveDUObject duObject) {
        try {
            DropboxAPI.Entry entryFile = (DropboxAPI.Entry) duObject.getObject();
            Object target = duObject.getTarget();
            File targetFile = null;
            if (target instanceof String) {
                targetFile = new File((String) target);
            } else if (target instanceof File) {
                targetFile = (File) target;
            }
            targetFile = new File(targetFile, entryFile.fileName());
            FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
            mDBApi.getFile(entryFile.path, null, fileOutputStream, new ProgressListener() {
                @Override
                public void onProgress(long complete, long total) {
                    duObject.setProgress(complete, total);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public InputStream getObjectStream(Object object) {
        return null;
    }

    @Override
    public String getName() {
        return "DropBox";
    }

    @Override
    public String getHomeFolderPath() {
        return "/";
    }

    @Override
    public Object loadList(String path) {
        DropboxAPI.Entry entry = null;
        try {
            entry = mDBApi.metadata(path, 10000, null, true, null);
        } catch (DropboxException e) {
            Log.e(TAG, "e = " + e);
            return null;
        }
        ArrayList<DropboxAPI.Entry> files = new ArrayList<DropboxAPI.Entry>();
        ArrayList<DropboxAPI.Entry> folders = new ArrayList<>();
        for (DropboxAPI.Entry ent : entry.contents) {
            if (!Settings.isShowHidden() && Utils.isHidden(ent.fileName()))
                continue;
            if (ent.isDir) {
                folders.add(ent);
            } else {
                files.add(ent);
            }
        }
        Loaders.sort(files, folders);
        return folders;
    }

    @Override
    public InputStream getThumbnailStream(String path, int width, int height) {
        try {
            return mDBApi.getThumbnailStream(path, DropboxAPI.ThumbSize.ICON_128x128, DropboxAPI.ThumbFormat.JPEG);
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
