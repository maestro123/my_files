package maestro.filemanager.drives.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

/**
 * Created by artyom on 9/12/14.
 */
public class DrivesDatabase extends SQLiteOpenHelper {

    public static final String TABLE_ONE_DRIVE = "one_drive";
    public static final String TABLE_GOOGLE_DRIVE = "google_drive";
    public static final String TABLE_YANDEX_DRIVE = "yandex_drive";
    public static final String TABLE_BOX_DRIVE = "box_drive";
    public static final String TABLE_DROP_BOX_DRIVE = "drop_box_drive";
    public static final String TABLE_MEGO_DRIVE = "mego_drive";
    public static final String KEY_ID = "id";
    public static final String KEY_PARENT = "parent";
    public static final String KEY_CONTENT = "item";
    private static final String DATABASE_NAME = "drives.tbl";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_TABLE = "create table %s (" + KEY_ID + " integer primary key, " + KEY_PARENT + " text, "
            + KEY_CONTENT + " text);";
    public static DrivesDatabase Instance;

    public DrivesDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Instance = this;
    }

    public static final String makeString(String value) {
        if (!TextUtils.isEmpty(value)) {
            return new StringBuilder().append("'").append(value).append("'").toString();
        }
        return value;
    }

    public static final String fromString(String value) {
        if (!TextUtils.isEmpty(value)) {
            return value.substring(1, value.length() - 2);
        }
        return value;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(String.format(CREATE_TABLE, TABLE_ONE_DRIVE));
        sqLiteDatabase.execSQL(String.format(CREATE_TABLE, TABLE_YANDEX_DRIVE));
        sqLiteDatabase.execSQL(String.format(CREATE_TABLE, TABLE_BOX_DRIVE));
        sqLiteDatabase.execSQL(String.format(CREATE_TABLE, TABLE_DROP_BOX_DRIVE));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public void insert(String table, String parentId, String content) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PARENT, makeString(parentId));
        contentValues.put(KEY_CONTENT, makeString(content));
        getWritableDatabase().insert(table, null, contentValues);
    }

    public String get(String table, String parentId) {
        Cursor cursor = getReadableDatabase().query(table, null, KEY_PARENT + " = ?", new String[]{makeString(parentId)}, null, null, null);
        try {
            if (cursor.getCount() > 0) {
                return fromString(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CONTENT)));
            }
        } finally {
            cursor.close();
        }
        return null;
    }

}
