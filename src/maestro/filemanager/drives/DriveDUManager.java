package maestro.filemanager.drives;

import java.util.ArrayList;

/**
 * Created by Artyom on 5/4/2015.
 */
public class DriveDUManager {

    private static DriveDUManager instance;

    public static synchronized DriveDUManager getInstance() {
        return instance == null ? (instance = new DriveDUManager()) : instance;
    }

    DriveDUManager() {
    }

    private ArrayList<DriveDUObject> mUploadQueue = new ArrayList<>();
    private ArrayList<DriveDUObject> mDownloadQueue = new ArrayList<>();
    private ArrayList<DriveDUObject> mNotProcessObject = new ArrayList<>();

    private Thread mUploadThread;
    private Thread mDownloadThread;

    public void upload(DriveDUObject uploadObject) {
        synchronized (mUploadQueue) {
            if (!mUploadQueue.contains(uploadObject)) {
                mUploadQueue.add(uploadObject);
            }
        }
        processUpload();
    }

    public void download(DriveDUObject downloadObject) {
        synchronized (mDownloadQueue) {
            if (!mDownloadQueue.contains(downloadObject)) {
                mDownloadQueue.add(downloadObject);
            }
        }
        processDownload();
    }

    public void add(DriveDUObject duObject) {
        synchronized (mNotProcessObject) {
            if (!mNotProcessObject.contains(duObject)) {
                mNotProcessObject.add(duObject);
            }
        }
    }

    public void remove(DriveDUObject duObject) {
        synchronized (mNotProcessObject) {
            mNotProcessObject.remove(duObject);
        }
    }

    public DriveDUObject get(String key, ArrayList<DriveDUObject> objects) {
        DriveDUObject duObject = null;
        synchronized (mUploadQueue) {
            duObject = getObject(mUploadQueue, objects, key);
        }
        if (duObject == null) {
            synchronized (mDownloadQueue) {
                duObject = getObject(mDownloadQueue, objects, key);
            }
            if (duObject == null) {
                synchronized (mNotProcessObject) {
                    duObject = getObject(mNotProcessObject, objects, key);
                }
            }
        }
        return duObject;
    }

    private DriveDUObject getObject(ArrayList<DriveDUObject> arrayList, ArrayList<DriveDUObject> outList, String key) {
        final int size = arrayList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                DriveDUObject object = mDownloadQueue.get(i);
                if (object.getKey().equals(key)) {
                    if (outList != null) {
                        outList.add(object);
                    } else {
                        return object;
                    }
                }
            }
        }
        return null;
    }


    private void processUpload() {
        synchronized (mUploadQueue) {
            if (mUploadThread == null && mUploadQueue.size() > 0) {
                final DriveDUObject udObject = mUploadQueue.get(mUploadQueue.size() - 1);
                mUploadThread = new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        udObject.upload();
                        synchronized (mUploadQueue) {
                            mUploadQueue.remove(udObject);
                        }
                        mUploadThread = null;
                        processUpload();
                    }
                };
                mUploadThread.start();
            }
        }
    }

    private void processDownload() {
        synchronized (mDownloadQueue) {
            if (mDownloadThread == null && mDownloadQueue.size() > 0) {
                final DriveDUObject udObject = mDownloadQueue.get(mDownloadQueue.size() - 1);
                mDownloadThread = new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        udObject.download();
                        synchronized (mDownloadQueue) {
                            mDownloadQueue.remove(udObject);
                        }
                        mDownloadThread = null;
                        processDownload();
                    }
                };
                mDownloadThread.start();
            }
        }
    }

}
