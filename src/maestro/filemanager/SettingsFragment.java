package maestro.filemanager;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.CheckBox;
import android.widget.TextView;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.utils.theme.ThemeHolder;

import java.util.ArrayList;

/**
 * Created by artyom on 21.4.15.
 */
public class SettingsFragment extends BaseFragment {

    public static final String TAG = SettingsFragment.class.getSimpleName();

    private RecyclerView mList;
    private GridLayoutManager mGridManager;
    private SettingsAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.settings_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(mGridManager = new GridLayoutManager(getActivity(), getSpanCount()));
        mGridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                return 1;
            }
        });

        final Toolbar mToolbar = (Toolbar) v.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.setting);
        mToolbar.setNavigationIcon(getSvgHolder().getDrawable(R.raw.ic_menu_24px, ThemeHolder.getActionBarIconColor()));
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mToolbar.setBackgroundColor(ThemeHolder.PrimaryColor);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);

        return v;
    }

    @Override
    public boolean enterAnimationEnable() {
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new SettingsAdapter(this));
        ArrayList<SettingsAdapter.SettingItem> mItems = new ArrayList<>();
        mItems.add(new SettingsAdapter.SettingItem().title(getString(R.string.clouds)).type(SettingsAdapter.TYPE_HEADER));
        mItems.add(new SettingsAdapter.SettingItem().title(String.format("%s %s", "One Drive", getString(R.string.save_path)))
                .content(Settings.getOneDriveDirectory().getPath()));
        mItems.add(new SettingsAdapter.SettingItem().title(String.format("%s %s", "Drop Box", getString(R.string.save_path)))
                .content(Settings.getDropBoxDirectory().getPath()));
        mItems.add(new SettingsAdapter.SettingItem().title(String.format("%s %s", "Yandex drive", getString(R.string.save_path)))
                .content(Settings.getYandexDriveDirectory().getPath()));
        mItems.add(new SettingsAdapter.SettingItem().title(String.format("%s %s", "Box", getString(R.string.save_path)))
                .content(Settings.getOneDriveDirectory().getPath()));
        mAdapter.update(mItems);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    final int getSpanCount() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 2 : 1;
    }

    public static final class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.Holder> {

        public static final int TYPE_TEXT = 0;
        public static final int TYPE_HEADER = 1;
        public static final int TYPE_RADIO = 2;
        public static final int TYPE_CHECKBOX = 3;

        private BaseFragment mFragment;
        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<SettingItem> mItems;

        public SettingsAdapter(BaseFragment fragment) {
            mFragment = fragment;
            mContext = fragment.getActivity();
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public final void update(ArrayList<SettingItem> items) {
            mItems = items;
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return mItems.get(position).Type;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = null;
            switch (i) {
                case TYPE_HEADER:
                    v = mInflater.inflate(R.layout.setting_header_view, null);
                    break;
                case TYPE_CHECKBOX:
                    v = mInflater.inflate(R.layout.setting_check_box_view, null);
                    break;
                case TYPE_TEXT:
                    v = mInflater.inflate(R.layout.setting_text_view, null);
                    break;
            }
            return new Holder(v, i);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            SettingItem item = mItems.get(i);
            holder.Title.setText(item.Title);
            switch (getItemViewType(i)) {
                case TYPE_TEXT:
                    holder.Content.setText(item.Content);
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return mItems != null ? mItems.size() : 0;
        }

        public final class Holder extends RecyclerView.ViewHolder {

            TextView Title;
            TextView Content;
            CheckBox Checkbox;

            public Holder(View itemView, int itemType) {
                super(itemView);
                Title = (TextView) itemView.findViewById(R.id.title);
                Content = (TextView) itemView.findViewById(R.id.additional);
                Checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
                switch (itemType) {
                    case TYPE_CHECKBOX:
                        Title = Checkbox;
                        break;
                }
            }

        }

        public static class SettingItem {

            int Type;
            int Id;
            String Title;
            String Content;
            int IconResource;

            public SettingItem() {

            }

            public SettingItem type(int type) {
                Type = type;
                return this;
            }

            public SettingItem id(int id) {
                Id = id;
                return this;
            }

            public SettingItem title(String title) {
                Title = title;
                return this;
            }

            public SettingItem content(String content) {
                Content = content;
                return this;
            }

            public SettingItem iconResource(int iconResource) {
                IconResource = iconResource;
                return this;
            }

        }

    }

}
