package maestro.filemanager.files.box;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by artyom on 4/22/15.
 */
public class BoxFile implements Parcelable {

    private JSONObject jObj;

    public BoxFile(JSONObject jsonObject) {
        jObj = jsonObject;
        create();
    }

    public BoxFile(Parcel source) throws JSONException {
        jObj = new JSONObject(source.readString());
        create();
    }

    final void create() {

    }

    public String getId() {
        return jObj.optString("id");
    }

    public String getType() {
        return jObj.optString("type");
    }

    public String getName() {
        return jObj.optString("name");
    }

    public String getCreateTime() {
        return jObj.optString("created_at");
    }

    public String getSize() {
        return jObj.optString("size");
    }

    public String getSharedLink() {
        return jObj.optString("shared_link");
    }

    public boolean isFolder() {
        String type = jObj.optString("type");
        return !TextUtils.isEmpty(type) && type.equals("folder");
    }

    public String getSHA1() {
        return jObj.optString("sha1");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<BoxFile> CREATOR = new Creator<BoxFile>() {
        @Override
        public BoxFile createFromParcel(Parcel source) {
            try {
                return new BoxFile(source);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public BoxFile[] newArray(int size) {
            return new BoxFile[size];
        }
    };

}
