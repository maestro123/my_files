package maestro.filemanager.files.onedrive;

import android.util.Log;
import maestro.filemanager.utils.Utils;
import org.json.JSONObject;

/**
 * Created by Artyom on 3/18/2015.
 */
public abstract class BaseOneDriveObject {

    public interface Visitor {
        public void visit(OneDriveAlbum album);

        public void visit(OneDriveAudio audio);

        public void visit(OneDrivePhoto photo);

        public void visit(OneDriveFolder folder);

        public void visit(OneDriveFile file);

        public void visit(OneDriveVideo video);
    }

    public static class From {
        private final JSONObject mFrom;

        public From(JSONObject from) {
            assert from != null;
            mFrom = from;
        }

        public String getName() {
            return mFrom.optString("name");
        }

        public String getId() {
            return mFrom.optString("id");
        }

        public JSONObject toJson() {
            return mFrom;
        }
    }

    public static class SharedWith {
        private final JSONObject mSharedWidth;

        public SharedWith(JSONObject sharedWith) {
            assert sharedWith != null;
            mSharedWidth = sharedWith;
        }

        public String getAccess() {
            return mSharedWidth.optString("access");
        }

        public JSONObject toJson() {
            return mSharedWidth;
        }
    }

    public static BaseOneDriveObject create(JSONObject skyDriveObject) {
        String type = skyDriveObject.optString("type");

        if (type.equals(OneDriveFolder.TYPE)) {
            return new OneDriveFolder(skyDriveObject);
        } else if (type.equals(OneDriveFile.TYPE)) {
            return new OneDriveFile(skyDriveObject);
        } else if (type.equals(OneDriveAlbum.TYPE)) {
            return new OneDriveAlbum(skyDriveObject);
        } else if (type.equals(OneDrivePhoto.TYPE)) {
            return new OneDrivePhoto(skyDriveObject);
        } else if (type.equals(OneDriveVideo.TYPE)) {
            return new OneDriveVideo(skyDriveObject);
        } else if (type.equals(OneDriveAudio.TYPE)) {
            return new OneDriveAudio(skyDriveObject);
        }

        final String name = skyDriveObject.optString("name");
        Log.e(BaseOneDriveObject.class.getName(),
                String.format("Unknown SkyDriveObject type.  Name: %s, Type %s", name, type));

        return null;
    }

    public String getReadableSize() {
        return Utils.readableFileSize(getSize());
    }

    public abstract long getSize();

    protected final JSONObject mObject;

    public BaseOneDriveObject(JSONObject object) {
        assert object != null;
        mObject = object;
    }

    public abstract void accept(Visitor visitor);

    public String getId() {
        return mObject.optString("id");
    }

    public From getFrom() {
        return new From(mObject.optJSONObject("from"));
    }

    public String getName() {
        return mObject.optString("name");
    }

    public String getParentId() {
        return mObject.optString("parent_id");
    }

    public String getDescription() {
        return mObject.isNull("description") ? null : mObject.optString("description");
    }

    public String getType() {
        return mObject.optString("type");
    }

    public String getLink() {
        return mObject.optString("link");
    }

    public String getCreatedTime() {
        return mObject.optString("created_time");
    }

    public String getUpdatedTime() {
        return mObject.optString("updated_time");
    }

    public String getUploadLocation() {
        return mObject.optString("upload_location");
    }

    public SharedWith getSharedWith() {
        return new SharedWith(mObject.optJSONObject("shared_with"));
    }

    public JSONObject toJson() {
        return mObject;
    }

}
