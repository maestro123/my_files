package maestro.filemanager.files;

import android.content.Context;
import android.os.Parcel;
import android.text.TextUtils;
import android.util.Log;
import maestro.filemanager.utils.FileUtils;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Utils;
import maestro.rar.RarEntryFile;
import maestro.rar.RarFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by artyom on 30.3.15.
 */
public class MArchiveFile extends MFile<File> {

    public static final String TAG = MArchiveFile.class.getSimpleName();

    private Object mArchiveFile;
    private long mLastModifyDate;

    private HashMap<String, ArrayList<MArchiveEntryFile>> mChilds = new HashMap<>();

    public static MArchiveFile fromParcel(Parcel source) {
        return MArchiveFile.create(new File(source.readString()), new AtomicBoolean());
    }

    public static MArchiveFile create(File originFile, AtomicBoolean canceled) {
        try {

            MArchiveFile zipFile = GlobalCache.getArchive(originFile.getPath());
            if (zipFile != null)
                return zipFile;
            zipFile = new MArchiveFile(originFile);
            final String name = originFile.getName();
            Log.e(TAG, "name: " + name + ", ends: " + name.endsWith("zip"));
            if (name.endsWith("zip") || name.endsWith("cbz")) {
                ZipFile file = new ZipFile(originFile);
                zipFile.setArchiveFile(file);
                Enumeration<? extends ZipEntry> e = file.entries();
                while (e.hasMoreElements()) {
                    ZipEntry entry = e.nextElement();
                    MArchiveEntryFile entryFile = new MArchiveEntryFile(zipFile, entry, zipFile.getPath(),
                            entry.getName(), entry.isDirectory(), entry.getTime(), entry.getSize());
                    zipFile.putOrAdd(entry.getName().split("/"), entryFile);
                }
            } else if (name.endsWith("jar")) {
                JarFile file = new JarFile(originFile);
                zipFile.setArchiveFile(file);
                for (Enumeration e = file.entries(); e.hasMoreElements(); ) {
                    JarEntry entry = (JarEntry) e.nextElement();
                    MArchiveEntryFile entryFile = new MArchiveEntryFile(zipFile, entry, zipFile.getPath(), entry.getName(),
                            entry.isDirectory(), entry.getTime(), entry.getSize());
                    zipFile.putOrAdd(entry.getName().split("/"), entryFile);
                }
            } else if (name.endsWith("rar") || name.endsWith("cbr")) {
                RarFile file = new RarFile(originFile);
                zipFile.setArchiveFile(file);
                List<RarEntryFile> entries = file.list();
                for (RarEntryFile entry : entries) {
                    MArchiveEntryFile entryFile = new MArchiveEntryFile(zipFile, entry, zipFile.getPath(), entry.getName(),
                            entry.isDirectory(), entry.getTimestamp(), entry.getUnpackedSize());
                    zipFile.putOrAdd(entry.getName().split("/"), entryFile);
                }
            }
            GlobalCache.addArchive(originFile.getPath(), zipFile);
            return zipFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MArchiveFile(File originalObject) {
        super(originalObject, originalObject.getName(), originalObject.getPath(), Utils.getExtension(originalObject.getPath()),
                Utils.readableFileSize(originalObject.length()), !originalObject.isHidden());
        mLastModifyDate = getOriginalObject().lastModified();
    }

    final void putOrAdd(String[] roots, MArchiveEntryFile entryFile) {
        final String root = roots.length == 1 ? "" : roots[roots.length - 2];
        if (!mChilds.containsKey("")) {
            mChilds.put("", new ArrayList<MArchiveEntryFile>());
        }
        if (roots.length > 1) {
            String prevRoot = "";
            StringBuilder rootBuilder = new StringBuilder();
            for (int i = 0; i < roots.length - 1; i++) {
                String localRoot = roots[i];
                if (!mChilds.containsKey(localRoot)) {
                    mChilds.put(localRoot, new ArrayList<MArchiveEntryFile>());
                }
                if (rootBuilder.length() > 0) {
                    rootBuilder.append("/");
                }
                rootBuilder.append(localRoot);
                MArchiveEntryFile mArchiveEntryFile = new MArchiveEntryFile(this,
                        null, getPath(), rootBuilder.toString(), true, mLastModifyDate, 0);
                if (!mChilds.get(prevRoot).contains(mArchiveEntryFile)) {
                    mChilds.get(prevRoot).add(mArchiveEntryFile);
                }
                prevRoot = localRoot;
            }
        }

        if (mChilds.get(root).contains(entryFile)) {
            mChilds.get(root).remove(entryFile);
        }
        mChilds.get(root).add(entryFile);
    }

    @Override
    public long getDate() {
        return mLastModifyDate;
    }

    @Override
    public boolean isFolder() {
        return false;
    }

    @Override
    public long getSize() {
        return getOriginalObject().length();
    }

    @Override
    public boolean rename(Context context, String name) {
        return false;
    }

    public Object getArchiveFile() {
        return mArchiveFile;
    }

    public void setArchiveFile(Object mArchiveFile) {
        this.mArchiveFile = mArchiveFile;
    }

    public MArchiveEntryFile getZipEntryFile(String name) {
        try {
            String path = MArchiveEntryFile.makePath(getPath(), name);
            String[] splited = name.split("/");
            String root = splited.length < 2 ? "" : splited[splited.length - 2];
            ArrayList<MArchiveEntryFile> childs = mChilds.get(root);
            if (childs != null && childs.size() > 0)
                for (MArchiveEntryFile entryFile : childs) {
                    if (entryFile.getPath().equals(path)) {
                        return entryFile;
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isDeleteEnable() {
        return getOriginalObject().canWrite();
    }

    @Override
    public boolean delete(Context context) {
        return getOriginalObject().delete();
    }

    @Override
    public boolean isExist() {
        return getOriginalObject().exists();
    }

    @Override
    public GlobalCache.ACTION_RESULT copy(String destFolder) {
        try {
            FileUtils.copyFileToDirectory(getOriginalObject(), new File(destFolder));
            return GlobalCache.ACTION_RESULT.OK;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public GlobalCache.ACTION_RESULT cut(String destFolder) {
        try {
            FileUtils.copyFileToDirectory(getOriginalObject(), new File(destFolder));
            if (getOriginalObject().delete()) {
                return GlobalCache.ACTION_RESULT.OK;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new FileInputStream(getOriginalObject());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public GlobalCache.ACTION_RESULT unPack(String destFolder) {
        File nFile = new File(destFolder, getOriginalObject().getName());
        if (nFile.mkdirs()) {
            MArchiveEntryFile[] files = list();
            for (int i = 0; i < files.length; i++) {
                GlobalCache.ACTION_RESULT result = files[i].unPack(nFile.getPath());
                if (result != GlobalCache.ACTION_RESULT.OK) {
                    return result;
                }
            }
            return GlobalCache.ACTION_RESULT.OK;
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getOriginalObject().getPath());
    }

    public MArchiveEntryFile[] list() {
        return list("");
    }

    public MArchiveEntryFile[] list(String root) {
        if (!TextUtils.isEmpty(root) && root.endsWith("/")) {
            String[] splited = root.split("/");
            root = splited.length == 0 ? "" : splited[splited.length - 1];
        }
        ArrayList<MArchiveEntryFile> childs = mChilds.get(root);
        return childs != null ? childs.toArray(new MArchiveEntryFile[childs.size()]) : new MArchiveEntryFile[0];
    }

    public static final Creator<MArchiveFile> CREATOR = new Creator<MArchiveFile>() {

        @Override
        public MArchiveFile createFromParcel(Parcel source) {
            return MArchiveFile.fromParcel(source);
        }

        @Override
        public MArchiveFile[] newArray(int size) {
            return new MArchiveFile[size];
        }

    };
}
