package maestro.filemanager.files;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Utils;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by artyom on 9/11/14.
 */
public abstract class MFile<T> implements Parcelable {

    private T originalObject;
    private String title;
    private String path;
    private String extension;
    private String readableSize;
    private boolean isVisible;

    public MFile(T originalObject, String title, String path, String extension, String readableSize, boolean isVisible) {
        this.originalObject = originalObject;
        this.title = title;
        this.path = path;
        this.extension = extension;
        this.readableSize = readableSize;
        this.isVisible = isVisible;
    }

    public MFile(Parcel source) {
        title = source.readString();
        path = source.readString();
        extension = source.readString();
        readableSize = source.readString();
        isVisible = source.readByte() == 0x00;
    }

    public T getOriginalObject() {
        return originalObject;
    }

    public void setOriginalObject(T originalObject) {
        this.originalObject = originalObject;
    }

    public String getTitle() {
        return title;
    }

    public String getExtension() {
        return extension;
    }

    public String getPath() {
        return path;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public abstract long getDate();

    public abstract boolean isFolder();

    public abstract long getSize();

    public abstract boolean rename(Context context, String name);

    public abstract boolean isDeleteEnable();

    public abstract boolean delete(Context context);

    public abstract boolean isExist();

    public abstract GlobalCache.ACTION_RESULT copy(String destFolder);

    public abstract GlobalCache.ACTION_RESULT cut(String destFolder);

    public abstract InputStream getInputStream();

    public String getReadableSize() {
        return readableSize;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(path);
        dest.writeString(extension);
        dest.writeString(readableSize);
        dest.writeByte((byte) (isVisible ? 0x00 : 0x01));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static MFile fromPath(String path, AtomicBoolean isCanceled) {
        if (path.contains(Utils.ZIP_DIVIDER)) {
            String[] split = path.split(Utils.ZIP_DIVIDER);
            MArchiveFile zipFile = MArchiveFile.create(new File(split[0]), isCanceled);
            return split.length == 1 ? zipFile : zipFile.getZipEntryFile(split[split.length - 1]);
        } else if (Utils.match(path, Utils.ZIP_PATTERN)) {
            return new MArchiveFile(new File(path));
        } else {
            return new MJavaFile(new File(path));
        }
    }

}
