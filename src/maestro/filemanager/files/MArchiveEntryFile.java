package maestro.filemanager.files;

import android.content.Context;
import android.os.Parcel;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Utils;
import maestro.rar.RarEntryFile;
import maestro.rar.RarFile;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by artyom on 30.3.15.
 */
public class MArchiveEntryFile extends MFile<Object> {

    public static final String TAG = MArchiveEntryFile.class.getSimpleName();

    public static String makePath(String parentPath, String name) {
        return parentPath + Utils.ZIP_DIVIDER + name;
    }

    private MArchiveFile mArchiveFile;
    private boolean mIsDirectory;
    private long mDate;
    private long mSize;

    public MArchiveEntryFile(MArchiveFile zipFile, Object originalObject, String parentPath, String name, boolean isDirectory, long date, long size) {
        super(originalObject, Utils.getTitleFromZipEntry(name), makePath(parentPath, name),
                Utils.getExtension(name), Utils.readableFileSize(size), Utils.isVisibleFile(name));
        mArchiveFile = zipFile;
        mIsDirectory = isDirectory;
        mDate = date;
        mSize = size;
    }

    public MArchiveEntryFile(Parcel source) {
        super(source);
    }

    @Override
    public InputStream getInputStream() {
        if (getOriginalObject() != null) {
            Object parentFile = mArchiveFile.getArchiveFile();
            if (parentFile instanceof ZipFile) {
                try {
                    return ((ZipFile) parentFile).getInputStream((ZipEntry) getOriginalObject());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (parentFile instanceof RarFile) {
                return ((RarFile) parentFile).getInputStream(((RarEntryFile) getOriginalObject()).getName());
            }
        }
        return null;
    }

    public MArchiveFile getArchiveFile() {
        return mArchiveFile;
    }

    public MArchiveEntryFile[] list() {
        return mArchiveFile != null ? mArchiveFile.list(getTitle()) : new MArchiveEntryFile[0];
    }

    @Override
    public long getDate() {
        return mDate;
    }

    @Override
    public boolean isFolder() {
        return mIsDirectory;
    }

    @Override
    public long getSize() {
        return mSize;
    }

    @Override
    public boolean rename(Context context, String name) {
        return false;
    }

    @Override
    public boolean isDeleteEnable() {
        return false;
    }

    @Override
    public boolean delete(Context context) {
        return false;
    }

    @Override
    public boolean isExist() {
        return true;
    }

    @Override
    public GlobalCache.ACTION_RESULT copy(String destFolder) {
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public GlobalCache.ACTION_RESULT cut(String destFolder) {
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    public GlobalCache.ACTION_RESULT unPack(String destFolder) {
        if (isFolder()) {
            File nFile = new File(destFolder, getTitle());
            if (nFile.mkdirs()) {
                MArchiveEntryFile[] files = list();
                for (int i = 0; i < files.length; i++) {
                    GlobalCache.ACTION_RESULT result = files[i].unPack(nFile.getPath());
                    if (result != GlobalCache.ACTION_RESULT.OK) {
                        return result;
                    }
                }
                return GlobalCache.ACTION_RESULT.OK;
            } else {
                try {
                    OutputStream outStream = new FileOutputStream(new File(nFile, getTitle()));
                    InputStream inStream = getInputStream();
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = inStream.read(buffer)) > 0) {
                        outStream.write(buffer, 0, len);
                    }
                    outStream.close();
                    inStream.close();
                    return GlobalCache.ACTION_RESULT.OK;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof String && getPath() != null) {
            return getPath().equals(o);
        } else if (o instanceof MArchiveEntryFile && getPath() != null) {
            MArchiveEntryFile entryFile = (MArchiveEntryFile) o;
            String path = getPath();
            String objectPath = entryFile.getPath();
            if (isFolder() && entryFile.isFolder()) {
                if (path.endsWith("/") && !objectPath.endsWith("/")) {
                    path = path.substring(0, path.length() - 1);
                } else if (!path.endsWith("/") && objectPath.endsWith("/")) {
                    objectPath = objectPath.substring(0, objectPath.length() - 1);
                }
            }
            return path.equals(objectPath);
        }
        return super.equals(o);
    }
}
