package maestro.filemanager.files;

import android.content.Context;
import android.os.Parcel;
import maestro.filemanager.utils.FileUtils;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Utils;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by artyom on 9/11/14.
 */
public class MJavaFile extends MFile<File> {

    public MJavaFile(File file) {
        super(file, file.getName(), file.getPath(), Utils.getExtension(file.getPath()),
                Utils.readableFileSize(file.length()), !file.isHidden());
    }

    public MJavaFile(Parcel source) {
        super(source);
        setOriginalObject(new File(source.readString()));
    }

    @Override
    public boolean isFolder() {
        return getOriginalObject().isDirectory();
    }

    @Override
    public boolean isVisible() {
        return !getOriginalObject().isHidden();
    }

    @Override
    public long getDate() {
        return getOriginalObject().lastModified();
    }

    @Override
    public long getSize() {
        return getOriginalObject().length();
    }

    @Override
    public boolean rename(Context context, String name) {
        File original = getOriginalObject();
        File newFile = new File(original.getParent(), name);
        return original.renameTo(newFile);
    }

    @Override
    public boolean isDeleteEnable() {
        return getOriginalObject().canWrite();
    }

    @Override
    public boolean delete(Context context) {
        if (isFolder()) {
            try {
                FileUtils.deleteDirectory(getOriginalObject());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return getOriginalObject().delete();
    }

    @Override
    public boolean isExist() {
        return getOriginalObject().exists();
    }

    @Override
    public GlobalCache.ACTION_RESULT copy(String destFolder) {
        if (isFolder()) {
            try {
                FileUtils.copyDirectory(getOriginalObject(), new File(destFolder));
                return GlobalCache.ACTION_RESULT.OK;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                FileUtils.copyFileToDirectory(getOriginalObject(), new File(destFolder));
                return GlobalCache.ACTION_RESULT.OK;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return GlobalCache.ACTION_RESULT.ERROR;
    }

    @Override
    public GlobalCache.ACTION_RESULT cut(String destFolder) {
        final GlobalCache.ACTION_RESULT copyResult = copy(destFolder);
        if (copyResult == GlobalCache.ACTION_RESULT.OK) {
            if (isFolder()) {
                try {
                    FileUtils.deleteDirectory(getOriginalObject());
                    return GlobalCache.ACTION_RESULT.OK;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (getOriginalObject().delete()) {
                return GlobalCache.ACTION_RESULT.OK;
            }
        }
        return copyResult;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getOriginalObject().getPath());
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof File) {
            return getOriginalObject().equals(o);
        } else if (o instanceof MJavaFile) {
            return getOriginalObject().equals(((MJavaFile) o).getOriginalObject());
        }
        return o != null ? o.equals(this) : false;
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new FileInputStream(getOriginalObject());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final Creator<MJavaFile> CREATOR = new Creator<MJavaFile>() {
        @Override
        public MJavaFile createFromParcel(Parcel source) {
            return new MJavaFile(source);
        }

        @Override
        public MJavaFile[] newArray(int size) {
            return new MJavaFile[size];
        }
    };

    public GlobalCache.ACTION_RESULT zip(String destPath) {
        try {

            final int BUFFER = 2048;

            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(destPath);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            if (isFolder()) {
                zipSubFolder(out, getOriginalObject(), getOriginalObject().getParent().length());
            } else {
                byte data[] = new byte[BUFFER];
                FileInputStream fi = new FileInputStream(getPath());
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(getLastPathComponent(getPath()));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
            }
            out.close();
            return GlobalCache.ACTION_RESULT.OK;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return GlobalCache.ACTION_RESULT.ERROR;
    }

    private void zipSubFolder(ZipOutputStream out, File folder, int basePathLength) throws IOException {

        final int BUFFER = 2048;

        File[] fileList = folder.listFiles();
        BufferedInputStream origin = null;
        for (File file : fileList) {
            if (file.isDirectory()) {
                zipSubFolder(out, file, basePathLength);
            } else {
                byte data[] = new byte[BUFFER];
                String unmodifiedFilePath = file.getPath();
                String relativePath = unmodifiedFilePath
                        .substring(basePathLength);
                FileInputStream fi = new FileInputStream(unmodifiedFilePath);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(relativePath);
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
        }
    }

    public String getLastPathComponent(String filePath) {
        String[] segments = filePath.split("/");
        String lastPathComponent = segments[segments.length - 1];
        return lastPathComponent;
    }

}
