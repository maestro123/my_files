package maestro.filemanager;

import android.graphics.Color;
import android.os.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import com.dream.android.mim.MIMUtils;
import maestro.filemanager.ui.*;
import maestro.filemanager.ui.base.BaseFileListFragment;
import maestro.filemanager.ui.base.BaseMediaFragment;
import maestro.filemanager.utils.FPopupMenu;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.IStorageInfoGetter;
import maestro.filemanager.utils.MBActivity;
import maestro.filemanager.utils.theme.ThemeHolder;

import java.lang.reflect.Field;
import java.util.List;

public class Main extends MBActivity implements GlobalCache.OnGlobalCacheChangeListener {

    public static final String TAG = Main.class.getSimpleName();

    private static final String SAVED_CURRENT_OPEN_TYPE = "current_open_type";

    public static final int TYPE_FILES = 0;
    public static final int TYPE_ONE_DRIVE = 1;
    public static final int TYPE_DROP_BOX = 2;
    public static final int TYPE_GOOGLE_DRIVE = 3;
    public static final int TYPE_YANDEX_DRIVE = 4;
    public static final int TYPE_BOX_DRIVE = 5;
    public static final int TYPE_BOOKMARKS = 6;
    public static final int TYPE_PHOTOS = 7;
    public static final int TYPE_DOWNLOADS = 8;
    public static final int TYPE_MUSIC = 9;
    public static final int TYPE_VIDEO = 10;
    public static final int TYPE_SETTINGS = 11;

    private DrawerLayout mDrawer;
    private FragmentTransaction mTransaction;
    private NavigationFragment mNavigationFragment;
    private CacheFragment mCacheFragment;
    private MenuItem mCacheMenuItem;
    private Toolbar mToolbar;

    private TypeHolder mCurrentType;
    private TypeHolder mPreviousType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        if (Build.VERSION.SDK_INT > 20) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(MIMUtils.blendColors(ThemeHolder.getPrimaryColor(), Color.parseColor("#000000"), 0.75f));
        }

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commit();
                invalidateOptionsMenu();
            }
        });
        if (savedInstanceState == null) {
            prepareFragments();
        } else {
            mCurrentType = savedInstanceState.getParcelable(SAVED_CURRENT_OPEN_TYPE);
        }
    }

    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        mToolbar = toolbar;
        mToolbar.setNavigationOnClickListener(mNavigationClickListener);
        ActionBar actionBar = getSupportActionBar();
        if (Build.VERSION.SDK_INT > 20) {
            actionBar.setElevation(0);
        }
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    protected void onDestroy() {
        GlobalCache.removeListener(this);
        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        GlobalCache.addListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        final int iconColor = getToolbarIconColor();
        menu.findItem(R.id.search).setIcon(((FileManagerApplication) getApplication()).getSvgHolder().getDrawable(R.raw.search, iconColor));
        menu.findItem(R.id.more).setIcon(((FileManagerApplication) getApplication()).getSvgHolder().getDrawable(R.raw.ic_more, iconColor));
        mCacheMenuItem = menu.findItem(R.id.cache_items);
        ensureCacheItemState(GlobalCache.getCount());
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawer.isDrawerOpen(Gravity.LEFT) || mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                mDrawer.closeDrawers();
                return true;
            }
            return false;
        } else if (item.getItemId() == R.id.cache_items) {
            mDrawer.openDrawer(Gravity.RIGHT);
            return true;
        } else if (item.getItemId() == R.id.more) {
            FPopupMenu.Create(this).show(findViewById(R.id.more));
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_CURRENT_OPEN_TYPE, mCurrentType);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.LEFT) || mDrawer.isDrawerOpen(Gravity.RIGHT)) {
            mDrawer.closeDrawers();
        } else
            super.onBackPressed();
    }

    public void commit() {
        if (mTransaction != null) {
            mTransaction.commit();
            mTransaction = null;
        }
    }

    private void prepareFragments() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.navigation_frame, mNavigationFragment = new NavigationFragment(), NavigationFragment.TAG)
                .replace(R.id.cache_frame, mCacheFragment = new CacheFragment(), CacheFragment.TAG).commit();
    }

    public void openFragment(TypeHolder holder) {
        if (mCurrentType == null || !holder.equals(mCurrentType)) {
            mTransaction = getSupportFragmentManager().beginTransaction();
            switch (holder.Type) {
                case TYPE_PHOTOS:
                    mTransaction.replace(R.id.main_frame, new MediaImageFragment(), MediaImageFragment.TAG);
                    break;
                case TYPE_MUSIC:
                    mTransaction.replace(R.id.main_frame, new MediaMusicFragment(), MediaMusicFragment.TAG);
                    break;
                case TYPE_VIDEO:
                    mTransaction.replace(R.id.main_frame, new MediaVideoFragment(), MediaVideoFragment.TAG);
                    break;
                case TYPE_ONE_DRIVE:
                    mTransaction.replace(R.id.main_frame, new OneDriveFragment(), OneDriveFragment.TAG);
                    break;
                case TYPE_DROP_BOX:
                    mTransaction.replace(R.id.main_frame, new DropBoxDriveFragment(), DropBoxDriveFragment.TAG);
                    break;
                case TYPE_GOOGLE_DRIVE:
                    mTransaction.replace(R.id.main_frame, new GoogleDriveFragment(), GoogleDriveFragment.TAG);
                    break;
                case TYPE_YANDEX_DRIVE:
                    mTransaction.replace(R.id.main_frame, new YandexDriveFragment(), YandexDriveFragment.TAG);
                    break;
                case TYPE_BOX_DRIVE:
                    mTransaction.replace(R.id.main_frame, new BoxDriveFragment(), BoxDriveFragment.TAG);
                    break;
                case TYPE_SETTINGS:
                    mTransaction.addToBackStack(null)
                            .add(R.id.main_frame, new SettingsFragment(), SettingsFragment.TAG);
                    break;
                default:
                    mTransaction.replace(R.id.main_frame, FilesListFragment.makeInstance(holder.Path));
                    break;
            }
            mPreviousType = mCurrentType;
            mCurrentType = holder;
            if (!mDrawer.isDrawerOpen(Gravity.LEFT)) {
                commit();
            } else {
                mDrawer.closeDrawers();
            }
        }
    }

    public void jumpToPreviousType() {
        openFragment(mPreviousType);
    }

    private int getToolbarIconColor() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null && fragmentList.size() > 0) {
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof BaseFileListFragment) {
                    return ((BaseFileListFragment) fragment).getToolbarIconColor();
                } else if (fragment instanceof BaseMediaFragment) {
                    return ((BaseMediaFragment) fragment).getToolbarIconColor();
                }
            }
        }
        return ThemeHolder.getActionBarIconColor();
    }

    public TypeHolder getCurrentOpenType() {
        return mCurrentType;
    }

    public void setStorageInfoGetter(IStorageInfoGetter getter) {
        Log.e(TAG, "setStorageInfoGetter: " + getter);
        if (mNavigationFragment != null) {
            mNavigationFragment.setStorageInfoGetter(getter);
        }
    }

    @Override
    public void onGlobalCacheCountChange(int count) {
        ensureCacheItemState(count);
        if (count > 0 && Settings.isFirstTimeCacheAdd()) {
            try {
                View rightFrame = findViewById(R.id.cache_frame);
                Field mDragger = mDrawer.getClass().getDeclaredField("mRightDragger");
                mDragger.setAccessible(true);
                ViewDragHelper draggerObj = (ViewDragHelper) mDragger.get(mDrawer);
                draggerObj.smoothSlideViewTo(rightFrame, (int) (mDrawer.getWidth() - rightFrame.getWidth() * 0.5f), 0);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDrawer.closeDrawer(Gravity.RIGHT);
                    }
                }, 500);
                Settings.setFirstTimeCacheAdd(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private final void ensureCacheItemState(int count) {
        mCacheMenuItem.setVisible(count > 0);
        mCacheMenuItem.setIcon(((FileManagerApplication) getApplication()).getSvgHolder().getDrawable(count > 1 ? R.raw.archive : R.raw.paste, Color.WHITE));
    }

    public void setDrawerState(boolean available) {
        if (mDrawer != null) {
            mDrawer.setDrawerLockMode(available ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    private final View.OnClickListener mNavigationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mDrawer != null) {
                if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
                    mDrawer.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawer.openDrawer(Gravity.LEFT);
                }
            }
        }
    };

    public static final class TypeHolder implements Parcelable {

        public int Type;
        public String Path;

        public TypeHolder(int type) {
            Type = type;
        }

        public TypeHolder(int type, String path) {
            this(type);
            Path = path;
        }

        TypeHolder(Parcel source) {
            Type = source.readInt();
            Path = source.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(Type);
            dest.writeString(Path);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof TypeHolder) {
                TypeHolder holder = (TypeHolder) o;
                return holder.Type == Type && ((holder.Path == null && Path == null)
                        || (holder.Path != null && Path != null && holder.Path.equals(Path)));
            }
            return super.equals(o);
        }

        public static final Creator<TypeHolder> CREATOR = new Creator<TypeHolder>() {
            @Override
            public TypeHolder createFromParcel(Parcel source) {
                return new TypeHolder(source);
            }

            @Override
            public TypeHolder[] newArray(int size) {
                return new TypeHolder[size];
            }
        };

    }

}
