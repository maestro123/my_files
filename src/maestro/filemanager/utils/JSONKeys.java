package maestro.filemanager.utils;

/**
 * Created by Artyom on 3/19/2015.
 */
public class JSONKeys {

    public static final String ERROR = "error";
    public static final String DATA = "data";

    public static final String QUOTA = "quota";
    public static final String AVAILABLE = "available";

}
