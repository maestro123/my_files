package maestro.filemanager.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.svg.SVGHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 1.4.15.
 */
public class FPopupMenu extends maestro.support.v1.menu.MPopupMenu {

    public static FPopupMenu Create(Context context) {
        ArrayList<MPopupMenuItem> items = new ArrayList<>();
        items.add(new MPopupMenuItem(context.getString(R.string.show_hidden), ItemType.CHECK_BOX).setId(R.id.show_hiden_files));
        items.add(new MPopupMenuItem(context.getString(R.string.sort), ItemType.DIVIDER));
        items.add(new MPopupMenuItem(context.getString(R.string.sort_name), R.raw.ic_sort, ItemType.NORMAL).setId(R.id.sort_name));
        items.add(new MPopupMenuItem(context.getString(R.string.sort_date), R.raw.ic_sort, ItemType.NORMAL).setId(R.id.sort_date));
        items.add(new MPopupMenuItem(context.getString(R.string.sort_size), R.raw.ic_sort, ItemType.NORMAL).setId(R.id.sort_size));
        items.add(new MPopupMenuItem(context.getString(R.string.sort_type), R.raw.ic_sort, ItemType.NORMAL).setId(R.id.sort_type));
        items.add(new MPopupMenuItem(context.getString(R.string.inverse), ItemType.CHECK_BOX).setId(R.id.sort_inverse));
        items.add(new MPopupMenuItem(context.getString(R.string.folder_first), ItemType.CHECK_BOX).setId(R.id.folder_first));
        FPopupMenu menu = new FPopupMenu(context, ((FileManagerApplication) context.getApplicationContext()).getSvgHolder(), items) {
            @Override
            public boolean isActivated(MPopupMenuItem item) {
                switch (item.Id) {
                    case R.id.sort_name:
                        return Settings.getSortType() == Settings.SORT_TYPE.TITLE;
                    case R.id.sort_date:
                        return Settings.getSortType() == Settings.SORT_TYPE.DATE;
                    case R.id.sort_size:
                        return Settings.getSortType() == Settings.SORT_TYPE.SIZE;
                    case R.id.sort_type:
                        return Settings.getSortType() == Settings.SORT_TYPE.TYPE;
                    case R.id.sort_inverse:
                        return Settings.isSortInverse();
                    case R.id.show_hiden_files:
                        return Settings.isShowHidden();
                    case R.id.folder_first:
                        return Settings.isFolderFirst();
                }
                return false;
            }
        };
        menu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(MPopupMenuItem item) {
                switch (item.Id) {
                    case R.id.sort_name:
                        Settings.setSortType(Settings.SORT_TYPE.TITLE);
                        break;
                    case R.id.sort_date:
                        Settings.setSortType(Settings.SORT_TYPE.DATE);
                        break;
                    case R.id.sort_size:
                        Settings.setSortType(Settings.SORT_TYPE.SIZE);
                        break;
                    case R.id.sort_type:
                        Settings.setSortType(Settings.SORT_TYPE.TYPE);
                        break;
                    case R.id.sort_inverse:
                        Settings.setSortInverse(!Settings.isSortInverse());
                        break;
                    case R.id.show_hiden_files:
                        Settings.setShowHideFolders(!Settings.isShowHidden());
                        break;
                    case R.id.folder_first:
                        Settings.setFolderFirst(!Settings.isFolderFirst());
                        break;
                }
            }
        });
        return menu;
    }

    public FPopupMenu(Context context, SVGHelper.SVGHolder holder, List<MPopupMenuItem> items) {
        super(context, holder, items);
    }

    public FPopupMenu(Context context, SVGHelper.SVGHolder holder, List<MPopupMenuItem> items, int styleResource) {
        super(context, holder, items, styleResource);
    }

    @Override
    public View getCustomView(int position, View v, ViewGroup parent) {
        return super.getCustomView(position, v, parent);
    }

    @Override
    public void onItemCreate(MPopupMenuItem item, ItemViewHolder holder) {
        super.onItemCreate(item, holder);
        holder.TextView.setTypeface(Typefacer.rMedium);
    }

    @Override
    public void onItemPrepare(MPopupMenuItem item, ItemViewHolder holder) {
        super.onItemPrepare(item, holder);
        holder.TextView.setTextColor(item.Type == ItemType.DIVIDER ? Color.parseColor("#aaaaaa") : getIconColor(item));
        if (holder instanceof CheckBoxItemViewHolder) {
            CheckBox checkBox = (CheckBox) holder.TextView;
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(isActivated(item));
            checkBox.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public int getIconColor(MPopupMenuItem item) {
        return isActivated(item) ? ThemeHolder.getPrimaryColor() : ThemeHolder.getIconColor();
    }

    public boolean isActivated(MPopupMenuItem item){
        return false;
    };

}
