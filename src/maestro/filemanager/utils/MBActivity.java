package maestro.filemanager.utils;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom on 16.04.2014.
 */
public class MBActivity extends AppCompatActivity {

    public static final String TAG = MBActivity.class.getSimpleName();

    private ArrayList<OnBackPressListener> mBackPressListeners = new ArrayList<OnBackPressListener>();

    public void attachOnBackPressListener(OnBackPressListener listener) {
        synchronized (mBackPressListeners) {
            if (mBackPressListeners.contains(listener)) {
                mBackPressListeners.remove(listener);
            }
            mBackPressListeners.add(listener);
        }
    }

    public void detachOnBackPressListener(OnBackPressListener listener) {
        synchronized (mBackPressListeners) {
            mBackPressListeners.remove(listener);
        }
    }

    @Override
    public void onBackPressed() {
        if (!ensureBackPressListeners()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public boolean ensureBackPressListeners() {
        synchronized (mBackPressListeners) {
            int size = mBackPressListeners.size();
            for (int i = size - 1; i > -1; i--) {
                if (mBackPressListeners.get(i).onBackPressed()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ensureOtherFragments(getSupportFragmentManager(), requestCode, resultCode, data);
    }

    private void ensureOtherFragments(FragmentManager mManager, int requestCode, int resultCode, Intent data) {
        List<Fragment> fragments = mManager.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment f : fragments) {
                if (f != null) {
                    f.onActivityResult(requestCode, resultCode, data);
                    ensureOtherFragments(f.getChildFragmentManager(), requestCode, resultCode, data);
                }
            }
        }
    }

    public interface OnBackPressListener {
        public boolean onBackPressed();
    }
}

