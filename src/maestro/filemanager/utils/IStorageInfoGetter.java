package maestro.filemanager.utils;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by artyom on 25.4.15.
 */
public interface IStorageInfoGetter {

    void prepareInfo(TextView userNamePlaceHolder, TextView sizePlaceHolder);

    void prepareIcon(ImageView placeHolder);

    void cancel();

}
