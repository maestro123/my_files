package maestro.filemanager.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaMetadataRetriever;
import android.text.TextUtils;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMUtils;
import com.dropbox.client2.DropboxAPI;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.*;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.drives.DropBoxManager;
import maestro.filemanager.drives.YandexDriveManager;
import maestro.filemanager.drives.boxdrive.BoxDriveApi;
import maestro.filemanager.drives.yandexdrive.ListItem;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.files.onedrive.OneDrivePhoto;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.svg.SVG;
import maestro.support.v1.svg.SVGHelper;
import maestro.support.v1.svg.SVGParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Artyom on 3/3/2015.
 */
public class MIconMaker extends MIMInternetMaker {

    public static final String TAG = MIconMaker.class.getSimpleName();

    private Context mContext;
    private SVGHelper.SVGHolder svgHolder;
    private ReentrantLock mLock = new ReentrantLock();

    public MIconMaker(Context context) {
        super(false);
        mContext = context;
        svgHolder = ((FileManagerApplication) context.getApplicationContext()).getSvgHolder();
    }

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        if (loadObject.getObject() instanceof MJavaFile && Utils.match(loadObject.getPath(), Utils.APK_PATTERN)) {
            try {
                PackageManager pm = mContext.getPackageManager();
                PackageInfo pi = pm.getPackageArchiveInfo(loadObject.getPath(), 0);
                if (pi != null) {
                    pi.applicationInfo.sourceDir = loadObject.getPath();
                    pi.applicationInfo.publicSourceDir = loadObject.getPath();
                    Drawable icon = pi.applicationInfo.loadIcon(pm);
                    if (icon != null) {
                        Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(bitmap);
                        icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                        icon.setFilterBitmap(true);
                        icon.draw(canvas);
                        return bitmap;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } else if (loadObject.getObject() instanceof MFile && Utils.match(loadObject.getPath(), Utils.SVG_PATTERN)) {
            SVG svg = SVGParser.getSVGFromInputStream(((MFile) loadObject.getObject()).getInputStream());
            final int sWidth = svg.getIntrinsicWidth();
            final int sHeight = svg.getIntrinsicHeight();
            final float xScale = loadObject.getWidth() / sWidth;
            final float yScale = loadObject.getHeight() / sHeight;
            return svg.getBitmap(Math.max(xScale, yScale));
        } else if (loadObject.getObject() instanceof MJavaFile && Utils.match(loadObject.getPath(), Utils.VIDEO_PATTERN)) {
            return getVideoBitmap(loadObject.getPath());
        } else if (loadObject.getObject() instanceof MJavaFile && Utils.match(loadObject.getPath(), Utils.MUSIC_PATTERN)) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(loadObject.getPath());
            try {
                byte[] bytes = mediaMetadataRetriever.getEmbeddedPicture();
                Bitmap bitmap = bytes != null && bytes.length > 0 ? BitmapFactory.decodeByteArray(bytes, 0, bytes.length) : null;
                return bitmap != null ? bitmap : getMusicSvgIcon(loadObject.getWidth());
            } finally {
                mediaMetadataRetriever.release();
            }
        } else if (loadObject.getObject() instanceof BaseMediaItem) {
            if (loadObject.getObject() instanceof MediaBucket) {
                MediaBucket bucket = (MediaBucket) loadObject.getObject();
                List<Image> images = MediaStoreHelper.getImages(ctx, bucket.Id);

                final int drawCount = images != null ? images.size() > 4 ? 4 : images.size() : 0;
                final int sidePadding = loadObject.getWidth() / drawCount;
                if (drawCount == 1) {
                    Bitmap bitmap = getBitmap(loadObject, images.get(0), true);
                    if (bitmap == null) {
                        SVG svg = svgHolder.getDrawable(R.raw.ic_library_image_24px, ThemeHolder.getIconColor(), loadObject.getWidth() / 24f);
                        return svg.getBitmap();
                    }
                    return bitmap;
                } else {
                    Bitmap bitmap = Bitmap.createBitmap(loadObject.getWidth(), loadObject.getHeight(), Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(bitmap);
                    if (drawCount == 0) {
                        canvas.drawColor(Color.GRAY);
                        SVG svg = svgHolder.getDrawable(R.raw.ic_library_image_24px, ThemeHolder.getIconColor(), loadObject.getWidth() / 24f);
                        return svg.getBitmap();
                    } else {
                        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                        paint.setFilterBitmap(true);
                        GradientDrawable rightShadow = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Color.BLACK, Color.TRANSPARENT});
                        rightShadow.setBounds(0, 0, (int) (ctx.getResources().getDisplayMetrics().density * 20), loadObject.getHeight());
                        for (int i = 0; i < drawCount; i++) {
                            Bitmap drawBitmap = getBitmap(loadObject, images.get(i), true);
                            if (drawBitmap != null) {
                                canvas.save();
                                final Rect srcRect = MIMUtils.calculateSrcRect(bitmap.getWidth(), bitmap.getHeight(), loadObject.getWidth(), loadObject.getHeight());
                                final Rect dstRect = new Rect(0, 0, loadObject.getWidth(), loadObject.getWidth());
                                canvas.translate(sidePadding * i, 0);
                                canvas.drawBitmap(drawBitmap, srcRect, dstRect, null);
                                if (i > 0)
                                    rightShadow.draw(canvas);
                                canvas.restore();
                                drawBitmap.recycle();
                            }
                        }
                    }
                    return bitmap;
                }
            } else if (loadObject.getObject() instanceof Image || loadObject.getObject() instanceof Album) {
                Bitmap bitmap = getBitmap(loadObject);
                if (bitmap == null) {
                    SVG svg = svgHolder.getDrawable(loadObject.getObject() instanceof Image
                                    ? R.raw.ic_library_image_24px : R.raw.ic_library_music_24px,
                            ThemeHolder.getIconColor(), loadObject.getWidth() / 24f);
                    return svg.getBitmap();
                }
                return bitmap;
            } else if (loadObject.getObject() instanceof Video) {
                return getVideoBitmap(((Video) loadObject.getObject()).Path);
            }
        } else if (loadObject.getObject() != null) {
            if (loadObject.getObject() instanceof MArchiveEntryFile) {
                return getBitmap(loadObject, loadObject.getObject(), true);
            }
            return getBitmap(loadObject);
        }
        return super.getBitmap(loadObject, ctx);
    }

    private final Bitmap getBitmap(ImageLoadObject loadObject) {
        return getBitmap(loadObject, loadObject.getObject(), true);
    }

    private final Bitmap getBitmap(ImageLoadObject loadObject, Object object, boolean inSample) {
        final boolean isLockRequired = isLockRequired(object);
        if (isLockRequired)
            mLock.lock();
        try {
            InputStream inputStream;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = loadObject.getConfig();
            if (inSample) {
                options.inJustDecodeBounds = true;
                inputStream = getStream(loadObject, object);
                if (inputStream == null)
                    return null;
                BitmapFactory.decodeStream(inputStream, null, options);
                options.inSampleSize = calculateInSampleSize(options, loadObject.getWidth(), loadObject.getHeight());
                options.inJustDecodeBounds = false;
            }
            inputStream = getStream(loadObject, object);
            if (inputStream == null)
                return null;
            return BitmapFactory.decodeStream(inputStream, null, options);
        } finally {
            if (isLockRequired)
                mLock.unlock();
        }
    }

    private InputStream getStream(ImageLoadObject loadObject, Object object) {
        if (object instanceof OneDrivePhoto) {
            try {
                return getNetworkStream(((OneDrivePhoto) object).getPicture());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (object instanceof DropboxAPI.Entry) {
            return DropBoxManager.getInstance().getThumbnailStream(((DropboxAPI.Entry) object).path, loadObject.getWidth(), loadObject.getHeight());
        } else if (object instanceof ListItem) {
            return YandexDriveManager.getInstance().getThumbnailStream(((ListItem) loadObject.getObject()).getFullPath(), loadObject.getWidth(), loadObject.getHeight());
        } else if (object instanceof MArchiveEntryFile) {
            return ((MArchiveEntryFile) object).getInputStream();
        } else if (object instanceof MJavaFile) {
            return ((MJavaFile) object).getInputStream();
        } else if (object instanceof BoxFile) {
            return BoxDriveApi.getThumbnail(((BoxFile) object).getId(), BoxDriveApi.SIZE.of(loadObject.getWidth()));
        } else if (object instanceof Image) {
            try {
                Image image = (Image) object;
                if (image != null && !TextUtils.isEmpty(image.Path))
                    return new FileInputStream(image.Path);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (object instanceof Album) {
            try {
                Album album = (Album) object;
                if (album != null && !TextUtils.isEmpty(album.artPath))
                    return new FileInputStream(album.artPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private final Bitmap getMusicSvgIcon(int width) {
        SVG svg = svgHolder.getDrawable(R.raw.ic_library_music_24px, ThemeHolder.getIconColor(), width / 24f);
        return svg.getBitmap();
    }

    private Bitmap getVideoBitmap(String path) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(path);
        try {
            return mediaMetadataRetriever.getFrameAtTime(Long.parseLong(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) / 2,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    private boolean isLockRequired(Object object) {
        return false;
    }

}
