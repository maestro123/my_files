package maestro.filemanager.utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import com.dropbox.client2.DropboxAPI;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import com.msoft.android.mplayer.lib.models.MediaBucket;
import com.msoft.android.mplayer.lib.models.Video;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.DropBoxManager;
import maestro.filemanager.drives.OneDriveManager;
import maestro.filemanager.drives.YandexDriveManager;
import maestro.filemanager.drives.boxdrive.BoxDriveApi;
import maestro.filemanager.drives.yandexdrive.ListItem;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MArchiveFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.files.onedrive.BaseOneDriveObject;
import maestro.filemanager.utils.data.FileManagerDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by artyom on 9/11/14.
 */
public class Loaders {

    public static final String TAG = Loaders.class.getSimpleName();

    public static final class DirectoryFilesLoader extends AsyncTaskLoader<ArrayList<MFile>> {

        private MFile mFile;
        private String mPath;
        private AtomicBoolean isCanceled = new AtomicBoolean();
        private long startTime;

        public DirectoryFilesLoader(Context context, MFile file) {
            super(context);
            this.mFile = file;
        }

        public DirectoryFilesLoader(Context context, String path) {
            super(context);
            mPath = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            startTime = System.currentTimeMillis();
            forceLoad();
        }

        @Override
        protected void onForceLoad() {
            super.onForceLoad();
            isCanceled.set(false);
        }

        @Override
        public boolean cancelLoad() {
            isCanceled.set(true);
            return super.cancelLoad();
        }

        @Override
        public ArrayList<MFile> loadInBackground() {
            if (mFile == null && !TextUtils.isEmpty(mPath)) {
                if (mPath.contains(Utils.ZIP_DIVIDER)) {
                    String[] split = mPath.split(Utils.ZIP_DIVIDER);
                    MArchiveFile zipFile = MArchiveFile.create(new File(split[0]), isCanceled);
                    mFile = split.length == 1 ? zipFile : zipFile.getZipEntryFile(split[split.length - 1]);
                } else if (StorageUtils.isBookmarksFolder(mPath)) {
                    return FileManagerDatabase.Instance.collectBookmarks(isCanceled);
                } else {
                    mFile = new MJavaFile(new File(mPath));
                }
            }

            ArrayList<MFile> folders = new ArrayList<MFile>();
            ArrayList<MFile> files = new ArrayList<MFile>();
            if (mFile != null) {
                if (mFile instanceof MArchiveEntryFile) {
                    MArchiveEntryFile[] zipFiles = ((MArchiveEntryFile) mFile).list();
                    for (MArchiveEntryFile entryFile : zipFiles) {
                        if (!Settings.isShowHidden() && Utils.isHidden(entryFile.getTitle()))
                            continue;
                        if (entryFile.isFolder()) {
                            folders.add(entryFile);
                        } else {
                            files.add(entryFile);
                        }
                    }

                } else if (mFile.getPath().matches(Utils.ARCHIVE_PATTERN)) {
                    MArchiveFile mZipFile = MArchiveFile.create((File) mFile.getOriginalObject(), isCanceled);
                    if (mZipFile != null) {
                        MArchiveEntryFile[] zipFiles = mZipFile.list();
                        for (MArchiveEntryFile entryFile : zipFiles) {
                            if (!Settings.isShowHidden() && Utils.isHidden(entryFile.getTitle()))
                                continue;
                            if (entryFile.isFolder()) {
                                folders.add(entryFile);
                            } else {
                                files.add(entryFile);
                            }
                        }
                    }
                } else {
                    MJavaFile mJavaFile = (MJavaFile) mFile;
                    File[] dirFiles = mJavaFile.getOriginalObject().listFiles();
                    if (dirFiles != null) {
                        final int size = dirFiles.length;
                        for (int i = 0; i < size; i++) {
                            MJavaFile jFile = new MJavaFile(dirFiles[i]);
                            if (!Settings.isShowHidden() && Utils.isHidden(jFile.getTitle()))
                                continue;
                            if (jFile.isFolder()) {
                                folders.add(jFile);
                            } else {
                                files.add(jFile);
                            }
                        }
                    }
                }
            }

            sort(files, folders);

            long processTime = System.currentTimeMillis() - startTime;
            if (processTime < 125) {
                try {
                    Thread.currentThread().sleep(125 - processTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return folders;
        }

        public String getPath() {
            return mPath;
        }
    }

    public static final class ImageBucketsLoader extends AsyncTaskLoader<ArrayList<MediaBucket>> {

        public ImageBucketsLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<MediaBucket> loadInBackground() {
            ArrayList<MediaBucket> bucketList = MediaStoreHelper.getImageBuckets(getContext(), true);
            if (bucketList != null && bucketList.size() > 0) {
                sort(bucketList, null);
                return bucketList;
            }
            return null;
        }
    }

    public static final class MusicAlbumLoader extends AsyncTaskLoader<ArrayList<Album>> {

        public MusicAlbumLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Album> loadInBackground() {
            ArrayList<Album> albums = MediaStoreHelper.getAlbums(getContext(), null);
            if (albums != null && albums.size() > 0) {
                sort(albums, null);
                return albums;
            }
            return null;
        }
    }

    public static final class VideoLoader extends AsyncTaskLoader<ArrayList<Video>> {

        public VideoLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Video> loadInBackground() {
            ArrayList<Video> videos = MediaStoreHelper.getVideos(getContext(), null);
            if (videos != null && videos.size() > 0) {
                sort(videos, null);
                return videos;
            }
            return null;
        }
    }

    public static final class OneDriveLoader extends AsyncTaskLoader<ArrayList<BaseOneDriveObject>> {

        private String path;

        public OneDriveLoader(Context context, String path) {
            super(context);
            this.path = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<BaseOneDriveObject> loadInBackground() {
            Object object = OneDriveManager.getInstance().loadList(path);
            if (object instanceof ArrayList) {
                return (ArrayList<BaseOneDriveObject>) object;
            }
            return null;
        }

    }

    public static final class DropBoxDriveLoader extends AsyncTaskLoader<ArrayList<DropboxAPI.Entry>> {

        private String path;

        public DropBoxDriveLoader(Context context, String path) {
            super(context);
            this.path = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<DropboxAPI.Entry> loadInBackground() {
            Object object = DropBoxManager.getInstance().loadList(path);
            if (object instanceof ArrayList) {
                return (ArrayList<DropboxAPI.Entry>) object;
            }
            return null;
        }

    }

    public static final class GoogleDriveLoader extends AsyncTaskLoader<Object[]> {

        private String path;

        public GoogleDriveLoader(Context context, String path) {
            super(context);
            this.path = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object[] loadInBackground() {
//            Object object = GoogleDriveManager.getInstance().loadList(path);
//            if (object instanceof Metadata[]) {
//                return (Metadata[]) object;
//            }
            return null;
        }
    }

    public static final class YandexDriveLoader extends AsyncTaskLoader<ArrayList<ListItem>> {

        private String path;

        public YandexDriveLoader(Context context, String path) {
            super(context);
            this.path = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<ListItem> loadInBackground() {
            Object object = YandexDriveManager.getInstance().loadList(path);
            if (object instanceof ArrayList) {
                return (ArrayList<ListItem>) object;
            }
            return null;
        }
    }

    public static final class BoxDriveLoader extends AsyncTaskLoader<ArrayList<BoxFile>> {

        private String path;

        public BoxDriveLoader(Context context, String path) {
            super(context);
            this.path = path;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<BoxFile> loadInBackground() {
            Object result = BoxDriveApi.list(path);
            if (result instanceof ArrayList) {
                return (ArrayList<BoxFile>) result;
            }
            return null;
        }
    }

    public static final class DriveFolderOperationLoader<T> extends AsyncTaskLoader<Object> {

        public enum FolderOperation {
            CREATE, DELETE, RENAME
        }

        private FolderOperation mOperation;
        private String mName;
        private String id;
        private BaseDriveManager mDriveManager;
        private Object mResult;

        public DriveFolderOperationLoader(Context context, BaseDriveManager driveManager, FolderOperation operation, String name, String id) {
            super(context);
            this.id = id;
            mOperation = operation;
            mName = name;
            mDriveManager = driveManager;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            if (mOperation == FolderOperation.CREATE) {
                mResult = mDriveManager.createFolder(mName, id);
            } else if (mOperation == FolderOperation.DELETE) {
                mResult = mDriveManager.deleteFolder(id);
            } else if (mOperation == FolderOperation.RENAME) {
                mResult = mDriveManager.renameFolder(mName, id);
            }
            return null;
        }

        public Object getResult() {
            return mResult;
        }
    }

//Comparators

    public static final class AZMFileComparator implements Comparator<Object> {

        @Override
        public int compare(Object lhs, Object rhs) {
            String str1 = null, str2 = null;
            if (lhs instanceof MFile) {
                str1 = ((MFile) lhs).getTitle();
                str2 = ((MFile) rhs).getTitle();
            } else if (lhs instanceof BaseMediaItem) {
                str1 = ((BaseMediaItem) lhs).Title;
                str2 = ((BaseMediaItem) rhs).Title;
            } else if (lhs instanceof BaseOneDriveObject) {
                str1 = ((BaseOneDriveObject) lhs).getName();
                str2 = ((BaseOneDriveObject) rhs).getName();
            } else if (lhs instanceof DropboxAPI.Entry) {
                str1 = ((DropboxAPI.Entry) lhs).fileName();
                str2 = ((DropboxAPI.Entry) rhs).fileName();
            } else if (lhs instanceof ListItem) {
                str1 = ((ListItem) lhs).getName();
                str2 = ((ListItem) rhs).getName();
            } else if (lhs instanceof BoxFile) {
                str1 = ((BoxFile) lhs).getName();
                str2 = ((BoxFile) rhs).getName();
            }
            return Utils.compareStrings(str1, str2);
        }

    }

    public static final class SizeMFileComparator implements Comparator<Object> {

        @Override
        public int compare(Object lhs, Object rhs) {
            long size1 = 0, size2 = 0;
            if (lhs instanceof MFile) {
                size1 = ((MFile) lhs).getSize();
                size2 = ((MFile) rhs).getSize();
            } else if (lhs instanceof MediaBucket) {
                size1 = ((MediaBucket) lhs).getChildCount();
                size2 = ((MediaBucket) rhs).getChildCount();
            } else if (lhs instanceof Album) {
                size1 = ((Album) lhs).count;
                size2 = ((Album) rhs).count;
            } else if (lhs instanceof Video) {
                size1 = ((Video) lhs).Duration;
                size2 = ((Video) rhs).Duration;
            } else if (lhs instanceof BaseOneDriveObject) {
                size1 = ((BaseOneDriveObject) lhs).getSize();
                size2 = ((BaseOneDriveObject) rhs).getSize();
            } else if (lhs instanceof DropboxAPI.Entry) {
                size1 = ((DropboxAPI.Entry) lhs).bytes;
                size2 = ((DropboxAPI.Entry) rhs).bytes;
            } else if (lhs instanceof ListItem) {
                size1 = ((ListItem) lhs).getContentLength();
                size2 = ((ListItem) rhs).getContentLength();
            } else if (lhs instanceof BoxFile) {
                size1 = Long.valueOf(((BoxFile) lhs).getSize());
                size2 = Long.valueOf(((BoxFile) rhs).getSize());
            }
            return (int) (size1 - size2);
        }

    }

    public static final class DateMFileComparator implements Comparator<Object> {

        @Override
        public int compare(Object lhs, Object rhs) {
            Date date1, date2;
            if (lhs instanceof MFile) {
                date1 = new Date(((MFile) lhs).getDate());
                date2 = new Date(((MFile) rhs).getDate());
                return date1.compareTo(date2);
            }
            return 0;
        }

    }

    public static final class TypeMFileComparator implements Comparator<Object> {

        @Override
        public int compare(Object lhs, Object rhs) {
            String str1 = null, str2 = null;
            if (lhs instanceof MFile) {
                str1 = ((MFile) lhs).getExtension();
                str2 = ((MFile) rhs).getExtension();
            } else if (lhs instanceof BaseMediaItem) {
                str1 = Utils.getExtension(((BaseMediaItem) lhs).Title);
                str2 = Utils.getExtension(((BaseMediaItem) rhs).Title);
            } else if (lhs instanceof BaseOneDriveObject) {
                str1 = Utils.getExtension(((BaseOneDriveObject) lhs).getName());
                str2 = Utils.getExtension(((BaseOneDriveObject) rhs).getName());
            } else if (lhs instanceof DropboxAPI.Entry) {
                str1 = Utils.getExtension(((DropboxAPI.Entry) lhs).fileName());
                str2 = Utils.getExtension(((DropboxAPI.Entry) rhs).fileName());
            } else if (lhs instanceof ListItem) {
                str1 = Utils.getExtension(((ListItem) lhs).getName());
                str2 = Utils.getExtension(((ListItem) rhs).getName());
            } else if (lhs instanceof BoxFile) {
                str1 = Utils.getExtension(((BoxFile) lhs).getName());
                str2 = Utils.getExtension(((BoxFile) rhs).getName());
            }
            return Utils.compareStrings(str1, str2);
        }

    }

    public static final void sort(ArrayList files, ArrayList folders) {

        Comparator comparator = new AZMFileComparator();
        if (folders != null)
            Collections.sort(folders, comparator);
        if (files != null)
            Collections.sort(files, comparator);

        switch (Settings.getSortType()) {
            case DATE:
                comparator = new DateMFileComparator();
                if (folders != null)
                    Collections.sort(folders, comparator);
                if (files != null)
                    Collections.sort(files, comparator);
                break;
            case SIZE:
                comparator = new SizeMFileComparator();
                if (folders != null)
                    Collections.sort(folders, comparator);
                if (files != null)
                    Collections.sort(files, comparator);
                break;
            case TYPE:
                comparator = new TypeMFileComparator();
                if (folders != null)
                    Collections.sort(folders, comparator);
                if (files != null)
                    Collections.sort(files, comparator);
                break;
        }

        if (Settings.isSortInverse()) {
            if (folders != null)
                Collections.reverse(folders);
            if (files != null)
                Collections.reverse(files);
        }

        if (folders != null) {
            folders.addAll(Settings.isFolderFirst() ? folders.size() : 0, files);
        }

    }

}
