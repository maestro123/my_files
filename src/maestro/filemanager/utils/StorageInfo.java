package maestro.filemanager.utils;

/**
 * Created by artyom on 9/12/14.
 */
public class StorageInfo {

    public String Name;
    public long TotalSpace;
    public long UsedSpace;

    public StorageInfo() {
    }

    @Override
    public String toString() {
        return "StorageInfo: Name = " + Name + ", TotalSpace = " + TotalSpace + ", UsedSpace = " + UsedSpace;
    }
}
