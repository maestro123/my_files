package maestro.filemanager.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.RemoteViews;
import maestro.filemanager.R;

import java.lang.annotation.Annotation;

public class ProgressIndicator extends View implements RemoteViews.RemoteView {

    public static final String TAG = ProgressIndicator.class.getSimpleName();

    private static final int MSG_DELAY = 300;
    private static final int MSG_SET_PROGRESS = 0;

    private int type = 0;
    private float textSize = 0;
    private float secondaryTextSize = 0;
    private float strokeWidth = 10;
    private int progress = 0;
    private int currentProgress = 0;

    private int primaryColor = Color.WHITE;
    private int secondaryColor = Color.parseColor("#40ffffff");
    private int textColor = Color.parseColor("#40ffffff");
    private int secondaryTextColor = Color.BLACK;
    private Paint rTPaint = new Paint();
    private Paint rWPaint = new Paint();
    private Paint cTPaint = new Paint();
    private Paint cAPaint = new Paint();
    private Paint cWPaint = new Paint();
    private Paint stPaint = new Paint();
    private ProgressAnimationRunnable mProgressAnimation;

    private String secondaryText = null;
    private boolean useRound = false;
    private boolean useRoundMinimum = true;
    private boolean canAnimate = true;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_PROGRESS:
                    setProgress((Integer) msg.obj);
                    break;
            }
        }
    };

    public ProgressIndicator(Context context) {
        super(context);
    }

    public ProgressIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressIndicator(Context context, AttributeSet attrs,
                             int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ProgressIndicator, 0, 0);
        try {

            type = a.getInt(R.styleable.ProgressIndicator_type, 0);
            textSize = a.getDimension(R.styleable.ProgressIndicator_circleTextSize, 20f);
            textColor = a.getColor(R.styleable.ProgressIndicator_circleTextColor, textColor);
            primaryColor = a.getColor(R.styleable.ProgressIndicator_primaryColor, primaryColor);
            secondaryColor = a.getColor(R.styleable.ProgressIndicator_secondaryColor, secondaryColor);
            strokeWidth = a.getDimension(R.styleable.ProgressIndicator_circleStrokeWidth, strokeWidth);
            secondaryText = a.getString(R.styleable.ProgressIndicator_secondaryText);
            secondaryTextColor = a.getColor(R.styleable.ProgressIndicator_secondaryTextColor, secondaryTextColor);
            secondaryTextSize = a.getDimension(R.styleable.ProgressIndicator_secondaryTextSize, 20f);
            useRound = a.getBoolean(R.styleable.ProgressIndicator_useRound, false);
            useRoundMinimum = a.getBoolean(R.styleable.ProgressIndicator_useRoundMinimum, true);
        } finally {
            a.recycle();
        }

        applyColor();

    }

    final void applyColor() {
        rTPaint.setColor(secondaryColor);
        rWPaint.setColor(primaryColor);

        cTPaint.setColor(secondaryColor);
        cTPaint.setAntiAlias(true);
        cTPaint.setStyle(Style.STROKE);
        cTPaint.setStrokeWidth(strokeWidth);

        cAPaint.setAntiAlias(true);
        cAPaint.setStyle(Style.STROKE);
        cAPaint.setStrokeWidth(strokeWidth);
        cAPaint.setStrokeCap(Paint.Cap.BUTT);
        cAPaint.setColor(primaryColor);

        cWPaint.setColor(textColor);
        cWPaint.setAntiAlias(true);
        cWPaint.setTextAlign(Paint.Align.CENTER);
        cWPaint.setTextSize(textSize);

        stPaint.setColor(secondaryTextColor);
        stPaint.setTextSize(secondaryTextSize);
        stPaint.setAntiAlias(true);
        stPaint.setTextAlign(Paint.Align.CENTER);
    }

    public void setBothColors(int primaryColor, int secondaryColor) {
        setColors(primaryColor, secondaryColor, primaryColor, secondaryColor);
    }

    public void setColors(int primaryColor, int secondaryColor, int primaryTextColor, int secondaryTextColor) {
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.textColor = primaryTextColor;
        this.secondaryTextColor = secondaryTextColor;
        applyColor();
    }

    public void setTypeface(Typeface tf) {
        if (cWPaint != null) {
            cWPaint.setTypeface(tf);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        final int width = getWidth();
        final int height = getHeight();
        switch (type) {
            case 1:
                float strokeWidth = cTPaint.getStrokeWidth();
                canvas.drawCircle(width / 2, height / 2, Math.round(width / 2 - strokeWidth), cTPaint);

                float angle = 360 * currentProgress / 100;
                cAPaint.setStyle(Style.STROKE);
                canvas.drawArc(new RectF(strokeWidth, strokeWidth, width - strokeWidth, height - strokeWidth), -90,
                        angle, false, cAPaint);

                Point point = getPointOnArc(width / 2, height / 2, Math.round(width / 2 - strokeWidth), angle - 90);
                cAPaint.setStyle(Style.FILL);
                canvas.drawCircle(point.x, point.y, strokeWidth / 2, cAPaint);

                point = getPointOnArc(width / 2, height / 2, Math.round(width / 2 - strokeWidth), -90);
                cAPaint.setStyle(Style.FILL);
                canvas.drawCircle(point.x, point.y, strokeWidth / 2, cAPaint);

                String text = String.valueOf(progress) + "%";
                int xPos = (canvas.getWidth() / 2);
                float textHeight = (cWPaint.descent() + cWPaint.ascent()) / 2;
                int yPos = (int) ((canvas.getHeight() / 2) - textHeight);
                canvas.drawText(text, xPos, yPos, cWPaint);

                if (secondaryText != null) {
                    yPos -= textHeight / 4;
                    canvas.drawText(secondaryText, xPos, yPos, stPaint);
                }
                break;
            default:
                final int round = useRound ? height / 2 : 0;
                canvas.drawRoundRect(new RectF(0, 0, width, height), round, round, rTPaint);
                int drawWidth = ((width * currentProgress) / 100);
                canvas.drawRoundRect(new RectF(0, 0, drawWidth < height && useRoundMinimum
                        ? height : drawWidth, height), round, round, rWPaint);
                break;
        }
    }

    private Point getPointOnArc(int circleCeX, int circleCeY, int circleRadius, float endAngle) {
        Point point = new Point();
        double endAngleRadian = endAngle * (Math.PI / 180);

        int pointX = (int) Math.round((circleCeX + circleRadius * Math.cos(endAngleRadian)));
        int pointY = (int) Math.round((circleCeY + circleRadius * Math.sin(endAngleRadian)));

        point.x = pointX;
        point.y = pointY;

        return point;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int minw = getPaddingLeft() + getPaddingRight()
                + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);
        int h = resolveSizeAndState(MeasureSpec.getSize(w), heightMeasureSpec,
                0);
        setMeasuredDimension(w, h);
    }

    public void drop() {
        mHandler.removeMessages(MSG_SET_PROGRESS);
        progress = 0;
        currentProgress = 0;
        if (mProgressAnimation != null)
            mProgressAnimation.cancel();
        invalidate();
    }

    public void setProgressDelayed(int progress) {
        setProgress(progress);
//        mHandler.removeMessages(MSG_SET_PROGRESS);
//        mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_PROGRESS, progress), MSG_DELAY);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        if (canAnimate) {
            if (mProgressAnimation != null)
                mProgressAnimation.cancel();
            compatPostOnAnimation(mProgressAnimation = new ProgressAnimationRunnable(currentProgress, progress));
        } else {
            currentProgress = progress;
            invalidate();
        }
    }

    public void setCanAnimate(boolean animate) {
        canAnimate = animate;
    }

    public void setSecondaryText(String text) {
        secondaryText = text;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }


    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }


    private void compatPostOnAnimation(Runnable runnable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            postOnAnimation(runnable);
        } else {
            postDelayed(runnable, 1000 / 60);
        }
    }

    private class ProgressAnimationRunnable implements Runnable {

        private static final float ANIMATE_TIME = 300;
        private long startTime;
        private int startProgress;
        private int endProgress;
        private boolean isCanceled;
        private DecelerateInterpolator interpolator = new DecelerateInterpolator(1.5f);

        ProgressAnimationRunnable(int startProgress, int endProgress) {
            startTime = System.currentTimeMillis();
            this.startProgress = startProgress;
            this.endProgress = endProgress;
        }

        @Override
        public void run() {
            float t = interpolate();
            currentProgress = startProgress + Math.round((endProgress - startProgress) * t);
            if (t >= 1f || isCanceled) {
                invalidate();
            } else {
                invalidate();
                compatPostOnAnimation(mProgressAnimation = this);
            }
        }

        private float interpolate() {
            long currTime = System.currentTimeMillis();
            float elapsed = (currTime - startTime) / ANIMATE_TIME;
            elapsed = Math.min(1f, elapsed);
            return interpolator.getInterpolation(elapsed);
        }

        public void cancel() {
            isCanceled = true;
        }

    }

}
