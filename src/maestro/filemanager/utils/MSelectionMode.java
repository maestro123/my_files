package maestro.filemanager.utils;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.*;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.Main;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by Artyom on 6/28/2015.
 */
public class MSelectionMode implements ActionMode.Callback {

    private AppCompatActivity mActivity;
    private ActionMode mMode;
    private Menu mMenu;
    private ViewGroup mList;
    private BaseUpdateAdapter mAdapter;
    private SVGHelper.SVGHolder mSvgHolder;
    private OnSelectionModeEventListener mListener;
    private int mSelectionCount;

    public interface OnSelectionModeEventListener {
        void onSelectionModeEvent(EVENT event);
    }

    public enum EVENT {
        START, FINISH
    }

    public MSelectionMode(AppCompatActivity activity, ViewGroup list, BaseUpdateAdapter adapter) {
        mActivity = activity;
        mList = list;
        mAdapter = adapter;
        mSvgHolder = ((FileManagerApplication) activity.getApplication()).getSvgHolder();
    }

    public void setOnSelectionModeEventListener(OnSelectionModeEventListener listener) {
        mListener = listener;
    }

    public void start(Object item) {
        mActivity.startSupportActionMode(this);
        mAdapter.startSelectionMode(mList, this, item);
    }

    public void finish() {
        if (mMode != null) {
            mMode.finish();
        }
    }

    public void setCount(int count) {
        if (mMode != null) {
            mMode.setTitle(new StringBuilder().append(count));
        }
        mSelectionCount = count;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        if (mActivity != null) {
            Window mWindow = mActivity.getWindow();
            mWindow.findViewById(R.id.action_mode_bar).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            if (mActivity instanceof Main) {
                ((Main) mActivity).setDrawerState(false);
            }
        }
        mMode = actionMode;
        mMenu = menu;
        setCount(mSelectionCount);
        actionMode.getMenuInflater().inflate(R.menu.selection_menu, menu);
        if (mListener != null) {
            mListener.onSelectionModeEvent(EVENT.START);
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        menu.findItem(R.id.delete).setIcon(mSvgHolder.getDrawable(R.raw.delete, Color.WHITE));
        menu.findItem(R.id.cut).setIcon(mSvgHolder.getDrawable(R.raw.cut, Color.WHITE));
        menu.findItem(R.id.copy).setIcon(mSvgHolder.getDrawable(R.raw.copy, Color.WHITE));
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        if (mMode != null) {
            mAdapter.finishSelectionMode(mList);
            mMode = null;
        }
        if (mActivity instanceof Main) {
            ((Main) mActivity).setDrawerState(true);
        }
        if (mListener != null) {
            mListener.onSelectionModeEvent(EVENT.FINISH);
        }
    }

    public boolean isShowing() {
        return mMode != null;
    }
}
