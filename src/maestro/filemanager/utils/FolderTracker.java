package maestro.filemanager.utils;

import android.os.FileObserver;
import android.os.Handler;

import java.io.File;

/**
 * Created by Artyom on 2/24/2015.
 */
public class FolderTracker extends FileObserver {

    public static final String TAG = FolderTracker.class.getSimpleName();

    private static final long DELAY = 150;

    public enum FOLDER_TRACKER_EVENT {
        DELETE, CREATE
    }

    public interface OnFolderTrackerEvent {
        public void onFolderTrackerEvent(FOLDER_TRACKER_EVENT event, String path);

        public boolean containsFile(File file);

        public boolean canNotify();
    }

    private String mPath;
    private OnFolderTrackerEvent mEventListener;
    private Thread mNotifyThread;
    private String mEventPath;
    private FOLDER_TRACKER_EVENT mCurrentEvent;
    private Object mLock = new Object();

    private static final Handler mHandler = new Handler();

    static {
        mHandler.getLooper();
    }

    public FolderTracker(String path, OnFolderTrackerEvent event, int mask) {
        super(path, mask);
        mPath = path;
        mEventListener = event;
    }

    @Override
    public void startWatching() {
        super.startWatching();
        mNotifyThread = new Thread() {
            @Override
            public void run() {
                super.run();
                while (mNotifyThread != null) {
                    if (mCurrentEvent != null && mEventListener != null && mEventListener.canNotify()) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized (mLock) {
                                    mEventListener.onFolderTrackerEvent(mCurrentEvent, mEventPath);
                                    mCurrentEvent = null;
                                }
                            }
                        });
                    }
                    try {
                        sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        mNotifyThread.start();
    }

    @Override
    public void stopWatching() {
        super.stopWatching();
        mNotifyThread = null;
    }

    @Override
    public void onEvent(int event, String path) {
        if (mEventListener != null && path != null) {
            File file = new File(mPath, path);

            FOLDER_TRACKER_EVENT trackerEvent = null;
            switch (event) {
                case DELETE:
                    trackerEvent = FOLDER_TRACKER_EVENT.DELETE;
                    break;
                case CREATE:
                    trackerEvent = FOLDER_TRACKER_EVENT.CREATE;
                    break;
            }

            if (!file.exists() && mEventListener.containsFile(file)) {
                trackerEvent = FOLDER_TRACKER_EVENT.DELETE;
            } else if (file.exists() && !mEventListener.containsFile(file)) {
                trackerEvent = FOLDER_TRACKER_EVENT.CREATE;
            }

            if (trackerEvent != null) {
                synchronized (mLock) {
                    mEventPath = path;
                    mCurrentEvent = trackerEvent;
                }
            }
        }
    }

    final String asString(int event) {
        switch (event) {
            case ACCESS:
                return "ACCESS";
            case MODIFY:
                return "MODIFY";
            case ATTRIB:
                return "ATTRIB";
            case CLOSE_WRITE:
                return "CLOSE_WRITE";
            case CLOSE_NOWRITE:
                return "CLOSE_NOWRITE";
            case OPEN:
                return "OPEN";
            case MOVED_FROM:
                return "MOVED_FROM";
            case MOVED_TO:
                return "MOVED_TO";
            case CREATE:
                return "CREATE";
            case DELETE:
                return "DELETE";
            case DELETE_SELF:
                return "DELETE_SELF";
            case MOVE_SELF:
                return "MOVE_SELF";
        }
        return null;
    }

}
