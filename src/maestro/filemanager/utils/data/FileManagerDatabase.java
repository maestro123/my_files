package maestro.filemanager.utils.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import maestro.filemanager.files.MFile;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by artyom on 12/27/14.
 */
public class FileManagerDatabase extends SQLiteOpenHelper {

    public static FileManagerDatabase Instance;

    private static final String DATABASE_NAME = "fm.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_BOOKMARKS = "tbl_bookmarks";

    public static final String PATH = "path";
    public static final String TABLE_BOOKMARKS_CREATE = "create table " + TABLE_BOOKMARKS + "(" + PATH + " text not null" + ");";

    public FileManagerDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Instance = this;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_BOOKMARKS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: implement migrate
    }

    private ArrayList<MFile> mBookmarks = new ArrayList<>();

    public void addBookmark(MFile file) {
        ContentValues values = new ContentValues(1);
        values.put(PATH, makeString(file.getPath()));
        getWritableDatabase().insert(TABLE_BOOKMARKS, null, values);
        mBookmarks.add(file);
    }

    public void removeBookmark(MFile file) {
        getWritableDatabase().delete(TABLE_BOOKMARKS, PATH + "=?", new String[]{makeString(file.getPath())});
        mBookmarks.remove(file);
    }

    private final String makeString(String string) {
        return !TextUtils.isEmpty(string) ? new StringBuilder().append("'").append(string).append("'").toString() : null;
    }

    private final String fromString(String string) {
        return !TextUtils.isEmpty(string) ? string.substring(1, string.length() - 1) : null;
    }

    public boolean isInBookmarks(MFile file) {
        if (mBookmarks != null) {
            return mBookmarks.contains(file);
        } else {
            Cursor cursor = getReadableDatabase().query(TABLE_BOOKMARKS, new String[]{PATH}, PATH + "=?", new String[]{makeString(file.getPath())}, null, null, null);
            return cursor != null && cursor.getCount() > 0;
        }
    }

    public ArrayList<MFile> collectBookmarks(AtomicBoolean isCanceled) {
        if (mBookmarks == null) {
            mBookmarks = new ArrayList<>();
            Cursor cursor = getReadableDatabase().query(TABLE_BOOKMARKS, null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                ArrayList<String> forRemove = new ArrayList<>();
                cursor.moveToFirst();
                do {
                    String path = cursor.getString(cursor.getColumnIndexOrThrow(PATH));
                    MFile file = MFile.fromPath(fromString(path), isCanceled);
                    if (file == null || !file.isExist()) {
                        forRemove.add(path);
                    } else {
                        mBookmarks.add(file);
                    }
                } while (cursor.moveToNext());
                if (forRemove.size() > 0) {
                    getWritableDatabase().delete(TABLE_BOOKMARKS, PATH + "=?", forRemove.toArray(new String[forRemove.size()]));
                }
            }
        }
        return mBookmarks;
    }

}
