package maestro.filemanager.utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import maestro.filemanager.R;

import java.io.*;
import java.util.*;

/**
 * Created by artyom on 9/11/14.
 */
public class StorageUtils {

    public static final String BOOKMARKS_FILSE = "dbMBookmarks";

    private static final String TAG = StorageUtils.class.getSimpleName();
    private static List<StorageInfo> mCachedInfos = new ArrayList<>();

    public static class StorageInfo {

        public final String path;
        public final boolean readonly;
        public final boolean removable;
        public final int number;

        StorageInfo(String path, boolean readonly, boolean removable, int number) {
            this.path = path;
            this.readonly = readonly;
            this.removable = removable;
            this.number = number;
        }

        public String getDisplayName(Context context) {
            StringBuilder res = new StringBuilder();
            if (!removable) {
                res.append(context.getString(R.string.internal_storage));
            } else if (number > 1) {
                res.append(context.getString(R.string.sd_card)).append(' ').append(number);
            } else {
                res.append(context.getString(R.string.sd_card));
            }
            if (readonly) {
                res.append(' ').append("(").append(context.getString(R.string.read_only)).append(')');
            }
            return res.toString();
        }
    }

    public static List<StorageInfo> getStorageList() {
        synchronized (mCachedInfos) {
            mCachedInfos.clear();
            String def_path = Environment.getExternalStorageDirectory().getPath();
            boolean def_path_removable = Environment.isExternalStorageRemovable();
            String def_path_state = Environment.getExternalStorageState();
            boolean def_path_available = def_path_state.equals(Environment.MEDIA_MOUNTED)
                    || def_path_state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
            boolean def_path_readonly = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);

            HashSet<String> paths = new HashSet<String>();
            int cur_removable_number = 1;

            if (def_path_available) {
                paths.add(def_path);
                mCachedInfos.add(0, new StorageInfo(def_path, def_path_readonly, def_path_removable, def_path_removable ? cur_removable_number++ : -1));
            }

            BufferedReader buf_reader = null;
            try {
                buf_reader = new BufferedReader(new FileReader("/proc/mounts"));
                String line;
                Log.d(TAG, "/proc/mounts");
                while ((line = buf_reader.readLine()) != null) {
                    Log.d(TAG, line);
                    if (line.contains("vfat") || line.contains("/mnt")) {
                        StringTokenizer tokens = new StringTokenizer(line, " ");
                        String unused = tokens.nextToken(); //device
                        String mount_point = tokens.nextToken(); //mount point
                        if (paths.contains(mount_point)) {
                            continue;
                        }
                        unused = tokens.nextToken(); //file system
                        List<String> flags = Arrays.asList(tokens.nextToken().split(",")); //flags
                        boolean readonly = flags.contains("ro");

                        if (line.contains("/dev/block/vold")) {
                            if (!line.contains("/mnt/secure")
                                    && !line.contains("/mnt/asec")
                                    && !line.contains("/mnt/obb")
                                    && !line.contains("/dev/mapper")
                                    && !line.contains("tmpfs")) {
                                if (mount_point.contains("media_rw")) {
                                    File workFile = new File(mount_point);
                                    if (!workFile.exists()) {
                                        mount_point = mount_point.replace("media_rw/", "");
                                        if (!(workFile = new File(mount_point)).exists())
                                            continue;
                                    }
                                }
                                paths.add(mount_point);
                                mCachedInfos.add(new StorageInfo(mount_point, readonly, true, cur_removable_number++));
                            }
                        }
                    }
                }

            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (buf_reader != null) {
                    try {
                        buf_reader.close();
                    } catch (IOException ex) {
                    }
                }
            }
            return mCachedInfos;
        }
    }

    public static StorageInfo getInfo(String path) {
        synchronized (mCachedInfos) {
            if (!TextUtils.isEmpty(path)) {
                if (mCachedInfos.size() == 0) {
                    getStorageList();
                }
                for (StorageInfo info : mCachedInfos) {
                    if (info.path.equals(path)) {
                        return info;
                    }
                }
            }
            return null;
        }
    }

    public static String getDownloadFolderPath() {
        File file = new File(Environment.getExternalStorageDirectory(), "Download");
        return file.exists() ? file.getPath() : null;
    }

    public static boolean isDownloadFolder(String path) {
        return path.equals(getDownloadFolderPath());
    }

    public static boolean isBookmarksFolder(String path) {
        return path.equals(BOOKMARKS_FILSE);
    }

}
