package maestro.filemanager.utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by artyom on 5.4.15.
 */
public class SearchHelper {

    public static class SearchParams {

        private String Path;
        private String Query;

        public SearchParams() {
        }

        public SearchParams path(String path) {
            Path = path;
            return this;
        }

        public SearchParams query(String query) {
            Query = query != null ? query.toLowerCase() : query;
            return this;
        }

    }

    public static abstract class SearchTask<T> extends AsyncTaskLoader<T> {

        private SearchParams mParams;

        public SearchTask(Context context, SearchParams params) {
            super(context);
            mParams = params;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();

            if (TextUtils.isEmpty(mParams.Query)) {
                deliverResult(null);
            } else {
                forceLoad();
            }
        }

        public SearchParams getParams() {
            return mParams;
        }

    }

    public static class StorageFileSearcher extends SearchTask<Object> {

        public static final String TAG = StorageFileSearcher.class.getSimpleName();

        public StorageFileSearcher(Context context, SearchParams params) {
            super(context, params);
        }

        @Override
        public Object loadInBackground() {
            File file = new File(getParams().Path);

            if (file.exists()) {
                File[] files = file.listFiles();
                if (files != null && files.length > 0) {
                    ArrayList<MFile> startMath = new ArrayList<>();
                    ArrayList<MFile> match = new ArrayList<>();
                    for (File f : files) {
                        String name = f.getName().toLowerCase();
                        if (name.startsWith(getParams().Query)) {
                            startMath.add(new MJavaFile(f));
                        } else if (name.contains(getParams().Query)) {
                            match.add(new MJavaFile(f));
                        }
                    }
                    if (startMath.size() > 0) {
                        Collections.sort(startMath, new Loaders.AZMFileComparator());
                        return startMath;
                    } else if (match.size() > 0) {
                        Collections.sort(match, new Loaders.AZMFileComparator());
                        return match;
                    }
                }
            }
            return null;
        }
    }

}
