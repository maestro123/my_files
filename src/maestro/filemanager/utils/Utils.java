package maestro.filemanager.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.Toast;
import com.dropbox.client2.DropboxAPI;
import com.msoft.android.mplayer.lib.models.Image;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.yandexdrive.ListItem;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.files.onedrive.BaseOneDriveObject;
import maestro.filemanager.files.onedrive.OneDriveAlbum;
import maestro.filemanager.files.onedrive.OneDriveFolder;
import maestro.filemanager.files.onedrive.OneDrivePhoto;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by artyom on 9/11/14.
 */
public class Utils {

    public static final String TAG = Utils.class.getSimpleName();

    public static final String MIM_ICON_KEY = "mim_icon";
    public static final String MIM_STORAGE_ICON = "mim_storage_icon";

    public static final String ARCHIVE_PATTERN = "^.+(zip|cbz|rar|cbr)$";
    public static final String ZIP_PATTERN = "^.+(zip|cbz|apk)$";
    public static final String RAR_PATTERN = "^.+(rar|cbr)$";
    public static final String ADOBE_BOOKS_PATTERN = "^.+(pdf|acsm|acsm_epub|acsm_pdf)$";
    public static final String BOOK_PATTERN = "^.+(fb2|fb2.zip|epub)$";
    public static final String TEXT_PATTERN = "^.+(doc|docx|rtf|txt)$";
    public static final String IMAGE_PATTERN = "^.+(jpg|jpeg|png|gif|svg|bmp|ico)$";
    public static final String MUSIC_PATTERN = "^.+(mp3|ogg)$";
    public static final String VIDEO_PATTERN = "^.+(mp4|avi|3gp|mpeg)$";
    public static final String APK_PATTERN = "^.+(apk)$";
    public static final String SVG_PATTERN = "^.+(svg)$";
    public static final String ZIP_DIVIDER = "_z-z_-z_0x01-0x02";
    public static final String RAR_DIVIDER = "_r-r_-r_0x01-0x02";
    private static final int COLOR_FOLDER = Color.parseColor("#626161");
    private static final int COLOR_ARCHIVE = Color.parseColor("#903f98");
    private static final int COLOR_ADOBE_BOOKS = Color.parseColor("#e14f5d");
    private static final int COLOR_TEXT = Color.parseColor("#4cb6dc");
    private static final int COLOR_IMAGE = Color.parseColor("#48cfae");
    private static final int COLOR_MUSIC = Color.parseColor("#ffce55");
    private static final int COLOR_UNKNOWN = Color.parseColor("#cccccc");
    private static final Pattern matchPattern = Pattern.compile(IMAGE_PATTERN);
    private static final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
    private static final String DATE_FORMAT = "MMM dd, yyy";
    private static final SimpleDateFormat UTC_DRIVE_DATE_FROMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public static final String getExtension(String path) {
        String[] splited = path.split("[/]");
        if (splited.length == 0)
            return null;
        String last = splited[splited.length - 1];
        int lastIndex = last.lastIndexOf('.');
        return lastIndex != -1 ? last.substring(lastIndex) : null;
    }

    public static String getTitleFromZipEntry(String name) {
        try {
            String[] splited = name.split("/");
            return splited[splited.length - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getIconColor(Object file) {
        if (file instanceof BaseOneDriveObject) {
            return getIconColor(getExtension(((BaseOneDriveObject) file).getName()));
        } else if (file instanceof DropboxAPI.Entry) {
            return getIconColor(getExtension(((DropboxAPI.Entry) file).fileName()));
        } else if (file instanceof ListItem) {
            return getIconColor(getExtension(((ListItem) file).getName()));
        } else if (file instanceof BoxFile) {
            return getIconColor(getExtension(((BoxFile) file).getName()));
        } else if (file instanceof MFile) {
            return getIconColor(((MFile) file).getExtension());
        }
        return -1;
    }

    public static int getIconColor(String extension) {
        if (extension == null) {
            return COLOR_UNKNOWN;
        } else if (extension.equals("folder")) {
            return COLOR_FOLDER;
        } else if (extension.matches(ARCHIVE_PATTERN)) {
            return COLOR_ARCHIVE;
        } else if (extension.matches(BOOK_PATTERN)) {
            return COLOR_TEXT;
        } else if (extension.matches(ADOBE_BOOKS_PATTERN)) {
            return COLOR_ADOBE_BOOKS;
        } else if (extension.matches(TEXT_PATTERN)) {
            return COLOR_TEXT;
        } else if (extension.matches(IMAGE_PATTERN)) {
            return COLOR_IMAGE;
        } else if (extension.matches(MUSIC_PATTERN)) {
            return COLOR_MUSIC;
        }
        return COLOR_UNKNOWN;
    }

    public static boolean isFileIconSupport(Object file) {
        if (file instanceof BaseOneDriveObject) {
            return isFileIconSupport((BaseOneDriveObject) file);
        } else if (file instanceof DropboxAPI.Entry) {
            return isFileIconSupport((DropboxAPI.Entry) file);
        } else if (file instanceof ListItem) {
            return isFileIconSupport((ListItem) file);
        } else if (file instanceof BoxFile) {
            return isFileIconSupport((BoxFile) file);
        } else if (file instanceof MFile) {
            return isFileIconSupport(((MFile) file).getExtension());
        }
        return false;
    }

    public static boolean isFileIconSupport(BaseOneDriveObject object) {
        return object instanceof OneDrivePhoto;
    }

    public static boolean isFileIconSupport(DropboxAPI.Entry entry) {
        return entry.thumbExists;
    }

    public static boolean isFileIconSupport(ListItem object) {
        return object.hasThumbnail();
    }

    public static boolean isFileIconSupport(BoxFile file) {
        return match(file.getName(), IMAGE_PATTERN) || match(file.getName(), VIDEO_PATTERN);
    }

    public static boolean isFileIconSupport(String path) {
        return match(path, IMAGE_PATTERN) || match(path, APK_PATTERN) || match(path, VIDEO_PATTERN) || match(path, MUSIC_PATTERN);
    }

    public static String readableFileSize(long size) {
        if (size <= 0)
            return null;
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static boolean match(String extension, String pattern) {
        return extension != null && matchPattern.compile(pattern).matcher(extension.toLowerCase()).matches();
    }

    public static boolean isVisibleFile(String name) {
        return !TextUtils.isEmpty(name) && !name.startsWith(".");
    }

    public static final void hideInput(Activity activity, EditText mEditText) {
        mEditText.clearFocus();
        InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static final void showInput(Activity activity, EditText editText) {
        editText.requestFocus();
        InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.showSoftInput(editText, 0);
    }

    public static final void processClick(Activity activity, MJavaFile file) {
        try {
            MimeTypeMap map = MimeTypeMap.getSingleton();
            String ext = MimeTypeMap.getFileExtensionFromUrl(file.getTitle());
            String type = map.getMimeTypeFromExtension(ext);
            if (type == null) {
                String extension = Utils.getExtension(file.getTitle());
                if (extension != null) {
                    type = "file://" + file.getPath();
                } else {
                    type = "*/*";
                }
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.fromFile(file.getOriginalObject());
            intent.setDataAndType(data, type);
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(activity, activity.getString(R.string.no_application_for_handle), Toast.LENGTH_SHORT).show();
            //TODO: show picker dialog
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, activity.getString(R.string.cant_open_file), Toast.LENGTH_SHORT).show();
        }
    }

    public static int compareStrings(String str1, String str2) {
        boolean firstEmpty = TextUtils.isEmpty(str1);
        boolean secondEmpty = TextUtils.isEmpty(str2);
        if (firstEmpty && !secondEmpty) {
            return 1;
        } else if (secondEmpty && !firstEmpty) {
            return -1;
        } else if (firstEmpty && secondEmpty) {
            return 0;
        } else {
            return str1.compareToIgnoreCase(str2.toLowerCase());
        }
    }

    public static boolean isHidden(String path) {
        if (TextUtils.isEmpty(path)) {
            return false;
        }
        return path.startsWith(".");
    }

    public static boolean isCloudObject(Object object) {
        return object instanceof BaseOneDriveObject || object instanceof DropboxAPI.Entry
                || object instanceof ListItem || object instanceof BoxFile;
    }

    public static final String getKey(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return ((BaseOneDriveObject) item).getId();
        } else if (item instanceof DropboxAPI.Entry) {
            return ((DropboxAPI.Entry) item).path;
        } else if (item instanceof ListItem) {
            return ((ListItem) item).getFullPath();
        } else if (item instanceof BoxFile) {
            return ((BoxFile) item).getId();
        } else if (item instanceof MFile) {
            return ((MFile) item).getPath();
        } else if (item instanceof Image) {
            return ((Image) item).Path;
        }
        return null;
    }

    public static final boolean isFolder(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return item instanceof OneDriveFolder || item instanceof OneDriveAlbum;
        } else if (item instanceof DropboxAPI.Entry) {
            return ((DropboxAPI.Entry) item).isDir;
        } else if (item instanceof ListItem) {
            return ((ListItem) item).isCollection();
        } else if (item instanceof BoxFile) {
            return ((BoxFile) item).isFolder();
        } else if (item instanceof MFile) {
            return ((MFile) item).isFolder();
        }
        return false;
    }

    public static final File getDirectoryFile(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return Settings.getOneDriveDirectory();
        } else if (item instanceof DropboxAPI.Entry) {
            return Settings.getDropBoxDirectory();
        } else if (item instanceof ListItem) {
            return Settings.getYandexDriveDirectory();
        } else if (item instanceof BoxFile) {
            return Settings.getBoxDirectory();
        }
        return null;
    }

    public static final String getName(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return ((BaseOneDriveObject) item).getName();
        } else if (item instanceof DropboxAPI.Entry) {
            return ((DropboxAPI.Entry) item).fileName();
        } else if (item instanceof ListItem) {
            return ((ListItem) item).getName();
        } else if (item instanceof BoxFile) {
            return ((BoxFile) item).getName();
        } else if (item instanceof MFile) {
            return ((MFile) item).getTitle();
        }
        return null;
    }

    public static final long getSize(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return ((BaseOneDriveObject) item).getSize();
        } else if (item instanceof DropboxAPI.Entry) {
            return ((DropboxAPI.Entry) item).bytes;
        } else if (item instanceof ListItem) {
            return ((ListItem) item).getContentLength();
        } else if (item instanceof BoxFile) {
            return Long.parseLong(((BoxFile) item).getSize());
        } else if (item instanceof MFile) {
            return ((MFile) item).getSize();
        }
        return 0;
    }

    public static final String getPath(Object item) {
        if (item instanceof BaseOneDriveObject) {
            return ((BaseOneDriveObject) item).getId();
        } else if (item instanceof DropboxAPI.Entry) {
            return ((DropboxAPI.Entry) item).path;
        } else if (item instanceof ListItem) {
            return ((ListItem) item).getFullPath();
        } else if (item instanceof BoxFile) {
            return ((BoxFile) item).getId();
        } else if (item instanceof MFile) {
            return ((MFile) item).getPath();
        }
        return null;
    }

    public static final CharSequence getDate(Object item) {
        try {
            if (item instanceof BaseOneDriveObject) {
                return DateFormat.format(DATE_FORMAT, UTC_DRIVE_DATE_FROMAT.parse(fixIso8601TimeZone((((BaseOneDriveObject) item).getCreatedTime()))));
            } else if (item instanceof DropboxAPI.Entry) {
                return DateFormat.format(DATE_FORMAT, new Date(((DropboxAPI.Entry) item).modified));
            } else if (item instanceof ListItem) {
                return DateFormat.format(DATE_FORMAT, ((ListItem) item).getLastUpdated());
            } else if (item instanceof BoxFile) {
                return DateFormat.format(DATE_FORMAT, UTC_DRIVE_DATE_FROMAT.parse(fixIso8601TimeZone((((BoxFile) item).getCreateTime()))));
            } else if (item instanceof MFile) {
                return DateFormat.format(DATE_FORMAT, ((MFile) item).getDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String fixIso8601TimeZone(String dateString) {
        if (dateString.length() >= 24 && dateString.charAt(22) == ':') {
            return dateString.substring(0, 22) + dateString.substring(23);
        }
        return dateString;
    }

    public enum ERROR {
        CONNECTION, UNKNOWN, NOT_AUTHORIZED
    }

}
