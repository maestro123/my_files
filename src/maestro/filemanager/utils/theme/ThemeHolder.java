package maestro.filemanager.utils.theme;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.utils.Typefacer;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by Artyom on 3/15/2015.
 */
public class ThemeHolder {

    public static int PrimaryColor = Color.parseColor("#3093bd");
    public static int SecondaryColor = Color.BLACK;
    public static int IconColor = Color.parseColor("#727272");
    public static int IconPrimaryColor = Color.parseColor("#3093bd");
    public static int IconSecondaryColor = Color.parseColor("#757575");
    public static int TextPrimaryColor = Color.parseColor("#212121");
    public static int TextSecondaryColor = Color.parseColor("#727272");

    public static void applyStyle(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup layout = (ViewGroup) view;
            final int childCount = layout.getChildCount();
            for (int i = 0; i < childCount; i++) {
                applyStyle(layout.getChildAt(i));
            }
        } else {
            //TODO: apply
        }
    }

    public static void applyDialogButtons(ViewGroup view) {
        Button ok = (Button) view.findViewById(R.id.btn_ok);
        Button cancel = (Button) view.findViewById(R.id.btn_cancel);
        if (ok != null) {
            ok.setTypeface(Typefacer.rMedium);
            ok.setTextColor(getPrimaryColor());
        }
        if (cancel != null) {
            cancel.setTypeface(Typefacer.rMedium);
            cancel.setTextColor(getPrimaryColor());
        }
    }

    public static void applyEmptyView(View view, SVGHelper.SVGHolder svgHolder) {
        final ImageView icon = (ImageView) view.findViewById(R.id.empty_icon);
        final TextView title = (TextView) view.findViewById(R.id.empty_title);
        if (icon != null) {
            svgHolder.applySVG(icon, R.raw.ic_mood_bad, getIconColor(), 8f);
        }
        if (title != null) {
            title.setTypeface(Typefacer.rMediumItalic);
        }
    }

    public static int getPrimaryColor() {
        return PrimaryColor;
    }

    public static int getIconColor() {
        return IconColor;
    }

    public static int getTextPrimaryColor() {
        return TextPrimaryColor;
    }

    public static int getTextSecondaryColor() {
        return TextSecondaryColor;
    }

    public static int getActionBarIconColor() {
        return Color.parseColor("#8a000000");
    }
}
