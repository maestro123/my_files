package maestro.filemanager.utils;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.support.v1.svg.SVG;

/**
 * Created by Artyom on 4/29/2015.
 */
public class ToastHelper {

    private static FileManagerApplication mApplication;

    public static void initialize(FileManagerApplication application) {
        mApplication = application;
    }

    public static void show(int msg, boolean error) {
        show(mApplication.getString(msg), error);
    }

    public static void show(String msg, boolean error) {
        Toast toast = new Toast(mApplication);
        final View v = View.inflate(mApplication, R.layout.toast_view, null);
        final TextView title = (TextView) v.findViewById(R.id.title);

        title.setBackgroundResource(error ? R.drawable.toast_frame_red : R.drawable.toast_frame);
        title.setText(msg);

        SVG icon = mApplication.getSvgHolder().getDrawable(error ? R.raw.ic_mood_bad : R.raw.ic_mood, Color.WHITE);
        title.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        title.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);

        toast.setView(v);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

}
