package maestro.filemanager.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import maestro.filemanager.R;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.DriveDUManager;
import maestro.filemanager.drives.DriveDUObject;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MArchiveFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by artyom on 9/11/14.
 */
public class GlobalCache {

    public static final String TAG = GlobalCache.class.getSimpleName();

    private static final HashMap<String, ActionObject> mCache = new HashMap<String, ActionObject>();
    private static final ArrayList<OnGlobalCacheChangeListener> mListeners = new ArrayList<>();
    private static final ArrayList<OnGlobalCacheEventListener> mEventsListener = new ArrayList<>();
    private static final ArrayList<OnGlobalCacheActionStatusChangeListener> mStatusListeners = new ArrayList<>();

    private static final HashMap<String, MArchiveFile> mArchiveCache = new HashMap<>();

    public static ActionObject get(String key) {
        return mCache.get(key);
    }

    public static ActionObject getAt(int position) {
        return position > -1 && position < mCache.size() ? mCache.get(new ArrayList<>(mCache.keySet()).get(position)) : null;
    }

    public static void remove(String key) {
        mCache.remove(key);
        notifyCountChange();
    }

    public static void add(String key, ActionObject cacheObject) {
        if (mCache.containsKey(key))
            mCache.remove(key);
        mCache.put(key, cacheObject);
        notifyCountChange();
    }

    public static int getCount() {
        return mCache.size();
    }

    public static void addListener(OnGlobalCacheChangeListener listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    public static void removeListener(OnGlobalCacheChangeListener listener) {
        mListeners.remove(listener);
    }

    public static void addEventListener(OnGlobalCacheEventListener listener) {
        if (!mEventsListener.contains(listener)) {
            mEventsListener.add(listener);
        }
    }

    public static void removeEventListener(OnGlobalCacheEventListener listener) {
        mEventsListener.remove(listener);
    }

    public static void addStatusChagneListener(OnGlobalCacheActionStatusChangeListener listener) {
        if (mStatusListeners != null) {
            mStatusListeners.add(listener);
        }
    }

    public static void removeStatusChangeListener(OnGlobalCacheActionStatusChangeListener listener) {
        mStatusListeners.remove(listener);
    }

    private static void notifyCountChange() {
        for (OnGlobalCacheChangeListener listener : mListeners) {
            listener.onGlobalCacheCountChange(mCache.size());
        }
    }

    public static void notify(ActionObject object) {
        for (OnGlobalCacheEventListener listener : mEventsListener) {
            listener.onGlobalCacheEvent(object);
        }
    }

    public static void notifyStatusChange(ActionObject actionObject) {
        for (OnGlobalCacheActionStatusChangeListener listener : mStatusListeners) {
            listener.onGlobalCacheActionStatusChange(actionObject);
        }
    }

    public static MArchiveFile getArchive(String path) {
        MArchiveFile archiveFile = mArchiveCache.get(path);
        if (archiveFile != null) {
            File file = new File(path);
            if (file.lastModified() != archiveFile.getDate()) {
                archiveFile = null;
                mArchiveCache.remove(path);
            }
        }
        return archiveFile;
    }

    public static void addArchive(String path, MArchiveFile file) {
        mArchiveCache.remove(path);
        mArchiveCache.put(path, file);
    }

    public static void removeArchive(String path) {
        mArchiveCache.remove(path);
    }


    public enum OBJECT_ACTION {
        COPY, CUT, ZIP, UN_ZIP, DOWNLOAD, UPLOAD, DELETE
    }

    public enum ACTION_RESULT {
        OK, ERROR, ALREADY_PROCESS
    }

    public enum ACTION_STATUS {
        IDLE, PROCESS, FINISH
    }

    public interface OnGlobalCacheEventListener {

        void onGlobalCacheEvent(ActionObject object);

    }

    public interface OnGlobalCacheChangeListener {

        void onGlobalCacheCountChange(int count);

    }

    public interface OnGlobalCacheActionStatusChangeListener {
        void onGlobalCacheActionStatusChange(ActionObject object);
    }

    public static final class ActionObject {

        private static final Handler mHandler = new Handler();

        static {
            mHandler.getLooper();
        }

        public String Key;
        public Object Obj;
        public OBJECT_ACTION Action;
        public OBJECT_ACTION MaskAction;
        public ACTION_STATUS ActionStatus = ACTION_STATUS.IDLE;
        private int Progress;

        private final Handler uiHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                GlobalCache.notifyStatusChange((ActionObject) msg.obj);
            }
        };

        public ActionObject() {

        }

        public ActionObject setKey(String key) {
            Key = key;
            return this;
        }

        public ActionObject setObj(Object obj) {
            Obj = obj;
            return this;
        }

        public ActionObject setAction(OBJECT_ACTION action) {
            Action = action;
            return this;
        }

        public ActionObject setMaskAction(OBJECT_ACTION maskAction) {
            MaskAction = maskAction;
            return this;
        }

        public int getStateIcon() {
            return Action == OBJECT_ACTION.COPY ? R.raw.copy : R.raw.cut;
        }

        public boolean isInProcess() {
            return ActionStatus != ACTION_STATUS.IDLE;
        }

        public ACTION_STATUS getStatus() {
            return ActionStatus;
        }

        public int getProgress() {
            return Progress;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof ActionObject) {
                ActionObject cacheObject = (ActionObject) o;
                return cacheObject.equals(cacheObject.Obj);
            }
            return Obj.equals(o);
        }

        private final void setStatus(ACTION_STATUS status) {
            ActionStatus = status;
            uiHandler.obtainMessage(0, this).sendToTarget();
        }

        public void doAction(final String destPath, final Object parentObject) {
            if (isInProcess()) {
                return;
            }
            setStatus(ACTION_STATUS.PROCESS);

            new Thread() {
                @Override
                public void run() {
                    super.run();
                    if (Obj instanceof ArrayList) {
                        for (Object object : (ArrayList) Obj) {
                            processActionWithObject(object, destPath, parentObject);
                        }
                    } else if (Obj instanceof Object[]) {
                        for (Object object : (Object[]) Obj) {
                            processActionWithObject(object, destPath, parentObject);
                        }
                    } else {
                        processActionWithObject(Obj, destPath, parentObject);
                    }
                    setStatus(ACTION_STATUS.FINISH);
                }
            }.start();
        }

        private void processActionWithObject(final Object pObject, String destPath, Object parentObject) {
            final MFile mFile = pObject instanceof MFile ? (MFile) pObject : null;
            if (mFile != null) {
                if (parentObject instanceof BaseDriveManager) {
                    DriveDUObject duObject = new DriveDUObject(DriveDUObject.TYPE.UPLOAD, (BaseDriveManager) parentObject,
                            pObject, destPath, Utils.getKey(pObject));
                    duObject.addListener(new DriveDUObject.DriveDUListenerAdapter() {

                        @Override
                        public void onDUProgress(DriveDUObject.TYPE type, int progress) {
                            Progress = progress;
                        }

                        @Override
                        public void onDUFinish(DriveDUObject.TYPE type, Object error) {
                            if (error == null) {
                                if (Action == OBJECT_ACTION.CUT) {
                                    BaseDriveManager.get(pObject).deleteFile(pObject);
                                }
                                GlobalCache.remove(Key);
                            }
                        }
                    });
                    DriveDUManager.getInstance().add(duObject);
                    duObject.upload();
                } else {
                    ACTION_RESULT mResult = ACTION_RESULT.ERROR;
                    switch (Action) {
                        case COPY:
                            mResult = mFile.copy(destPath);
                            break;
                        case CUT:
                            mResult = mFile.cut(destPath);
                            break;
                        case UN_ZIP:
                            if (mFile instanceof MArchiveFile) {
                                mResult = ((MArchiveFile) mFile).unPack(destPath);
                            } else if (mFile instanceof MArchiveEntryFile) {
                                mResult = ((MArchiveEntryFile) mFile).unPack(destPath);
                            }
                            break;
                        case ZIP:
                            if (mFile instanceof MJavaFile) {
                                mResult = ((MJavaFile) mFile).zip(destPath);
                            }
                            break;
                    }
                    if (mResult == ACTION_RESULT.OK)
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                GlobalCache.remove(Key);

                            }
                        });
                }
            } else if (Utils.isCloudObject(pObject)) {
                if (parentObject instanceof BaseDriveManager) {
                    File javaFile = Utils.getDirectoryFile(pObject);
                    javaFile.mkdir();
                    DriveDUObject downloadObject = new DriveDUObject(DriveDUObject.TYPE.DOWNLOAD, BaseDriveManager.get(pObject), pObject, javaFile.getPath(), Utils.getKey(pObject));
                    downloadObject.addListener(new DriveDUObject.DriveDUListenerAdapter() {
                        @Override
                        public void onDUProgress(DriveDUObject.TYPE type, int progress) {
                            Progress = progress / 50;
                        }

                    });
                    DriveDUManager.getInstance().add(downloadObject);
                    downloadObject.download();
                    File file = new File(javaFile.getPath(), Utils.getName(pObject));
                    if (file.exists()) {
                        DriveDUObject uploadObject = new DriveDUObject(DriveDUObject.TYPE.UPLOAD, (BaseDriveManager) parentObject, new MJavaFile(file), destPath, file.getPath());
                        uploadObject.upload();
                    }
                } else {
                    DriveDUObject duObject = new DriveDUObject(DriveDUObject.TYPE.DOWNLOAD, BaseDriveManager.get(pObject),
                            pObject, destPath, Utils.getKey(pObject));
                    duObject.addListener(new DriveDUObject.DriveDUListenerAdapter() {

                        @Override
                        public void onDUProgress(DriveDUObject.TYPE type, int progress) {
                            Progress = progress;
                        }

                        @Override
                        public void onDUFinish(DriveDUObject.TYPE type, Object error) {
                            if (error == null) {
                                GlobalCache.remove(Key);
                            }
                        }
                    });
                    DriveDUManager.getInstance().add(duObject);
                    duObject.download();
                }
            }
        }
    }

}
