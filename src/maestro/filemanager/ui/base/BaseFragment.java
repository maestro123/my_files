package maestro.filemanager.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import android.view.WindowManager;
import com.dream.android.mim.MIMUtils;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.Main;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.utils.MBActivity;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by artyom on 9/11/14.
 */
public abstract class BaseFragment extends DialogFragment implements MBActivity.OnBackPressListener,
        Settings.OnSettingChangeListener {

    public Main main;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (enterAnimationEnable())
            dialog.getWindow().setWindowAnimations(R.style.FragmentAnimation);
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        main = (Main) activity;
        if (main != null) {
            main.attachOnBackPressListener(this);
        }
        Settings.attachListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            if (Build.VERSION.SDK_INT > 20) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(MIMUtils.blendColors(getStatusBarColor(), Color.parseColor("#000000"), 0.75f));
            }
    }

    @Override
    public void onDetach() {
        Settings.detachListener(this);
        if (main != null) {
            main.detachOnBackPressListener(this);
            main = null;
        }
        super.onDetach();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public SVGHelper.SVGHolder getSvgHolder() {
        return ((FileManagerApplication) getActivity().getApplication()).getSvgHolder();
    }

    public boolean enterAnimationEnable() {
        return false;
    }

    @Override
    public void onSettingChange(String pref) {

    }

    public int getStatusBarColor() {
        return ThemeHolder.getPrimaryColor();
    }
}
