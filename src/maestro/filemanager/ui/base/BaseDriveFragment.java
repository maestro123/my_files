package maestro.filemanager.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.ui.FileNameInputDialog;
import maestro.filemanager.ui.UniqAdapter;
import maestro.filemanager.ui.media.ImageViewerFragment;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.ToastHelper;
import maestro.filemanager.utils.Utils;
import maestro.support.v1.menu.MDropDownMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom on 3/18/2015.
 */
public abstract class BaseDriveFragment<T> extends BaseFileListFragment<T> implements BaseDriveManager.OnDriveManagerEventListener {

    public static final String TAG = BaseDriveFragment.class.getSimpleName();

    public static final String PARAM_NAME = "name";
    public static final String PARAM_PARENT = "parent";
    public static final String PARAM_OPERATION = "operation";

    private static final int KEY_UPLOAD_FILE = 0x02;

    private UniqAdapter mAdapter;

    private boolean isFirstStart = true;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getDriveManager().attachListener(getKey(), this);
        if (!getDriveManager().isConnected()) {
            getDriveManager().connect(this);
        }
        if (getDriveManager().isConnected() && main != null) {
            main.setStorageInfoGetter(getDriveManager());
        }
    }

    @Override
    public void onDetach() {
        getDriveManager().detachListener(getKey());
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        getDriveManager().onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFirstStart) {
            isFirstStart = false;
        } else {
            getDriveManager().onResume();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addMMenuItem(new FMMenuItem(KEY_UPLOAD_FILE, R.string.upload_file, R.raw.ic_cloud_upload));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getDriveManager().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        if (i == FOLDER_OPERATION_LOADER) {
            Loaders.DriveFolderOperationLoader.FolderOperation operation = (Loaders.DriveFolderOperationLoader.FolderOperation) bundle.getSerializable(PARAM_OPERATION);
            showWait();
            return new Loaders.DriveFolderOperationLoader(getActivity(), getDriveManager(), operation,
                    bundle.getString(PARAM_NAME), bundle.getString(PARAM_PARENT));
        }
        return super.onCreateLoader(i, bundle);
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        animateLoadStart();
        prepareNavigation();
        return null;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<T>> loader, ArrayList<T> ts) {
        if (loader.getId() == FOLDER_OPERATION_LOADER) {
            dismissWait();
            Object result = ((Loaders.DriveFolderOperationLoader) loader).getResult();
            if (result instanceof Boolean && (Boolean) result) {
                loadSilent();
                ToastHelper.show(R.string.folder_created, false);
            } else {
                ToastHelper.show(R.string.cant_create_folder, true);
            }
        } else
            super.onLoadFinished(loader, ts);
    }

    @Override
    public void onDataLoadFinished(Loader<ArrayList<T>> loader, ArrayList<T> items) {
        mAdapter.update((ArrayList) items);
        animateLoadFinished(null, mAdapter.haveItems());
    }

    @Override
    public void onRenameConfirm(FileNameInputDialog.TYPE type, String text, Object attachedObject) {
        if (type == FileNameInputDialog.TYPE.CREATE_FOLDER) {
            Bundle args = new Bundle(2);
            args.putSerializable(PARAM_OPERATION, Loaders.DriveFolderOperationLoader.FolderOperation.CREATE);
            args.putString(PARAM_NAME, text);
            args.putString(PARAM_PARENT, getLastHistory().Path);
            getLoaderManager().initLoader(FOLDER_OPERATION_LOADER, args, this);
        }
    }

    @Override
    public BaseUpdateAdapter buildAdapter(boolean search) {
        return mAdapter = new UniqAdapter(this, getSpanCount());
    }

    @Override
    public void onItemClick(T item) {
        if (Utils.isFolder(item)) {
            load(new HistoryWrite(Utils.getName(item), Utils.getPath(item)));
        } else {
            String name = Utils.getName(item);
            if (Utils.match(name, Utils.IMAGE_PATTERN)) {
                List<Object> files = mAdapter.collectFiles(Utils.IMAGE_PATTERN);
                ImageViewerFragment.makeInstance(files.indexOf(item), files).show(getChildFragmentManager(), ImageViewerFragment.TAG);
            } else {
                File file = new File(Settings.getOneDriveDirectory(), name);
                if (file.exists()) {
                    Utils.processClick(getActivity(), new MJavaFile(file));
                } else {
                    //TODO: start download?
                }
            }
        }
    }

    @Override
    public void onItemClick(View v, MDropDownMenu.MDropItem dropItem) {
        if (dropItem.getId() == ID_CREATE_FOLDER) {
            FileNameInputDialog.makeInstance(FileNameInputDialog.TYPE.CREATE_FOLDER, getString(R.string.folder_name), null, null)
                    .show(getChildFragmentManager(), FileNameInputDialog.TAG);
        }
    }

    @Override
    public void onUserInfoTookEvent(Object object) {
    }

    @Override
    public HistoryWrite getRootHistory() {
        return new HistoryWrite(getDriveManager().getName(), getDriveManager().getHomeFolderPath());
    }

    @Override
    public boolean canLoadData() {
        return getDriveManager().isConnected();
    }

    @Override
    public void onConnect() {
        Log.e(TAG, "onConnect");
        load();
        if (main != null) {
            main.setStorageInfoGetter(getDriveManager());
        }
    }

    @Override
    public void onDisconnet() {
        Log.e(TAG, "onDisconnect");
        if (main != null) {
            main.jumpToPreviousType();
        }
    }

    @Override
    public boolean isEventAllow(GlobalCache.ActionObject actionObject) {
        return actionObject.Action == GlobalCache.OBJECT_ACTION.CUT || actionObject.Action == GlobalCache.OBJECT_ACTION.COPY;
    }

    @Override
    public void onGlobalCacheEvent(GlobalCache.ActionObject object) {
        if (isEventAllow(object)) {
            object.doAction(getLastHistory().Path, getDriveManager());
        }
    }

    public abstract BaseDriveManager getDriveManager();

    public abstract String getKey();

}
