package maestro.filemanager.ui.base;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.utils.MSelectionMode;
import maestro.filemanager.utils.Typefacer;
import maestro.support.v1.svg.SVGHelper;

import java.util.ArrayList;

/**
 * Created by artyom on 9/11/14.
 */
public abstract class BaseUpdateAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = BaseUpdateAdapter.class.getSimpleName();

    public static final int TYPE_ITEM = 0;
    public static final int TYPE_HEADER = 1;
    public static final int TYPE_FOOTER = 2;
    public static final int TYPE_DIVIDER = 3;

    private ArrayList<T> mItems = null;
    private ArrayList<T> mSelectedObjects = new ArrayList<>();
    private MSelectionMode mSelectionMode;
    private Context mContext;
    private LayoutInflater mInflater;
    private SVGHelper.SVGHolder mSvgHolder;
    private OnItemClickActionListener mItemClickListener;

    private int mSpanCount;
    private int topPadding;

    private boolean withHeader;
    private boolean withFooter;

    public BaseUpdateAdapter(Context context, int spanCount) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSpanCount = spanCount;
        mSvgHolder = ((FileManagerApplication) context.getApplicationContext()).getSvgHolder();
    }

    public BaseUpdateAdapter(Context context, int spanCount, int topPadding) {
        this(context, spanCount);
        this.topPadding = topPadding;
    }

    public void setOnItemClickAction(OnItemClickActionListener clickAction) {
        mItemClickListener = clickAction;
    }

    public void setWithHeader(boolean value) {
        withHeader = value;
    }

    public void setWithFooter(boolean value) {
        withFooter = value;
    }

    public Resources getResources() {
        return mContext.getResources();
    }

    public String getString(int resId) {
        return mContext.getString(resId);
    }

    public int getSpanCount(int position) {
        return getItemViewType(position) == TYPE_ITEM ? 1 : mSpanCount;
    }

    public int getActualPosition(int position) {
        if (withHeader)
            position--;
        return position;
    }

    @Override
    public int getItemCount() {
        int itemCount = getItemsCount();
        if (withFooter)
            itemCount++;
        if (withHeader)
            itemCount++;
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && withHeader) {
            return TYPE_HEADER;
        } else if (position == getItemCount() - 1 && withFooter) {
            return TYPE_FOOTER;
        } else {
            return getItem(position) instanceof RowDivider ? TYPE_DIVIDER : TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return type == TYPE_HEADER ? createHeader(mInflater) : type == TYPE_FOOTER ? createFooter(mInflater)
                : type == TYPE_DIVIDER ? createDivider(mInflater) : createItemHolder(mInflater);
    }

    public RecyclerView.ViewHolder createHeader(LayoutInflater inflater) {
        return null;
    }

    public RecyclerView.ViewHolder createFooter(LayoutInflater inflater) {
        return null;
    }

    public RecyclerView.ViewHolder createDivider(LayoutInflater inflater) {
        return new SimpleViewHolder(inflater.inflate(R.layout.list_divider_view, null));
    }

    public VH createItemHolder(LayoutInflater inflater) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int type = getItemViewType(i);
        if (type == TYPE_ITEM) {
            T item = getItem(i);
            onBindItemHolder((VH) viewHolder, item, i);
            ensureCheckMask(viewHolder, mSelectionMode != null, mSelectionMode != null ? mSelectedObjects.contains(item) : false);
            viewHolder.itemView.setTag(viewHolder);
            viewHolder.itemView.setOnClickListener(this);
            viewHolder.itemView.setOnLongClickListener(this);
        } else if (type == TYPE_DIVIDER && viewHolder instanceof SimpleViewHolder) {
            SimpleViewHolder mHolder = (SimpleViewHolder) viewHolder;
            int actualPosition = getActualPosition(i);
            mHolder.Title.setText(((RowDivider) mItems.get(actualPosition)).TitleResource);
            if (actualPosition == 0 && mHolder.Divider.getVisibility() == View.VISIBLE) {
                mHolder.Divider.setVisibility(View.GONE);
            } else if (actualPosition > 0 && mHolder.Divider.getVisibility() == View.GONE) {
                mHolder.Divider.setVisibility(View.VISIBLE);
            }
        }
    }

    public abstract void onBindItemHolder(VH holder, T item, int position);

    public abstract void ensureCheckMask(RecyclerView.ViewHolder holder, boolean inSelectionMode, boolean checked);

    public void update(ArrayList<T> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    public ArrayList<T> getItems() {
        return mItems;
    }

    public int getItemsCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public boolean haveItems() {
        return mItems != null && mItems.size() > 0;
    }

    public T getItem(int position) {
        if (withHeader)
            position--;
        return position >= 0 && position < mItems.size() ? mItems.get(position) : null;
    }

    public Context getContext() {
        return mContext;
    }

    public SVGHelper.SVGHolder getSvgHolder() {
        return mSvgHolder;
    }

    public LayoutInflater getInflater() {
        return mInflater;
    }

    @Override
    public void onClick(View v) {
        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) v.getTag();
        if (holder != null) {
            T item = getItem(holder.getAdapterPosition());
            if (item != null) {
                if (mSelectionMode != null) {
                    final boolean checked = mSelectedObjects.contains(item);
                    if (checked) {
                        mSelectedObjects.remove(item);
                    } else {
                        mSelectedObjects.add(item);
                    }
                    ensureCheckMask(holder, true, !checked);
                    mSelectionMode.setCount(mSelectedObjects.size());
                } else if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(item);
                }
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        Log.e(TAG, "onLongClick");
        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) v.getTag();
        if (holder != null) {
            T item = getItem(holder.getAdapterPosition());
            return item != null && mItemClickListener != null && mItemClickListener.onItemLongClick(item);
        }
        return false;
    }

    public boolean contains(Object object) {
        if (haveItems() && object != null) {
            for (T item : getItems()) {
                if (item.equals(object)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void startSelectionMode(ViewGroup group, MSelectionMode selectionMode, T item) {
        mSelectedObjects.clear();
        mSelectionMode = selectionMode;
        mSelectedObjects.add(item);
        mSelectionMode.setCount(mSelectedObjects.size());

        for (int i = 0; i < group.getChildCount(); i++) {
            RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) group.getChildAt(i).getTag();
            if (holder != null) {
                ensureCheckMask(holder, true, mSelectedObjects.contains(getItem(holder.getAdapterPosition())));
            }
        }

    }

    public void finishSelectionMode(ViewGroup group) {
        mSelectionMode = null;
        for (int i = 0; i < group.getChildCount(); i++) {
            RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) group.getChildAt(i).getTag();
            if (holder != null) {
                ensureCheckMask(holder, false, false);
            }
        }
    }

    public interface OnItemClickActionListener<T> {
        public void onItemClick(T item);

        public boolean onItemLongClick(T item);
    }

    public static final class SimpleViewHolder extends RecyclerView.ViewHolder {

        TextView Title;
        View Divider;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            Title = (TextView) itemView.findViewById(R.id.title);
            Divider = itemView.findViewById(R.id.divider);
            if (Title != null) {
                Title.setTypeface(Typefacer.rRegular);
            }
        }
    }

    public static final class RowDivider {

        int TitleResource;

        public RowDivider(int resId) {
            TitleResource = resId;
        }
    }

}
