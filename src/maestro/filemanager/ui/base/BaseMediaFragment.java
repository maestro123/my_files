package maestro.filemanager.ui.base;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.filemanager.R;
import maestro.filemanager.Settings;

/**
 * Created by U1 on 30.06.2015.
 */
public abstract class BaseMediaFragment extends BaseFragment {

    private Toolbar mToolbar;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbar.setNavigationIcon(getSvgHolder().getDrawable(R.raw.ic_menu_24px, Color.WHITE));
            mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        }
    }

    public abstract void load();

    @Override
    public void onSettingChange(String pref) {
        super.onSettingChange(pref);
        if (Settings.isSortPreference(pref)) {
            load();
        }
    }

    public int getToolbarIconColor() {
        return Color.WHITE;
    }

    @Override
    public int getStatusBarColor() {
        return Color.BLACK;
    }

}
