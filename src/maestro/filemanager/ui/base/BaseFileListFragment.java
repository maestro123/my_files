package maestro.filemanager.ui.base;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.*;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.ui.ConfirmDialog;
import maestro.filemanager.ui.FileNameInputDialog;
import maestro.filemanager.ui.WaitDialog;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.MSelectionMode;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.Utils;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.menu.MDropDownMenu;
import maestro.support.v1.svg.SVG;
import maestro.support.v1.svg.SVGHelper;

import java.util.ArrayList;

/**
 * Created by artyom on 9/11/14.
 */
public abstract class BaseFileListFragment<T> extends BaseFragment implements LoaderManager.LoaderCallbacks<ArrayList<T>>,
        View.OnClickListener, BaseUpdateAdapter.OnItemClickActionListener<T>, TextWatcher,
        FileNameInputDialog.OnRenameEventListener, MDropDownMenu.OnItemClickListener,
        ConfirmDialog.OnConfirmDialogEventListener, MSelectionMode.OnSelectionModeEventListener,
        GlobalCache.OnGlobalCacheEventListener {

    public static final String TAG = BaseFileListFragment.class.getSimpleName();

    public static final String PARAM_HISTORY = "param_history";

    public static final int SEARCH_LOADER = "SearchLoader".hashCode();
    public static final int FOLDER_OPERATION_LOADER = "CreateFolderLoader".hashCode();
    public static final int ID_CREATE_FOLDER = 0x00;
    public static final int ID_CREATE_FILE = 0x01;

    private ArrayList<HistoryWrite> mHistory = new ArrayList<HistoryWrite>();

    private ImageView imgEdit;
    private Toolbar mToolbar;
    private TextView mEmptyView;
    private RecyclerView mList;
    private GridLayoutManager mLayoutManager;
    private LinearLayout mNavigationBarParent, mNavigationParent;
    private HorizontalScrollView mNavigationScrollView;
    private View mSearchParent, mShadow;
    private ImageButton mSearchBack;
    private EditText mSearchEditText;
    private RecyclerView mSearchList;
    private BaseUpdateAdapter mAdapter;
    private BaseUpdateAdapter mSearchAdapter;
    private MDropDownMenu mMenuView;
    private SwipeRefreshLayout mSwipeRefresh;
    private MSelectionMode mSelectionMode;

    private STATE mState;

    private boolean isNoClearLoading;
    private boolean isSilentLoading;
    private int mNavigationPadding;
    private int mNavigationLeftPadding;

    public enum STATE {
        NONE, TOP, BOTTOM
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.list_fragment_view, null);
        mNavigationScrollView = (HorizontalScrollView) v.findViewById(R.id.navigation_bar_scroll_view);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {

            private int mHideTrashHolder = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(getActivity()));
            private int minScrollHideSize = getResources().getDimensionPixelOffset(R.dimen.abc_action_bar_default_height_material) * 2;
            private int totalScrolled = 0;
            private int scrolledDistance = 0;
            private boolean controlsVisible = true;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalScrolled += dy;

                if (mSelectionMode != null && mSelectionMode.isShowing())
                    return;

                if (scrolledDistance > mHideTrashHolder && totalScrolled > minScrollHideSize && controlsVisible) {
                    changeNavigationAlpha(false);
                    mNavigationParent.animate().translationY(-mNavigationParent.getHeight()).start();
                    mShadow.animate().translationY(-mNavigationParent.getHeight()).start();
                    imgEdit.animate().translationY(imgEdit.getHeight()).start();

                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -mHideTrashHolder && !controlsVisible) {
                    changeNavigationAlpha(true);
                    mNavigationParent.animate().translationY(0).start();
                    mShadow.animate().translationY(0).start();
                    imgEdit.animate().translationY(0).start();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }

                if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                    scrolledDistance += dy;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mState = STATE.NONE;
                } else {
                    mMenuView.animate(false);
                }
                BaseFileListFragment.this.onScrollStateChange(recyclerView, newState);
            }

            private void changeNavigationAlpha(boolean visible) {
                mNavigationScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        mNavigationScrollView.fullScroll(View.FOCUS_RIGHT);
                    }
                });
                int childCount = mNavigationBarParent.getChildCount();
                for (int i = 0; i < childCount - 1; i++) {
                    mNavigationBarParent.getChildAt(i).animate().alpha(visible ? .5f : 0).start();
                }
            }

        });
        mList.setLayoutManager(mLayoutManager = new GridLayoutManager(getActivity(), getSpanCount()));
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                return mAdapter.getSpanCount(i);
            }
        });
        mNavigationBarParent = (LinearLayout) v.findViewById(R.id.navigation_bar_items_parent);
        mNavigationParent = (LinearLayout) v.findViewById(R.id.navigation_bar_layout);
        imgEdit = (ImageView) v.findViewById(R.id.edit_button);
        imgEdit.setOnClickListener(this);
        mMenuView = (MDropDownMenu) v.findViewById(R.id.drop_menu_view);
        mMenuView.setAlign(MDropDownMenu.ALIGN.RIGHT);
        mEmptyView = (TextView) v.findViewById(R.id.empty_view);
        mShadow = v.findViewById(R.id.shadow);
        mSwipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh);
        mSwipeRefresh.setProgressViewOffset(false, 0, getResources().getDimensionPixelOffset(R.dimen.abc_action_bar_default_height_material) * 2
                + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()));
        mSwipeRefresh.setColorSchemeColors(getResources().getColor(R.color.actionbar_color));
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNoClear();
            }
        });

        mEmptyView.setTypeface(Typefacer.rMediumItalic);

        Drawable drawable = getSvgHolder().getDrawable(R.raw.el_primary_action_button, getResources().getColor(R.color.actionbar_color));
        imgEdit.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imgEdit.setBackgroundDrawable(drawable);
        imgEdit.setImageDrawable(null);

        mMenuView.setAnchorView(imgEdit);
        mMenuView.setOnItemClickListener(this);
        mMenuView.setOnToggleStartListener(new MDropDownMenu.OnToggleStartListener() {
            @Override
            public void onToggleStart(boolean visible) {
                Drawable drawable = imgEdit.getDrawable();
                if (drawable instanceof SVG) {
                    ObjectAnimator animator = ObjectAnimator.ofInt(drawable, SVG.ROTATION, visible ? 0 : 135, visible ? 135 : 0);
                    animator.setInterpolator(new OvershootInterpolator());
                    animator.setDuration(300);
                    animator.start();
                }
            }
        });

        getSvgHolder().applySVG(imgEdit, R.raw.add, Color.WHITE);

        mSearchParent = getActivity().findViewById(R.id.search_parent);
        mSearchBack = (ImageButton) mSearchParent.findViewById(R.id.search_btn_back);
        mSearchEditText = (EditText) mSearchParent.findViewById(R.id.search_edit_text);
        mSearchList = (RecyclerView) mSearchParent.findViewById(R.id.search_list);
        mSearchList.setLayoutManager(new GridLayoutManager(getActivity(), getSpanCount()));

        mSearchParent.setVisibility(View.GONE);

        mSearchBack.setOnClickListener(this);
        mSearchBack.setBackgroundDrawable(null);
        getSvgHolder().applySVG(mSearchBack, R.raw.ic_back, Color.WHITE);

        mToolbar = (Toolbar) v.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(getSvgHolder().getDrawable(R.raw.ic_menu_24px, ThemeHolder.getActionBarIconColor()));
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        v.findViewById(R.id.toolbar_background_view).setBackgroundColor(getToolbarColor());
//        v.findViewById(R.id.toolbar_left_mask).setBackgroundColor(getToolbarColor());
//        v.findViewById(R.id.toolbar_right_mask).setBackgroundColor(getToolbarColor());
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = buildAdapter(false));
        if (mAdapter != null)
            mAdapter.setOnItemClickAction(this);
        if (savedInstanceState == null) {
            mHistory.add(getRootHistory());
        } else {
            mHistory = savedInstanceState.getParcelableArrayList(PARAM_HISTORY);
        }

        mMenuView.addItem(new FMMenuItem(ID_CREATE_FILE, R.string.new_file, R.raw.ic_file_24px));
        mMenuView.addItem(new FMMenuItem(ID_CREATE_FOLDER, R.string.new_folder, R.raw.ic_folder_24px));

        load();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        GlobalCache.addEventListener(this);
    }

    @Override
    public void onDetach() {
        GlobalCache.removeEventListener(this);
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PARAM_HISTORY, mHistory);
    }

    @Override
    public Loader<ArrayList<T>> onCreateLoader(int i, Bundle bundle) {
        if (i == SEARCH_LOADER) {
            return getSearchLoader(mSearchEditText.getText().toString());
        } else {
            mState = STATE.TOP;
            applyState();
            return getBaseLoader(i, bundle);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<T>> loader, ArrayList<T> ts) {
        if (loader.getId() == SEARCH_LOADER) {
            mSearchAdapter.update(ts);
        } else {
            onDataLoadFinished(loader, ts);
            isSilentLoading = false;
            isNoClearLoading = false;
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<T>> loader) {
    }

    @Override
    public void onConfirmEvent(ConfirmDialog.TYPE type, Object object) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search) {
            showSearchView();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onBackPressed() {
        if (hideSearchView()) {
            return true;
        } else if (mSelectionMode != null && mSelectionMode.isShowing()) {
            mSelectionMode.finish();
            return true;
        } else if (mMenuView.isVisible()) {
            mMenuView.toggle();
            return true;
        } else if (mHistory.size() > 1) {
            mHistory.remove(mHistory.size() - 1);
            load();
            return true;
        }
        return super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_button:
                mMenuView.toggle();
                break;
            case R.id.search_btn_back:
                hideSearchView();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(T item) {
        if (mSelectionMode == null) {
            mSelectionMode = new MSelectionMode((AppCompatActivity) getActivity(), mList, mAdapter);
            mSelectionMode.setOnSelectionModeEventListener(this);
        }
        if (!mSelectionMode.isShowing()) {
            mState = STATE.TOP;
            applyState();
            mSelectionMode.start(item);
        }
        return true;
    }

    private void applyState() {
        if (!isSilentLoading || (mSelectionMode != null && mSelectionMode.isShowing())) {
            if (mState == STATE.BOTTOM) {
                mNavigationParent.animate().translationY(-getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)).start();
                mShadow.animate().translationY(-getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)).start();
                imgEdit.animate().translationY(imgEdit.getHeight()).alpha(0f).start();
            } else if (mState == STATE.TOP) {
                mNavigationParent.animate().translationY(0).start();
                mShadow.animate().translationY(0).start();
                imgEdit.animate().translationY(0).alpha(1f).start();
            }
        }
    }

    public final int getSpanCount() {
        final boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        return getResources().getBoolean(R.bool.is7inch) ? isLandscape ? 2 : 1 : getResources().getBoolean(R.bool.is10inch) ? isLandscape ? 3 : 2 : 1;
    }

    public void ensureItemPosition() {
        HistoryWrite write = mHistory.get(mHistory.size() - 1);
        Log.e(TAG, "ensureItemPosition = " + write.ItemPosition);
        mList.scrollBy(0, 0);//write.ItemPosition);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (getLoaderManager().getLoader(SEARCH_LOADER) == null) {
            getLoaderManager().initLoader(SEARCH_LOADER, null, BaseFileListFragment.this);
        } else {
            getLoaderManager().restartLoader(SEARCH_LOADER, null, BaseFileListFragment.this);
        }
    }


    public void addMMenuItem(FMMenuItem item) {
        mMenuView.addItem(item);
    }

    public void onScrollStateChange(RecyclerView recyclerView, int scrollState) {

    }

    public ArrayList<HistoryWrite> getHistory() {
        return mHistory;
    }

    public HistoryWrite getLastHistory() {
        return mHistory.get(mHistory.size() - 1);
    }

    public void load() {
        if (canLoadData() && isAdded()) {
            if (getLoaderManager().getLoader(getLoaderId()) != null) {
                getLoaderManager().restartLoader(getLoaderId(), null, this);
            } else {
                getLoaderManager().initLoader(getLoaderId(), null, this);
            }
        }
    }

    public void loadNoClear() {
        isNoClearLoading = true;
        load();
    }

    public void loadSilent() {
        isSilentLoading = true;
        loadNoClear();
    }

    public void load(HistoryWrite write) {
        if (canLoadData()) {
            mHistory.add(write);
            if (getLoaderManager().getLoader(getLoaderId()) != null) {
                getLoaderManager().restartLoader(getLoaderId(), null, this);
            } else {
                getLoaderManager().initLoader(getLoaderId(), null, this);
            }
        }
    }

    public boolean canLoadData() {
        return true;
    }

    public boolean isSilentLoading() {
        return isSilentLoading;
    }

    public boolean isNoClearLoading() {
        return isNoClearLoading;
    }

    public void showSearchView() {
        mSearchEditText.setText(null);
        mSearchEditText.addTextChangedListener(this);
        mSearchList.setAdapter(mSearchAdapter = buildAdapter(true));
        mSearchAdapter.setOnItemClickAction(this);
        mSearchAdapter.setWithHeader(false);
        mSearchParent.animate().alpha(1f)
                .setInterpolator(AnimationUtils.loadInterpolator(getActivity(), R.anim.decelerate_quart))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        mSearchParent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        Utils.showInput(getActivity(), mSearchEditText);
                    }
                }).start();
    }

    public boolean hideSearchView() {
        if (mSearchParent.getVisibility() == View.VISIBLE) {
            mSearchEditText.removeTextChangedListener(this);
            mSearchParent.animate().alpha(0f)
                    .setInterpolator(AnimationUtils.loadInterpolator(getActivity(), R.anim.decelerate_quart))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mSearchParent.setVisibility(View.GONE);
                            Utils.hideInput(getActivity(), mSearchEditText);
                        }
                    }).start();
            return true;
        }
        return false;
    }

    public void animateListIn(Animator.AnimatorListener listener) {
        if (!isNoClearLoading)
            mList.animate().translationX(0).alpha(1f).setDuration(125).setListener(listener).start();
    }

    public void animateListOut(Animator.AnimatorListener listener) {
        if (!isNoClearLoading)
            mList.animate().translationX(-mList.getWidth() * 0.3f).alpha(0f).setDuration(125).setListener(listener).start();
    }

    public void animateEmptyIn() {
        if (mEmptyView.getTranslationX() == 0) {
            mEmptyView.setTranslationX(mList.getWidth() * 0.3f);
            mEmptyView.setAlpha(0f);
        }
        mEmptyView.animate().translationX(0).alpha(1f).setDuration(125).start();
    }

    public void animateEmptyOut() {
        mEmptyView.animate().translationX(mList.getWidth() * 0.3f).alpha(0).setDuration(125).start();
    }

    public void animateProgressIn() {
        if (!isSilentLoading) {
            mSwipeRefresh.setRefreshing(true);
            setActionButtonVisibility(false);
        }
    }

    public void animateProgressOut() {
        if (!isSilentLoading) {
            mSwipeRefresh.setRefreshing(false);
            setActionButtonVisibility(true);
        }
    }

    public void animateLoadStart() {
        animateEmptyOut();
        animateListOut(null);
        animateProgressIn();
    }

    public void animateLoadFinished(Animator.AnimatorListener listener, boolean haveData) {
        animateProgressOut();
        if (!haveData) {
            animateEmptyIn();
        } else {
            animateListIn(listener);
        }
    }

    public void setActionButtonVisibility(boolean visible) {
        if (visible) {
            imgEdit.animate().scaleY(1f).scaleX(1f).alpha(1f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            imgEdit.setVisibility(View.VISIBLE);
//                            Drawable drawable = imgEdit.getDrawable();
//                            if (drawable instanceof SVG) {
//                                ObjectAnimator animator = ObjectAnimator.ofInt(drawable, SVG.ROTATION, 0, -90);
//                                animator.setInterpolator(new OvershootInterpolator());
//                                animator.setDuration(300);
//                                animator.start();
//                            }
                        }
                    }).start();
        } else if (!visible) {
            imgEdit.animate().scaleY(0.3f).scaleX(0.3f).alpha(0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
//                            Drawable drawable = imgEdit.getDrawable();
//                            if (drawable instanceof SVG) {
//                                ObjectAnimator animator = ObjectAnimator.ofInt(drawable, SVG.ROTATION, -90, 0);
//                                animator.setInterpolator(new OvershootInterpolator());
//                                animator.setDuration(300);
//                                animator.start();
//                            }
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            imgEdit.setVisibility(View.GONE);
                        }
                    }).start();
        }
    }

    @Override
    public void onRenameCancel(FileNameInputDialog.TYPE type) {

    }

    @Override
    public void onSettingChange(String pref) {
        if (Settings.isSortPreference(pref)) {
            load();
        }
    }

    @Override
    public void onSelectionModeEvent(MSelectionMode.EVENT event) {
        if (event == MSelectionMode.EVENT.START) {
            setActionButtonVisibility(false);
        } else {
            setActionButtonVisibility(true);
        }
    }

    public void showWait() {
        new WaitDialog().show(getChildFragmentManager(), WaitDialog.TAG);
    }

    public void showWait(String msg) {
        WaitDialog.makeInstance(msg).show(getChildFragmentManager(), WaitDialog.TAG);
    }

    public void dismissWait() {
        DialogFragment waitFragment = (DialogFragment) getChildFragmentManager().findFragmentByTag(WaitDialog.TAG);
        if (waitFragment != null) {
            waitFragment.dismiss();
        }
    }

    public int getToolbarColor() {
        return ThemeHolder.PrimaryColor;
    }

    public int getToolbarIconColor() {
        return ThemeHolder.getActionBarIconColor();
    }

    public void prepareNavigation() {
        mNavigationPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        mNavigationLeftPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 72, getResources().getDisplayMetrics());
        final int size = mHistory.size();

        while (mNavigationBarParent.getChildCount() > size - 1) {
            mNavigationBarParent.removeViewAt(0);
        }

        for (int i = 0; i < size; i++) {
            View child = mNavigationBarParent.getChildAt(i);
            if (child == null) {
                mNavigationBarParent.addView(child = makeHistoryView(null, mHistory.get(i), i == size - 1, i == 0));
            } else {
                makeHistoryView(child, mHistory.get(i), i == size, i == 0);
            }
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HistoryHolder holder = (HistoryHolder) v.getTag();
                    if (holder != null) {
                        final int index = mHistory.indexOf(holder.Write);
                        if (index != -1 && index != mHistory.size() - 1) {
                            do {
                                mHistory.remove(mHistory.size() - 1);
                            } while (index != mHistory.size() - 1);
                            load();
                        }
                    }
                }
            });
        }
        mNavigationScrollView.post(new Runnable() {
            @Override
            public void run() {
                mNavigationScrollView.fullScroll(View.FOCUS_RIGHT);
            }
        });
    }

    private final View makeHistoryView(View view, HistoryWrite write, boolean isCurrent, boolean isFirst) {
        LinearLayout linearLayout;
        HistoryHolder mHolder;
        if (view == null) {
            linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.history_navigation_view, null);
            view = linearLayout;
            mHolder = new HistoryHolder(view);
        } else {
            mHolder = (HistoryHolder) view.getTag();
        }
        mHolder.Write = write;
        mHolder.Title.setText(write.Title);
        mHolder.Title.setTypeface(isCurrent ? Typefacer.rMedium : Typefacer.rRegular);
        view.setPadding(isFirst ? mNavigationLeftPadding : mNavigationPadding / 2, mNavigationPadding, 0, mNavigationPadding);
        if (!isCurrent) {
            mHolder.Icon.setVisibility(View.VISIBLE);
            view.setAlpha(.5f);
            view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)));
        } else {
            mHolder.Icon.setVisibility(View.GONE);
            view.setAlpha(1f);
            view.setLayoutParams(new LinearLayout.LayoutParams(mToolbar.getWidth() - mNavigationLeftPadding, getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)));
        }
        return view;
    }

    public abstract int getLoaderId();

    public abstract Loader getSearchLoader(String query);

    public abstract Loader getBaseLoader(int id, Bundle args);

    public abstract void onDataLoadFinished(Loader<ArrayList<T>> loader, ArrayList<T> ts);

    public abstract BaseUpdateAdapter buildAdapter(boolean search);

    public abstract HistoryWrite getRootHistory();

    public abstract boolean isEventAllow(GlobalCache.ActionObject actionObject);

    private class HistoryHolder {

        private TextView Title;
        private ImageView Icon;
        private HistoryWrite Write;

        public HistoryHolder(View v) {
            Title = (TextView) v.findViewById(R.id.title);
            Icon = (ImageView) v.findViewById(R.id.icon);
            getSvgHolder().applySVG(Icon, R.raw.ic_arrow_right, Color.WHITE);
            v.setTag(this);
        }

    }

    public static final class HistoryWrite implements Parcelable {

        public static final int NOT_SET = 0x0;
        public static final Creator<HistoryWrite> CREATOR = new Creator<HistoryWrite>() {
            @Override
            public HistoryWrite createFromParcel(Parcel parcel) {
                return new HistoryWrite(parcel);
            }

            @Override
            public HistoryWrite[] newArray(int i) {
                return new HistoryWrite[i];
            }
        };
        public String Title;
        public String Path;
        public int ItemPosition = NOT_SET;
        public int Icon = NOT_SET;
        public int Color = NOT_SET;

        public HistoryWrite(String title, String path) {
            this.Title = title;
            this.Path = path;
        }

        public HistoryWrite(Parcel source) {
            Title = source.readString();
            Path = source.readString();
            ItemPosition = source.readInt();
            Icon = source.readInt();
            Color = source.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(Title);
            parcel.writeString(Path);
            parcel.writeInt(ItemPosition);
            parcel.writeInt(Icon);
            parcel.writeInt(Color);
        }

    }

    public static final class FMMenuItem extends MDropDownMenu.MDropItem {

        private int IconResource;
        private int TitleResource;

        public FMMenuItem(int id, int title, int iconResource) {
            super(id);
            TitleResource = title;
            IconResource = iconResource;
        }

        @Override
        public View makeView(Context context, LayoutInflater inflater) {
            final View v = inflater.inflate(R.layout.fm_menu_item_view, null);
            SVGHelper.SVGHolder holder = ((FileManagerApplication) context.getApplicationContext()).getSvgHolder();
            TextView title = ((TextView) v.findViewById(R.id.title));
            title.setTypeface(Typefacer.rRegular);
            title.setText(TitleResource);
            ImageView icon = (ImageView) v.findViewById(R.id.icon);

            Drawable drawable = holder.getDrawable(R.raw.el_primary_action_button, ThemeHolder.getPrimaryColor(), 68f / 84 * SVGHelper.DPI);
            icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            icon.setBackgroundDrawable(drawable);
            icon.setImageDrawable(null);
            SVGHelper.applySVG(icon, IconResource, Color.WHITE);

            return v;
        }

    }

}
