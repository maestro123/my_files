package maestro.filemanager.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.utils.Typefacer;

/**
 * Created by Artyom on 5/4/2015.
 */
public class WaitDialog extends DialogFragment {

    public static final String TAG = WaitDialog.class.getSimpleName();

    private static final String PARAM_MSG = "msg";

    public static WaitDialog makeInstance(String msg) {
        WaitDialog dialog = new WaitDialog();
        Bundle args = new Bundle(1);
        args.putString(PARAM_MSG, msg);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.wait_dialog_view, null);
        final TextView mTitle = (TextView) v.findViewById(R.id.title);
        mTitle.setTypeface(Typefacer.rRegular);
        mTitle.setText(getArguments() != null && getArguments().containsKey(PARAM_MSG)
                ? getArguments().getString(PARAM_MSG) : getString(R.string.wait));
        return v;
    }
}
