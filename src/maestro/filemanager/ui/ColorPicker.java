package maestro.filemanager.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.SeekBar;
import maestro.filemanager.R;

/**
 * Created by artyom on 12/25/14.
 */
public class ColorPicker extends DialogFragment implements SeekBar.OnSeekBarChangeListener {

    public static final String TAG = ColorPicker.class.getSimpleName();

    private static final String PARAM_INITIAL_COLOR = "initial_color";

    private EditText edtR, edtG, edtB;
    private SeekBar seekR, seekG, seekB;
    private View mColorPreview;
    private ColorDrawable mColorDrawable = new ColorDrawable();

    private int mInitialColor = 0x00;

    public static final ColorPicker makeInstance(int color) {
        ColorPicker fragment = new ColorPicker();
        Bundle args = new Bundle(1);
        args.putInt(PARAM_INITIAL_COLOR, color);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mInitialColor = getArguments().getInt(PARAM_INITIAL_COLOR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.color_picker_view, null);
        mColorPreview = v.findViewById(R.id.color_preview);

        edtR = (EditText) v.findViewById(R.id.edit_text_R);
        edtG = (EditText) v.findViewById(R.id.edit_text_G);
        edtB = (EditText) v.findViewById(R.id.edit_text_B);

        seekR = (SeekBar) v.findViewById(R.id.color_preview_seek_red);
        seekG = (SeekBar) v.findViewById(R.id.color_preview_seek_green);
        seekB = (SeekBar) v.findViewById(R.id.color_preview_seek_blue);

        seekR.setOnSeekBarChangeListener(this);
        seekG.setOnSeekBarChangeListener(this);
        seekB.setOnSeekBarChangeListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mColorPreview.setBackgroundDrawable(mColorDrawable);
        final int r = Color.red(mInitialColor);
        final int g = Color.green(mInitialColor);
        final int b = Color.blue(mInitialColor);
        applyColor(r, g, b);
        seekR.setProgress(r);
        seekG.setProgress(g);
        seekB.setProgress(b);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser)
            applyColor(seekR.getProgress(), seekG.getProgress(), seekB.getProgress());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void applyColor(int r, int g, int b) {
        mColorDrawable.setColor(Color.rgb(r, g, b));
        edtR.setText(String.valueOf(r));
        edtG.setText(String.valueOf(g));
        edtB.setText(String.valueOf(b));
    }

}
