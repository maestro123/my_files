package maestro.filemanager.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.Loader;
import android.view.View;
import com.dropbox.client2.DropboxAPI;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.DropBoxManager;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.Loaders;
import maestro.support.v1.menu.MDropDownMenu;

import java.io.File;
import java.util.List;

/**
 * Created by Artyom on 3/18/2015.
 */
public class DropBoxDriveFragment extends BaseDriveFragment<DropboxAPI.Entry> {

    public static String TAG = DropBoxDriveFragment.class.getSimpleName();

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public Loader getSearchLoader(String query) {
        return null;
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        super.getBaseLoader(id, args);
        return new Loaders.DropBoxDriveLoader(getActivity(), getLastHistory().Path);
    }

    @Override
    public BaseDriveManager getDriveManager() {
        return DropBoxManager.getInstance();
    }

    @Override
    public String getKey() {
        return TAG;
    }

}
