package maestro.filemanager.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.utils.ToastHelper;
import maestro.filemanager.utils.Utils;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.fview.FilterEditText;

/**
 * Created by artyom on 3/12/15.
 */
public class FileNameInputDialog extends BaseFragment implements View.OnClickListener, TextWatcher {

    public static final String TAG = FileNameInputDialog.class.getSimpleName();

    private static final String PARAM_TITLE = "title";
    private static final String PARAM_START_TEXT = "start_text";
    private static final String PARAM_TYPE = "type";

    private static Object mSharedObject;

    public static final FileNameInputDialog makeInstance(TYPE type, String title, String text, Object object) {
        FileNameInputDialog dialog = new FileNameInputDialog();
        Bundle args = new Bundle(3);
        args.putSerializable(PARAM_TYPE, type);
        args.putString(PARAM_TITLE, title);
        args.putString(PARAM_START_TEXT, text);
        mSharedObject = object;
        dialog.setArguments(args);
        return dialog;
    }

    private FilterEditText mEdit;
    private TextView mTitle;
    private Button btnOk, btnCancel;
    private TYPE mType = TYPE.UNKNOWN;
    private String startText;

    private OnRenameEventListener mListener;

    public enum TYPE {
        UNKNOWN, RENAME, CREATE_FILE, CREATE_FOLDER
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup v = (ViewGroup) inflater.inflate(R.layout.file_rename_dialog_view, container);
        mTitle = (TextView) v.findViewById(R.id.title);
        mEdit = (FilterEditText) v.findViewById(R.id.edit_text);
        btnOk = (Button) v.findViewById(R.id.btn_ok);
        btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        mEdit.setColors(ThemeHolder.getPrimaryColor(), ThemeHolder.getTextSecondaryColor());
        ThemeHolder.applyDialogButtons(v);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mType = getArguments() != null ? (TYPE) getArguments().getSerializable(PARAM_TYPE) : TYPE.UNKNOWN;
        mTitle.setText(getArguments() != null ? getArguments().getString(PARAM_TITLE) : null);
        startText = getArguments() != null ? getArguments().getString(PARAM_START_TEXT) : null;
        mEdit.setText(startText);

        if (startText != null) {
            final int lastDot = startText.lastIndexOf(".");
            if (lastDot > 0) {
                mEdit.setSelection(lastDot);
            }
        }

        Utils.showInput(getActivity(), mEdit);

        if (getParentFragment() != null && getParentFragment() instanceof OnRenameEventListener) {
            mListener = (OnRenameEventListener) getParentFragment();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSharedObject = null;
    }

    public FileNameInputDialog setOnRenameEventListener(OnRenameEventListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ok) {
            processConfirm();
        } else if (v.getId() == R.id.btn_cancel) {
            if (mListener != null) {
                mListener.onRenameCancel(mType);
            }
            dismiss();
        }
    }

    private final void processConfirm() {
        String text = mEdit.getText().toString();
        if (text == null || text.length() == 0 || TextUtils.isEmpty(text)) {
            ToastHelper.show(R.string.empty_error, true);
            mEdit.requestFocus();
        } else if (!TextUtils.isEmpty(startText) && startText.equals(text)) {
            ToastHelper.show(R.string.same_file_name, true);
        } else {
            if (mListener != null) {
                mListener.onRenameConfirm(mType, text, mSharedObject);
            }
            dismiss();
        }
    }

    public interface OnRenameEventListener {

        public void onRenameConfirm(TYPE type, String text, Object attachedObject);

        public void onRenameCancel(TYPE type);

    }

}
