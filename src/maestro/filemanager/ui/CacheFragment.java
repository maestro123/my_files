package maestro.filemanager.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.*;
import android.widget.ImageButton;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.RecyclingImageView;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.GlobalCache.OnGlobalCacheActionStatusChangeListener;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.Utils;
import maestro.filemanager.utils.theme.ThemeHolder;

/**
 * Created by Artyom on 4/30/2015.
 */
public class CacheFragment extends BaseFragment implements GlobalCache.OnGlobalCacheChangeListener, OnGlobalCacheActionStatusChangeListener {

    public static final String TAG = CacheFragment.class.getSimpleName();

    private RecyclerView mList;
    private View mEmpty;
    private CacheAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.cache_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new GridLayoutManager(getActivity(), getSpanCount()));
        mEmpty = v.findViewById(R.id.empty_view);
        ThemeHolder.applyEmptyView(v, getSvgHolder());
        return v;
    }

    private int getSpanCount() {
        return 1;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        GlobalCache.addListener(this);
        GlobalCache.addStatusChagneListener(this);
    }

    @Override
    public void onDetach() {
        GlobalCache.removeListener(this);
        GlobalCache.removeStatusChangeListener(this);
        super.onDetach();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new CacheAdapter(this, getSpanCount()));
        ensureState();
    }

    @Override
    public void onGlobalCacheCountChange(int count) {
        if (mAdapter != null) {
            mList.getRecycledViewPool().clear();
            mAdapter.notifyDataSetChanged();
            ensureState();
        }
    }

    private final void ensureState() {
        if (mAdapter != null) {
            if (mAdapter.getItemCount() == 0) {
                mEmpty.setVisibility(View.VISIBLE);
            } else {
                mEmpty.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onGlobalCacheActionStatusChange(GlobalCache.ActionObject object) {
        final int childCount = mList.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View v = mList.getChildAt(i);
            CacheAdapter.Holder holder = (CacheAdapter.Holder) v.getTag();
            if (holder != null) {
                GlobalCache.ActionObject eObject = mAdapter.getItem(holder.getAdapterPosition());
                if (eObject != null && eObject.equals(object)) {
                    holder.ensureState(object);
                    break;
                }
            }
        }
    }

    public class CacheAdapter extends BaseUpdateAdapter<GlobalCache.ActionObject, CacheAdapter.Holder> {

        private BaseFragment mBaseFragment;
        private int width, height;

        public CacheAdapter(BaseFragment fragment, int spanCount) {
            super(fragment.getActivity(), spanCount);
            mBaseFragment = fragment;
            init();
        }

        public CacheAdapter(BaseFragment fragment, int spanCount, int topPadding) {
            super(fragment.getActivity(), spanCount, topPadding);
            mBaseFragment = fragment;
            init();
        }

        final void init() {
            width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
            height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
        }

        @Override
        public int getItemCount() {
            return GlobalCache.getCount();
        }

        @Override
        public GlobalCache.ActionObject getItem(int position) {
            return GlobalCache.getAt(position);
        }

        @Override
        public Holder createItemHolder(LayoutInflater inflater) {
            final View v = getInflater().inflate(R.layout.file_cache_row_item_view, null);
            final Holder holder = new Holder(v);
            v.setOnTouchListener(new View.OnTouchListener() {

                private float startX;
                private float startY;
                private float downX;
                private float downY;
                private boolean isDragging;
                private boolean isFling;
                private boolean isFlingToLeft;
                private float mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();


                private GestureDetectorCompat mGesture = new GestureDetectorCompat(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        isFling = true;
                        isFlingToLeft = e1.getX() > e2.getX();
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

                @Override
                public boolean onTouch(final View v, MotionEvent event) {

                    final float x = event.getRawX();
                    final float y = event.getRawY();
                    float abs = Math.abs(downX - x);

                    mGesture.onTouchEvent(event);
                    switch (event.getAction() & event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            downX = x;
                            downY = y;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (!isDragging && abs >= mTouchSlop) {
                                isDragging = true;
                                startX = x;
                                startY = y;
                            }
                            float setX = x - startX;
                            if (isDragging && setX < 0) {
                                v.setTranslationX(x - startX);
                            }
                            break;
                        case MotionEvent.ACTION_CANCEL:
                        case MotionEvent.ACTION_UP:

                            boolean remove = (isFling && isFlingToLeft) || abs > v.getMeasuredWidth() / 2;

                            if (remove) {
                                removeItem(v, holder.getAdapterPosition());
                            } else {
                                v.animate().translationX(0);
                            }

                            v.setTranslationX(0);

                            isDragging = false;
                            isFling = false;
                            break;
                    }

                    return isDragging;
                }

            });
            return holder;
        }

        private void removeItem(final View v, final int position) {
            v.animate().translationX(-v.getMeasuredWidth()).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    GlobalCache.ActionObject item = getItem(position);
                    GlobalCache.remove(item.Key);
                }
            });
        }

        @Override
        public void onBindItemHolder(final Holder holder, final GlobalCache.ActionObject item, final int position) {
            holder.Title.setText(Utils.getName(item.Obj));
            if (Utils.isFolder(item.Obj)) {
                ImageLoadObject.cancel(holder.Image);
                getSvgHolder().applySVG(holder.Image, R.raw.ic_folder_24px, ThemeHolder.getIconColor());
            } else {
                if (Utils.isFileIconSupport(item.Obj)) {
                    holder.Image.setLoadObject(MIM.by(Utils.MIM_ICON_KEY).to(holder.Image, Utils.getKey(item.Obj)).object(item.Obj));
                } else {
                    ImageLoadObject.cancel(holder.Image);
                }
                getSvgHolder().applySVG(holder.Image, R.raw.ic_file_24px, Utils.getIconColor(item.Obj));
            }
            holder.ensureState(item);
            holder.BtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(holder.itemView, position);
                }
            });
            holder.BtnDoAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalCache.notify(item);
                }
            });
            mBaseFragment.getSvgHolder().applySVG(holder.BtnDoAction, item.getStateIcon(), ThemeHolder.getIconColor());
        }

        @Override
        public void ensureCheckMask(RecyclerView.ViewHolder holder, boolean inSelectionMode, boolean checked) {

        }

        public class Holder extends RecyclerView.ViewHolder {

            public TextView Title;
            public TextView Size;
            public RecyclingImageView Image;
            public ImageButton BtnDelete;
            public ImageButton BtnDoAction;

            public Holder(View itemView) {
                super(itemView);

                Title = (TextView) itemView.findViewById(R.id.title);
                Size = (TextView) itemView.findViewById(R.id.additional);
                Image = (RecyclingImageView) itemView.findViewById(R.id.image);
                BtnDelete = (ImageButton) itemView.findViewById(R.id.delete);
                BtnDoAction = (ImageButton) itemView.findViewById(R.id.do_action_button);

                Title.setTypeface(Typefacer.rRegular);
                Size.setTypeface(Typefacer.rRegular);

                mBaseFragment.getSvgHolder().applySVG(BtnDelete, R.raw.ic_close, ThemeHolder.getIconColor());

                itemView.setTag(this);
            }

            public void ensureState(GlobalCache.ActionObject aObject) {
                if (aObject.getStatus() == GlobalCache.ACTION_STATUS.FINISH) {
                    Size.setText(R.string.finish);
                    Size.setTextColor(ThemeHolder.getPrimaryColor());
                }else if (aObject.getStatus() == GlobalCache.ACTION_STATUS.PROCESS){
                    Size.setText(R.string.running);
                    Size.append("...");
                    Size.setTextColor(Color.parseColor("#F44336"));
                }else {
                    Size.setText(Utils.isFolder(aObject.Obj) ? Utils.getDate(aObject.Obj) : Utils.readableFileSize(Utils.getSize(aObject.Obj)));
                }
            }

        }

    }

}
