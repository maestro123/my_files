package maestro.filemanager.ui;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMCirclePostMaker;
import com.dream.android.mim.RecyclingImageView;
import maestro.filemanager.R;
import maestro.filemanager.drives.DriveDUManager;
import maestro.filemanager.drives.DriveDUObject;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MArchiveFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.*;
import maestro.filemanager.utils.data.FileManagerDatabase;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.menu.MPopupMenu;
import maestro.support.v1.menu.MPopupMenu.MPopupMenuItem;
import maestro.support.v1.svg.SVGHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Artyom on 6/28/2015.
 */
public class UniqAdapter extends BaseUpdateAdapter<Object, UniqAdapter.Holder> implements CompoundButton.OnCheckedChangeListener {

    private BaseFragment mFragment;
    private MIM mim;
    private boolean isCloudAdapter;

    public UniqAdapter(BaseFragment fragment, int spanCount) {
        super(fragment.getActivity(), spanCount);
        mFragment = fragment;
        isCloudAdapter = mFragment instanceof BaseDriveFragment;
        setWithHeader(true);
        mim = new MIM(getContext()).maker(new MIconMaker(getContext()))
                .postMaker(new MIMCirclePostMaker((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()), "#cacaca"));
    }

    @Override
    public void update(ArrayList<Object> items) {
        if (items != null && items.size() > 0) {
            ArrayList<Object> outObjects = new ArrayList<>();
            boolean filesHeaderAdd = false;
            boolean folderHeaderAdd = false;
            for (int i = 0; i < items.size(); i++) {
                if (!filesHeaderAdd && !Utils.isFolder(items.get(i))) {
                    outObjects.add(new RowDivider(R.string.files));
                    filesHeaderAdd = true;
                } else if (!folderHeaderAdd && Utils.isFolder(items.get(i))) {
                    outObjects.add(new RowDivider(R.string.folders));
                    folderHeaderAdd = true;
                }
                outObjects.add(items.get(i));
            }
            super.update(outObjects);
        } else
            super.update(items);
    }

    @Override
    public RecyclerView.ViewHolder createHeader(LayoutInflater inflater) {
        Space frameLayout = new Space(getContext());
        frameLayout.setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
        return new SimpleViewHolder(frameLayout);
    }

    @Override
    public Holder createItemHolder(LayoutInflater inflater) {
        return new Holder(getInflater().inflate(R.layout.file_row_item_view, null), getSvgHolder());
    }

    @Override
    public void onBindItemHolder(Holder holder, final Object item, int position) {
        if (Utils.isCloudObject(item)) {
            if (!Utils.isFolder(item)) {
                DriveDUObject duObject = DriveDUManager.getInstance().get(Utils.getKey(item), null);
                if (duObject == null) {
                    changeVisibility(holder.Progress, View.GONE);
                } else {
                    changeVisibility(holder.Progress, View.VISIBLE);
                    holder.Progress.setProgress(duObject.getProgress());
                    duObject.addListener(holder);
                }
                changeVisibility(holder.DownloadIcon, View.VISIBLE);
                holder.setDownloadIcon(getSvgHolder(), new File(Utils.getDirectoryFile(item), Utils.getName(item)).exists());
            } else {
                changeVisibility(holder.Progress, View.GONE);
                changeVisibility(holder.DownloadIcon, View.GONE);
            }
        }
        holder.BtnMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPopupMenu(v, item);
            }
        });
        holder.Title.setText(Utils.getName(item));
        if (Utils.isFolder(item)) {
            ImageLoadObject.cancel(holder.Image);
            holder.Size.setText(Utils.getDate(item));
            getSvgHolder().applySVG(holder.Image, R.raw.ic_folder_24px, ThemeHolder.getIconColor());
        } else {
            if (Utils.isFileIconSupport(item)) {
                holder.Image.setLoadObject(mim.to(holder.Image, Utils.getKey(item)).object(item));
            } else {
                ImageLoadObject.cancel(holder.Image);
                getSvgHolder().applySVG(holder.Image, R.raw.ic_file_24px, Utils.getIconColor(item));
            }
            holder.Size.setText(Utils.readableFileSize(Utils.getSize(item)));
        }
    }

    @Override
    public void ensureCheckMask(RecyclerView.ViewHolder viewHolder, boolean inSelectionMode, boolean checked) {
        if (viewHolder instanceof Holder) {
            Object object = getItem(viewHolder.getAdapterPosition());
            Holder holder = (Holder) viewHolder;
            if (Utils.isCloudObject(object) && Utils.isFolder(object)) {
                holder.CheckBox.setChecked(false);
                changeVisibility(holder.CheckBox, View.GONE);
                changeVisibility(holder.BtnMoreOptions, inSelectionMode ? View.GONE : View.VISIBLE);
                changeVisibility(holder.DownloadIcon, View.GONE);
            } else if (inSelectionMode) {
                holder.CheckBox.setChecked(checked);
                changeVisibility(holder.CheckBox, View.VISIBLE);
                changeVisibility(holder.BtnMoreOptions, View.GONE);
                changeVisibility(holder.DownloadIcon, View.GONE);
            } else {
                holder.CheckBox.setChecked(false);
                changeVisibility(holder.CheckBox, View.GONE);
                changeVisibility(holder.BtnMoreOptions, View.VISIBLE);
                changeVisibility(holder.DownloadIcon, View.VISIBLE);
            }
        }
    }

    private final void changeVisibility(View view, int state) {
        if (view.getVisibility() != state) {
            view.setVisibility(state);
        }
    }

    private final void openPopupMenu(final View anchor, final Object item) {
        ArrayList<MPopupMenuItem> items = new ArrayList<>();
        items.add(new MPopupMenuItem(getResources().getString(R.string.copy), R.raw.copy).setId(R.id.copy));
        if (!isCloudAdapter)
            items.add(new MPopupMenuItem(getResources().getString(R.string.cut), R.raw.cut).setId(R.id.cut));
        items.add(new MPopupMenuItem(getString(R.string.rename), R.raw.create).setId(R.id.rename));
        items.add(new MPopupMenuItem(getString(R.string.info), R.raw.info).setId(R.id.info));
        if (item instanceof MJavaFile)
            items.add(new MPopupMenuItem(getString(R.string.zip), R.raw.ic_zip).setId(R.id.zip));
        if (item instanceof MArchiveFile || item instanceof MArchiveEntryFile)
            items.add(new MPopupMenuItem(getString(R.string.unzip), R.raw.ic_zip).setId(R.id.unzip));
        if (!isCloudAdapter) {
            boolean remove = FileManagerDatabase.Instance.isInBookmarks((MFile) item);
            items.add(new MPopupMenuItem(getString(remove ? R.string.remove_from_bookmark : R.string.add_to_bookmark),
                    remove ? R.raw.ic_bookmark_remove : R.raw.ic_bookmark_24px).setId(R.id.add_to_bookmark));
        }
        items.add(new MPopupMenuItem(getString(R.string.delete), R.raw.delete).setId(R.id.delete));
        if (isCloudAdapter) {
            items.add(new MPopupMenu.MPopupMenuItem(getString(R.string.download), R.raw.ic_file_download_24px).setId(R.id.download));
        }
        FPopupMenu mPopupMenu = new FPopupMenu(getContext(), getSvgHolder(), items);
        mPopupMenu.setOnMenuItemClickListener(new maestro.support.v1.menu.MPopupMenu.OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(maestro.support.v1.menu.MPopupMenu.MPopupMenuItem menuItem) {
                switch (menuItem.Id) {
                    case R.id.cut:
                        GlobalCache.add(Utils.getPath(item), new GlobalCache.ActionObject().setAction(GlobalCache.OBJECT_ACTION.CUT)
                                .setKey(Utils.getKey(item)).setObj(item));
                        break;
                    case R.id.copy:
                        GlobalCache.add(Utils.getPath(item), new GlobalCache.ActionObject().setAction(GlobalCache.OBJECT_ACTION.COPY)
                                .setKey(Utils.getKey(item)).setObj(item));
                        break;
                    case R.id.rename:
                        FileNameInputDialog.makeInstance(FileNameInputDialog.TYPE.RENAME, getResources().getString(R.string.rename), Utils.getName(item), item)
                                .show(mFragment.getChildFragmentManager(), FileNameInputDialog.TAG);
                        break;
                    case R.id.zip:
                        GlobalCache.ActionObject actionObject = new GlobalCache.ActionObject()
                                .setAction(GlobalCache.OBJECT_ACTION.ZIP).setKey(Utils.getKey(item)).setObj(item);
                        GlobalCache.notify(actionObject);


                        break;
                    case R.id.unzip:

                        break;
                    case R.id.info:

                        break;
                    case R.id.add_to_bookmark:
                        if (item instanceof MFile) {
                            MFile file = (MFile) item;
                            if (FileManagerDatabase.Instance.isInBookmarks(file)) {
                                FileManagerDatabase.Instance.removeBookmark(file);
                            } else {
                                FileManagerDatabase.Instance.addBookmark(file);
                            }
                            if (mFragment instanceof FilesListFragment && StorageUtils.isBookmarksFolder(((FilesListFragment) mFragment).getLastHistory().Path)) {
                                ((FilesListFragment) mFragment).loadSilent();
                            }
                        }
                        break;
                    case R.id.delete:
                        ConfirmDialog.makeInstance(ConfirmDialog.TYPE.DELETE_FILE, String.format(getString(R.string.delete_confirm_msg), Utils.getName(item)),
                                getString(R.string.delete), getResources().getString(android.R.string.cancel), item)
                                .show(mFragment.getChildFragmentManager(), ConfirmDialog.TAG);
                        break;
                    case R.id.download:
                        File javaFile = Utils.getDirectoryFile(item);
                        javaFile.mkdir();
                        DriveDUObject duObject = new DriveDUObject(DriveDUObject.TYPE.DOWNLOAD, ((BaseDriveFragment) mFragment).getDriveManager(),
                                item, javaFile.getPath(), Utils.getKey(item));
                        Holder holder = (Holder) anchor.getTag();
                        holder.Progress.setVisibility(View.VISIBLE);
                        duObject.addListener(holder);
                        DriveDUManager.getInstance().download(duObject);
                        break;
                }
            }
        });
        mPopupMenu.show(anchor);
    }

    public List<Object> collectFiles(String pattern) {
        if (haveItems()) {
            ArrayList<Object> out = new ArrayList<>();
            for (Object object : getItems()) {
                if (!(object instanceof RowDivider)) {
                    String name = Utils.getName(object);
                    if (Utils.match(name, pattern)) {
                        if (Utils.isCloudObject(object)) {
                            File file = new File(Utils.getDirectoryFile(object), Utils.getName(object));
                            if (file.exists()) {
                                out.add(new MJavaFile(file));
                                continue;
                            }
                        }
                        out.add(object);
                    }
                }
            }
            return out;
        }
        return Collections.emptyList();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public static class Holder extends RecyclerView.ViewHolder implements DriveDUObject.DriveDUListener {

        public TextView Title;
        public TextView Size;
        public RecyclingImageView Image;
        public ImageButton BtnMoreOptions;
        public ImageView DownloadIcon;
        public ProgressIndicator Progress;
        public CheckBox CheckBox;
        private SVGHelper.SVGHolder mSvgHolder;

        public Holder(View itemView, SVGHelper.SVGHolder svgHolder) {
            super(itemView);
            mSvgHolder = svgHolder;
            Title = (TextView) itemView.findViewById(R.id.title);
            Size = (TextView) itemView.findViewById(R.id.additional);
            Image = (RecyclingImageView) itemView.findViewById(R.id.image);
            Progress = (ProgressIndicator) itemView.findViewById(R.id.progress_indicator);
            BtnMoreOptions = (ImageButton) itemView.findViewById(R.id.more_options_button);
            DownloadIcon = (ImageView) itemView.findViewById(R.id.download_icon);
            CheckBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            itemView.findViewById(R.id.download_progress_layout).setVisibility(View.VISIBLE);
            BtnMoreOptions.setTag(this);

            Title.setTypeface(Typefacer.rRegular);
            Size.setTypeface(Typefacer.rRegular);

            svgHolder.applySVG(BtnMoreOptions, R.raw.more, ThemeHolder.getIconColor());
        }

        @Override
        public void onDUStart(DriveDUObject.TYPE type) {
        }

        @Override
        public void onDUProgress(DriveDUObject.TYPE type, int progress) {
            if (Progress.getVisibility() == View.VISIBLE) {
                Progress.setProgress(progress);
            }
        }

        @Override
        public void onDUFinish(DriveDUObject.TYPE type, Object error) {
            if (Progress.getVisibility() == View.VISIBLE) {
                Progress.setVisibility(View.GONE);
            }
            if (error == null) {
                setDownloadIcon(mSvgHolder, true);
            }
        }

        public void setDownloadIcon(SVGHelper.SVGHolder svgHolder, boolean downloaded) {
            if (svgHolder != null) {
                svgHolder.applySVG(DownloadIcon, R.raw.ic_file_download_24px, downloaded ? ThemeHolder.getPrimaryColor() : ThemeHolder.getIconColor(), SVGHelper.DPI - 18 / 24f);
            }
        }

    }

}
