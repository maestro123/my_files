package maestro.filemanager.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.GoogleDriveManager;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.Loaders;

/**
 * Created by Artyom on 5/26/2015.
 */
public class GoogleDriveFragment extends BaseDriveFragment<Object> {

    public static final String TAG = GoogleDriveFragment.class.getSimpleName();

    @Override
    public BaseDriveManager getDriveManager() {
        return GoogleDriveManager.getInstance();
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public Loader getSearchLoader(String query) {
        return null;
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        return new Loaders.GoogleDriveLoader(getActivity(), getLastHistory().Path);
    }

    @Override
    public BaseUpdateAdapter buildAdapter(boolean search) {
        return null;
    }

    @Override
    public void onItemClick(Object item) {

    }
}
