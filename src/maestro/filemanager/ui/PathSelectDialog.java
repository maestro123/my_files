package maestro.filemanager.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.RecyclingImageView;
import maestro.filemanager.R;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.GlobalCache;
import maestro.filemanager.utils.StorageUtils;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.Utils;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.fview.FilterEditText;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Artyom on 6/30/2015.
 * <p/>
 * Parent fragment or activity should implement OnPathSelectListener (or set it).
 * After confirm click, there will be called method in listener with some params (see makeInstance)
 */

public class PathSelectDialog extends BaseFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<ArrayList<MJavaFile>> {

    public static final String TAG = PathSelectDialog.class.getSimpleName();

    private static final String PARAM_HISTORY = "history";
    private static final String PARAM_START_PATH = "start_path";
    private static final String PARAM_ACTION = "action";
    private static final String PARAM_START_NAME = "start_name";
    private static final String PARAM_START_FILE = "start_file";

    private RecyclerView mList;
    private TextView mTitle;
    private FilterEditText mEditText;
    private ImageView mCloseIcon, mDoneIcon;
    private LinearLayout mIndicatorParent;
    private FolderAdapter mAdapter;
    private View mEmptyView;
    private HorizontalScrollView mScrollView;
    private MJavaFile mStartFile;
    private ArrayList<MJavaFile> mHistory = new ArrayList<>();
    private OnPathSelectListener mListener;

    private int mNavigationPadding;
    private int mNavigationLeftPadding;

    /**
     * @param startPath default saving path
     * @param startName default name
     * @param action    action (see also{@link maestro.filemanager.utils.GlobalCache.OBJECT_ACTION} for more information)
     * @return new PathSelectDialog.class instance
     */

    public static PathSelectDialog makeInstance(String startPath, String startName, GlobalCache.OBJECT_ACTION action) {
        PathSelectDialog fragment = new PathSelectDialog();
        Bundle args = new Bundle(3);
        args.putString(PARAM_START_PATH, startPath);
        args.putString(PARAM_START_NAME, startName);
        args.putSerializable(PARAM_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnPathSelectListener {
        void onPathSelect(String originPath, String newPath, GlobalCache.OBJECT_ACTION action);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.FM_Theme_Dialog_Fullscreen);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP ? onBackPressed() : false;
            }
        });
        return d;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.path_select_dialog, null);
        mTitle = (TextView) v.findViewById(R.id.title);
        mEditText = (FilterEditText) v.findViewById(R.id.edit_text);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCloseIcon = (ImageView) v.findViewById(R.id.close_icon);
        mDoneIcon = (ImageView) v.findViewById(R.id.done_icon);
        mIndicatorParent = (LinearLayout) v.findViewById(R.id.indicator_parent);
        mScrollView = (HorizontalScrollView) v.findViewById(R.id.navigation_bar_scroll_view);
        mEmptyView = v.findViewById(R.id.empty_view);
        mEditText.setColors(Color.WHITE, ThemeHolder.getTextSecondaryColor());
        mEditText.setTypeface(Typefacer.rRegular);
        mTitle.setTypeface(Typefacer.rRegular);

        mCloseIcon.setOnClickListener(this);
        mDoneIcon.setOnClickListener(this);

        getSvgHolder().applySVG(mCloseIcon, R.raw.ic_close, Color.WHITE);
        getSvgHolder().applySVG(mDoneIcon, R.raw.ic_check, Color.WHITE);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mHistory = savedInstanceState.getParcelableArrayList(PARAM_HISTORY);
            mStartFile = savedInstanceState.getParcelable(PARAM_START_FILE);
        } else if (getArguments() != null) {
            mEditText.setText(getArguments().getString(PARAM_START_NAME));
        }

        if (mListener == null && getParentFragment() instanceof OnPathSelectListener) {
            mListener = (OnPathSelectListener) getParentFragment();
        }

        mList.setAdapter(mAdapter = new FolderAdapter(getActivity(), 1));
        mAdapter.setOnItemClickAction(new BaseUpdateAdapter.OnItemClickActionListener() {
            @Override
            public void onItemClick(Object item) {
                mHistory.add((MJavaFile) item);
                load();
            }

            @Override
            public boolean onItemLongClick(Object item) {
                return false;
            }
        });
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                if (mHistory.isEmpty()) {
                    String startPath = getArguments() != null ? getArguments().getString(PARAM_START_PATH) : null;
                    prepareHistory(mStartFile = new MJavaFile(!TextUtils.isEmpty(startPath)
                            ? new File(startPath) : Environment.getExternalStorageDirectory()));
                }
                load();
            }
        });
    }

    public void setOnPathSelectListener(OnPathSelectListener listener) {
        mListener = listener;
    }

    private void prepareHistory(MJavaFile startFile) {
        mHistory.add(startFile);
        MJavaFile pFile = startFile;
        while (true) {
            File parent = pFile.getOriginalObject().getParentFile();
            if (parent != null) {
                mHistory.add(pFile = new MJavaFile(parent));
            } else {
                break;
            }
        }
        Collections.reverse(mHistory);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PARAM_HISTORY, mHistory);
        outState.putParcelable(PARAM_START_FILE, mStartFile);
    }

    public void load() {
        if (getLoaderManager().getLoader(TAG.hashCode()) != null) {
            getLoaderManager().restartLoader(TAG.hashCode(), null, this);
        } else {
            getLoaderManager().initLoader(TAG.hashCode(), null, this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done_icon:
                if (mListener != null) {
                    String name = mEditText.getText().toString();
                    if (!TextUtils.isEmpty(name)) {
                        if (getArguments() != null) {
                            mListener.onPathSelect(mStartFile.getPath(), mHistory.get(mHistory.size() - 1) + "/" + name,
                                    (GlobalCache.OBJECT_ACTION) getArguments().getSerializable(PARAM_ACTION));
                        } else {
                            mListener.onPathSelect(mStartFile.getPath(), name, null);
                        }
                    }
                }
            case R.id.close_icon:
                dismiss();
                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mHistory.size() > 1
                && mHistory.contains(mStartFile)
                && !mHistory.get(mHistory.size() - 1).equals(mStartFile)) {
            mHistory.remove(mHistory.size() - 1);
            load();
            return true;
        }
        return super.onBackPressed();
    }

    @Override
    public Loader<ArrayList<MJavaFile>> onCreateLoader(int i, Bundle bundle) {
        prepareNavigation();
        mEmptyView.setVisibility(View.GONE);
        return new FolderLoader(getActivity(), mHistory.get(mHistory.size() - 1));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MJavaFile>> loader, ArrayList<MJavaFile> mJavaFiles) {
        mAdapter.update(mJavaFiles);
        if (mAdapter.getItemCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MJavaFile>> loader) {

    }


    public class FolderAdapter extends BaseUpdateAdapter<MJavaFile, FolderAdapter.Holder> {

        public FolderAdapter(Context context, int spanCount) {
            super(context, spanCount);
        }

        @Override
        public Holder createItemHolder(LayoutInflater inflater) {
            return new Holder(inflater.inflate(R.layout.path_select_item_view, null));
        }

        @Override
        public void onBindItemHolder(Holder holder, MJavaFile item, int position) {
            holder.Title.setText(item.getTitle());
            holder.Size.setText(Utils.getDate(item));
        }

        @Override
        public void ensureCheckMask(RecyclerView.ViewHolder holder, boolean inSelectionMode, boolean checked) {

        }

        public class Holder extends RecyclerView.ViewHolder {

            public TextView Title;
            public TextView Size;
            public RecyclingImageView Image;

            public Holder(View itemView) {
                super(itemView);
                Title = (TextView) itemView.findViewById(R.id.title);
                Size = (TextView) itemView.findViewById(R.id.additional);
                Image = (RecyclingImageView) itemView.findViewById(R.id.image);
                getSvgHolder().applySVG(Image, R.raw.ic_folder_24px, ThemeHolder.getIconColor());
            }
        }

    }

    public static final class FolderLoader extends AsyncTaskLoader<ArrayList<MJavaFile>> {

        private MJavaFile rFile;

        public FolderLoader(Context context, MJavaFile rootFile) {
            super(context);
            rFile = rootFile;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<MJavaFile> loadInBackground() {
            File[] listFile = rFile.getOriginalObject().listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.isDirectory();
                }
            });
            if (listFile != null && listFile.length > 0) {
                ArrayList<MJavaFile> mJavaFiles = new ArrayList<>();
                for (File file : listFile) {
                    mJavaFiles.add(new MJavaFile(file));
                }
                return mJavaFiles;
            }
            return null;
        }
    }

    public void prepareNavigation() {
        mNavigationPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        mNavigationLeftPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
        final int size = mHistory.size();

        while (mIndicatorParent.getChildCount() > size - 1) {
            mIndicatorParent.removeViewAt(0);
        }

        for (int i = 0; i < size; i++) {
            View child = mIndicatorParent.getChildAt(i);
            if (child == null) {
                mIndicatorParent.addView(child = makeHistoryView(null, mHistory.get(i), i == size - 1, i == 0));
            } else {
                makeHistoryView(child, mHistory.get(i), i == size - 1, i == 0);
            }
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HistoryHolder holder = (HistoryHolder) v.getTag();
                    if (holder != null) {
                        final int index = mHistory.indexOf(holder.Write);
                        if (index != -1 && index != mHistory.size() - 1) {
                            do {
                                mHistory.remove(mHistory.size() - 1);
                            } while (index != mHistory.size() - 1);
                            load();
                        }
                    }
                }
            });
        }
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(View.FOCUS_RIGHT);
            }
        });
    }

    private final View makeHistoryView(View view, MJavaFile write, boolean isCurrent, boolean isFirst) {
        LinearLayout linearLayout;
        HistoryHolder mHolder;
        if (view == null) {
            linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.history_navigation_view, null);
            view = linearLayout;
            mHolder = new HistoryHolder(view);
        } else {
            mHolder = (HistoryHolder) view.getTag();
        }
        mHolder.Write = write;
        String title = null;
        if (isFirst) {
            List<StorageUtils.StorageInfo> mInfos = StorageUtils.getStorageList();
            for (StorageUtils.StorageInfo info : mInfos) {
                if (info.path.equals(write.getPath())) {
                    title = info.getDisplayName(getActivity());
                    break;
                }
            }
        }
        if (title == null) {
            title = write.getPath().equals("/") ? getString(R.string.root) : write.getTitle();
        }

        mHolder.Title.setText(title);
        mHolder.Title.setTypeface(isCurrent ? Typefacer.rMedium : Typefacer.rRegular);
        view.setPadding(isFirst ? mNavigationLeftPadding : mNavigationPadding / 2, mNavigationPadding, 0, mNavigationPadding);
        if (!isCurrent) {
            mHolder.Icon.setVisibility(View.VISIBLE);
            view.setAlpha(.5f);
            view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)));
        } else {
            mHolder.Icon.setVisibility(View.GONE);
            view.setAlpha(1f);
            view.setLayoutParams(new LinearLayout.LayoutParams(mScrollView.getWidth() - mNavigationLeftPadding, getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)));
        }
        return view;
    }

    private class HistoryHolder {

        private TextView Title;
        private ImageView Icon;
        private MJavaFile Write;

        public HistoryHolder(View v) {
            Title = (TextView) v.findViewById(R.id.title);
            Icon = (ImageView) v.findViewById(R.id.icon);
            getSvgHolder().applySVG(Icon, R.raw.ic_arrow_right, Color.WHITE);
            v.setTag(this);
        }

    }

}
