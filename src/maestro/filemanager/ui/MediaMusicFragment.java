package maestro.filemanager.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMUtils;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.ui.base.BaseMediaFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.ui.media.MusicViewerFragment;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.SpaceItemDecoration;
import maestro.filemanager.utils.Utils;

import java.util.ArrayList;

/**
 * Created by artyom on 25.4.15.
 */
public class MediaMusicFragment extends BaseMediaFragment implements LoaderManager.LoaderCallbacks<ArrayList<Album>>,
        BaseUpdateAdapter.OnItemClickActionListener<Album> {

    public static final String TAG = MediaMusicFragment.class.getSimpleName();

    private RecyclerView mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private MusicAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.media_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setHasFixedSize(true);
        mList.addItemDecoration(new SpaceItemDecoration(computeSpanCount(), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()),
                getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material), false));

        mLayoutManager = new GridLayoutManager(getActivity(), computeSpanCount());
        mList.setLayoutManager(mLayoutManager);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new MusicAdapter(getActivity(), computeSpanCount()));
        mAdapter.setOnItemClickAction(this);
        load();
    }

    @Override
    public void load() {
        if (getLoaderManager().getLoader(TAG.hashCode()) != null) {
            getLoaderManager().restartLoader(TAG.hashCode(), null, this);
        } else {
            getLoaderManager().initLoader(TAG.hashCode(), null, this);
        }
    }

    @Override
    public Loader<ArrayList<Album>> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.MusicAlbumLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Album>> loader, ArrayList<Album> albums) {
        mAdapter.update(albums);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Album>> loader) {

    }

    public int computeSpanCount() {
        final boolean is7inch = getResources().getBoolean(R.bool.is7inch);
        final boolean is10inch = getResources().getBoolean(R.bool.is10inch);
        final boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        return !is7inch && !is10inch && !isLandscape ? 2 : is7inch ? isLandscape ? 5 : 3 : is10inch ? isLandscape ? 6 : 4 : 2;
    }

    @Override
    public void onItemClick(Album item) {
        MusicViewerFragment.makeInstance(item).show(getChildFragmentManager(), MusicViewerFragment.TAG);
    }

    @Override
    public boolean onItemLongClick(Album item) {
        return false;
    }

    public final class MusicAdapter extends BaseUpdateAdapter<Album, MusicAdapter.Holder> {

        private int size;

        public MusicAdapter(Context context, int spanCount) {
            super(context, spanCount);
            size = getResources().getDisplayMetrics().widthPixels / spanCount;
        }

        public MusicAdapter(Context context, int spanCount, int topPadding) {
            super(context, spanCount, topPadding);
        }

        @Override
        public Holder createItemHolder(LayoutInflater inflater) {
            View v = inflater.inflate(R.layout.album_item_view, null);
            Holder holder = new Holder(v);
            holder.Image.getLayoutParams().width = size;
            holder.Image.getLayoutParams().height = size;
            return holder;
        }

        @Override
        public void onBindItemHolder(final MusicAdapter.Holder holder, Album item, int position) {
            holder.AlbumItemParent.setBackgroundColor(Color.TRANSPARENT);
            MIM.by(Utils.MIM_ICON_KEY).to(holder.Image, item.artPath).object(item).analyze(true)
                    .listener(new ImageLoadObject.OnImageLoadEventListener() {
                        @Override
                        public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
                            if (event == IMAGE_LOAD_EVENT.FINISH) {
                                if (loadObject.getAnalyzedColors() != null && loadObject.getAnalyzedColors().length > 0) {
                                    ColorDrawable drawable = (ColorDrawable) holder.AlbumItemParent.getBackground();
                                    if (drawable == null) {
                                        drawable = new ColorDrawable();
                                        holder.AlbumItemParent.setBackgroundDrawable(drawable);
                                    }
                                    drawable.setColor(MIMUtils.blendColors(loadObject.getAnalyzedColors()[loadObject.getAnalyzedColors().length - 1],
                                            Color.BLACK, 0.75f));
                                    ObjectAnimator.ofInt(drawable, "alpha", 0, 255).start();
                                }
                            }
                        }
                    }).size(size, size).async();
            holder.Title.setText(item.Title);
            holder.Author.setText(item.author);
        }

        @Override
        public void ensureCheckMask(RecyclerView.ViewHolder holder, boolean inSelectionMode, boolean checked) {

        }

        class Holder extends RecyclerView.ViewHolder {

            ImageView Image;
            TextView Title;
            TextView Author;
            LinearLayout AlbumItemParent;

            public Holder(View itemView) {
                super(itemView);
                Image = (ImageView) itemView.findViewById(R.id.image);
                Title = (TextView) itemView.findViewById(R.id.title);
                Author = (TextView) itemView.findViewById(R.id.author);
                AlbumItemParent = (LinearLayout) itemView.findViewById(R.id.album_item_parent);
            }

        }

    }
}
