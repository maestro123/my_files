package maestro.filemanager.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.BoxDriveManager;
import maestro.filemanager.files.box.BoxFile;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.Loaders;

import java.util.ArrayList;

/**
 * Created by Artyom on 3/18/2015.
 */
public class BoxDriveFragment extends BaseDriveFragment<BoxFile> {

    public static String TAG = BoxDriveFragment.class.getSimpleName();

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public Loader getSearchLoader(String query) {
        return null;
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        super.getBaseLoader(id, args);
        return new Loaders.BoxDriveLoader(getActivity(), getLastHistory().Path);
    }

    @Override
    public BaseDriveManager getDriveManager() {
        return BoxDriveManager.getInstance();
    }

    @Override
    public String getKey() {
        return TAG;
    }

}
