package maestro.filemanager.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import maestro.filemanager.Main;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.utils.IStorageInfoGetter;
import maestro.filemanager.utils.StorageUtils;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.theme.ThemeHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 9/11/14.
 */
public class NavigationFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = NavigationFragment.class.getSimpleName();

    private ListView mList;
    private NavigationAdapter mAdapter;
    private ImageView mIcon;
    private TextView mUserName;
    private TextView mSpaceInfo;
    private IStorageInfoGetter mInfoGetter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.navigation_fragment_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setBackgroundColor(Color.WHITE);
        mList.setDividerHeight(0);

        final View mHeaderView = View.inflate(getActivity(), R.layout.navigation_header, null);
        mIcon = (ImageView) mHeaderView.findViewById(R.id.icon);
        mUserName = (TextView) mHeaderView.findViewById(R.id.user_name);
        mSpaceInfo = (TextView) mHeaderView.findViewById(R.id.space_info);
        mUserName.setTypeface(Typefacer.rMedium);
        mSpaceInfo.setTypeface(Typefacer.rRegular);
        mList.addHeaderView(mHeaderView, null, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new NavigationAdapter(getActivity()));
        mList.setOnItemClickListener(this);
        mAdapter.update(getItems());
        if (savedInstanceState == null) {
            List<StorageUtils.StorageInfo> mInfo = StorageUtils.getStorageList();
            for (int i = 0; i < mAdapter.getCount(); i++) {
                NavigationItem item = mAdapter.getItem(i);
                for (StorageUtils.StorageInfo info : mInfo) {
                    if (item.Type != null && item.Type.Type == Main.TYPE_FILES
                            && info.path.equals(item.Type.Path)) {
                        main.openFragment(item.Type);
                        main.commit();
                        return;
                    }
                }
            }
            NavigationItem item = mAdapter.getItem(1);
            main.openFragment(item.Type);
            main.commit();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (main != null) {
            main.openFragment(mAdapter.getItem(position - 1).Type);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setStorageInfoGetter(IStorageInfoGetter getter) {
        if (mInfoGetter != null) {
            mInfoGetter.cancel();
        }
        mInfoGetter = getter;
        if (getActivity() != null) {
            mInfoGetter.prepareInfo(mUserName, mSpaceInfo);
            mInfoGetter.prepareIcon(mIcon);
        }
    }

    private final class NavigationAdapter extends BaseAdapter {

        private static final int TYPE_SPACE = -2;
        public static final int TYPE_DIVIDER = -1;
        public static final int TYPE_NORMAL = 1;

        private Context mContext;
        private LayoutInflater mInflater;
        private NavigationItem[] mItems;
        private int verticalPadding;
        private int padding8dp;

        public NavigationAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            verticalPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());
            padding8dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, context.getResources().getDisplayMetrics());
        }

        public void update(NavigationItem[] items) {
            mItems = items;
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.length : 0;
        }

        @Override
        public NavigationItem getItem(int position) {
            return mItems[position];
        }

        @Override
        public long getItemId(int i) {
            NavigationItem item = getItem(i);
            return item.AdapterType == TYPE_NORMAL ? item.Type.Type : item.AdapterType;
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position).AdapterType;
        }

        @Override
        public boolean isEnabled(int position) {
            return getItem(position).AdapterType == TYPE_NORMAL;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            final int itemType = getItemViewType(position);
            if (itemType == TYPE_SPACE) {
                if (v == null || !(v.getTag() instanceof SpaceHolder)) {
                    v = new Space(getActivity());
                    v.setMinimumHeight(verticalPadding);
                    v.setTag(new SpaceHolder());
                }
            } else if (itemType == TYPE_DIVIDER) {
                DividerHolder holder;
                if (v == null || !(v.getTag() instanceof DividerHolder)) {
                    v = mInflater.inflate(R.layout.navigation_item_divider, null);
                    holder = new DividerHolder(v);
                } else {
                    holder = (DividerHolder) v.getTag();
                }
                NavigationItem item = getItem(position);
                if (!TextUtils.isEmpty(item.Title)) {
                    holder.Title.setText(getItem(position).Title);
                    holder.Title.setVisibility(View.VISIBLE);
                    holder.SpaceView.setVisibility(View.GONE);
                } else {
                    holder.Title.setVisibility(View.GONE);
                    holder.SpaceView.setVisibility(View.VISIBLE);
                }
            } else if (itemType == TYPE_NORMAL) {
                Holder holder;
                if (v == null || !(v.getTag() instanceof Holder)) {
                    v = mInflater.inflate(R.layout.navigation_item_view, null);
                    holder = new Holder(v);
                    v.setTag(holder);
                } else {
                    holder = (Holder) v.getTag();
                }
                NavigationItem item = getItem(position);
                int actualColor = item.Type.equals(main.getCurrentOpenType()) ? ThemeHolder.IconPrimaryColor : ThemeHolder.IconSecondaryColor;
                getSvgHolder().applySVG(holder.Image, item.Icon, actualColor);
                holder.Title.setText(item.Title);
                holder.Title.setTextColor(actualColor);
            }
            return v;
        }

        class Holder {
            TextView Title;
            ImageView Image;

            public Holder(View itemView) {
                Title = (TextView) itemView.findViewById(R.id.title);
                Image = (ImageView) itemView.findViewById(R.id.image);
                Title.setTypeface(Typefacer.rMedium);
            }
        }

        class SpaceHolder {

        }

        class DividerHolder {
            TextView Title;
            View SpaceView;

            public DividerHolder(View itemView) {
                Title = (TextView) itemView.findViewById(R.id.title);
                Title.setTypeface(Typefacer.rMedium);
                SpaceView = itemView.findViewById(R.id.additional_space);
            }
        }

    }

    public final class NavigationItem {

        public Main.TypeHolder Type;
        public String Title;
        public int Icon;
        public int AdapterType = NavigationAdapter.TYPE_NORMAL;

        public NavigationItem(Main.TypeHolder typeHolder, String title, int icon) {
            Type = typeHolder;
            Title = title;
            Icon = icon;
        }

        public NavigationItem(int adapterType) {
            AdapterType = adapterType;
        }

        public NavigationItem(String title, int adapterType) {
            AdapterType = adapterType;
            Title = title;
        }

    }

    private NavigationItem[] getItems() {
        final List<StorageUtils.StorageInfo> infoList = StorageUtils.getStorageList();
        final int size = infoList.size();
        final ArrayList<NavigationItem> items = new ArrayList<NavigationItem>();
        items.add(new NavigationItem(NavigationAdapter.TYPE_SPACE));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_FILES, "/"), getResources().getString(R.string.root), R.raw.ic_folder_24px));
        for (int i = 0; i < size; i++) {
            items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_FILES, infoList.get(i).path),
                    infoList.get(i).getDisplayName(getActivity()), R.raw.ic_folder_24px));
        }
        final String downloadsPath = StorageUtils.getDownloadFolderPath();
        if (downloadsPath != null)
            items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_DOWNLOADS, downloadsPath), getString(R.string.downloads), R.raw.ic_file_download_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_BOOKMARKS, StorageUtils.BOOKMARKS_FILSE), getResources().getString(R.string.bookmarks), R.raw.ic_bookmark_24px));
        items.add(new NavigationItem(getString(R.string.media), NavigationAdapter.TYPE_DIVIDER));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_PHOTOS), getResources().getString(R.string.images), R.raw.ic_library_image_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_VIDEO), getResources().getString(R.string.videos), R.raw.ic_video_library_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_MUSIC), getResources().getString(R.string.music), R.raw.ic_library_music_24px));
        items.add(new NavigationItem(getString(R.string.clouds), NavigationAdapter.TYPE_DIVIDER));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_ONE_DRIVE), "One drive", R.raw.ic_cloud_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_DROP_BOX), "Drop box", R.raw.ic_cloud_24px));
//        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_GOOGLE_DRIVE), "Google drive", R.raw.ic_cloud_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_YANDEX_DRIVE), "Yandex drive", R.raw.ic_cloud_24px));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_BOX_DRIVE), "Box drive", R.raw.ic_cloud_24px));
        items.add(new NavigationItem(NavigationAdapter.TYPE_DIVIDER));
        items.add(new NavigationItem(new Main.TypeHolder(Main.TYPE_SETTINGS), getString(R.string.setting), R.raw.ic_settings_24px));
        items.add(new NavigationItem(NavigationAdapter.TYPE_SPACE));
        return items.toArray(new NavigationItem[items.size()]);
    }

}
