package maestro.filemanager.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.os.FileObserver;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.files.MArchiveEntryFile;
import maestro.filemanager.files.MFile;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.ui.base.BaseFileListFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.ui.media.ImageViewerFragment;
import maestro.filemanager.ui.media.VideoViewFragment;
import maestro.filemanager.utils.*;
import maestro.support.v1.menu.MDropDownMenu;
import maestro.support.v1.svg.SVGHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 9/11/14.
 */
public class FilesListFragment extends BaseFileListFragment<MFile> implements FolderTracker.OnFolderTrackerEvent, IStorageInfoGetter {

    public static final String TAG = FilesListFragment.class.getSimpleName();

    private static final String PARAM_PATH = "param_path";

    private String rootPath;
    private UniqAdapter mAdapter;
    private FolderTracker mTracker;
    private int loaderId;
    private boolean canNotify;

    public static final FilesListFragment makeInstance(String path) {
        FilesListFragment fragment = new FilesListFragment();
        Bundle args = new Bundle(1);
        args.putString(PARAM_PATH, path);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootPath = getArguments().getString(PARAM_PATH);
        loaderId = TAG.hashCode() + rootPath.hashCode();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (main != null) {
            main.setStorageInfoGetter(this);
        }
    }

    @Override
    public int getLoaderId() {
        return loaderId;
    }

    @Override
    public Loader getSearchLoader(String query) {
        return new SearchHelper.StorageFileSearcher(getActivity(), new SearchHelper.SearchParams().path(getLastHistory().Path).query(query));
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        prepareNavigation();
        animateLoadStart();
        if (mTracker != null) {
            mTracker.stopWatching();
        }
        return new Loaders.DirectoryFilesLoader(getActivity(), getLastHistory().Path);
    }

    @Override
    public BaseUpdateAdapter buildAdapter(boolean search) {
        if (search) {
            return new UniqAdapter(this, getSpanCount());
        }
        return mAdapter = new UniqAdapter(this, getSpanCount());
    }

    @Override
    public HistoryWrite getRootHistory() {
        StorageUtils.StorageInfo info = StorageUtils.getInfo(rootPath);
        if (info != null) {
            return new HistoryWrite(info.getDisplayName(getActivity()), rootPath);
        } else if (rootPath.equals("/")) {
            return new HistoryWrite(getString(R.string.root), rootPath);
        } else if (rootPath.equals(StorageUtils.BOOKMARKS_FILSE)) {
            return new HistoryWrite(getString(R.string.bookmarks), rootPath);
        }
        return new HistoryWrite(rootPath.substring(rootPath.lastIndexOf("/") + 1), rootPath);
    }

    @Override
    public boolean isEventAllow(GlobalCache.ActionObject actionObject) {
        return true;
    }

    @Override
    public void onDataLoadFinished(Loader<ArrayList<MFile>> loader, ArrayList<MFile> mFiles) {
        applyItems(mFiles);
        if (!isSilentLoading()) {
            animateLoadFinished(null, mAdapter.haveItems());
        }
        setActionButtonVisibility(!Utils.match(((Loaders.DirectoryFilesLoader) loader).getPath(), Utils.ARCHIVE_PATTERN));
    }

    private void applyItems(ArrayList<MFile> files) {
        mTracker = new FolderTracker(getLastHistory().Path, FilesListFragment.this, FileObserver.ALL_EVENTS);
        mTracker.startWatching();
        mAdapter.update((ArrayList) files);
        ensureItemPosition();
    }

    @Override
    public void onDetach() {
        if (mTracker != null) {
            mTracker.stopWatching();
        }
        super.onDetach();
    }

    @Override
    public void onItemClick(MFile file) {
        hideSearchView();
        if (file.isFolder() || Utils.match(file.getPath(), Utils.ARCHIVE_PATTERN)) {
            load(new HistoryWrite(file.getTitle(), file.getPath()));
        } else if (Utils.match(file.getExtension(), Utils.IMAGE_PATTERN)) {
            List<Object> files = mAdapter.collectFiles(Utils.IMAGE_PATTERN);
            ImageViewerFragment.makeInstance(files.indexOf(file), files).show(getChildFragmentManager(), ImageViewerFragment.TAG);
        } else if (Utils.match(file.getExtension(), Utils.VIDEO_PATTERN)) {
            if (file instanceof MArchiveEntryFile) {
                //TODO: unpack before load
            } else {
                VideoViewFragment.makeInstance(file.getPath()).show(getFragmentManager(), VideoViewFragment.TAG);
            }
        } else if (file instanceof MJavaFile) {
            Utils.processClick(getActivity(), (MJavaFile) file);
        }
    }

    @Override
    public void onScrollStateChange(RecyclerView recyclerView, int scrollState) {
        super.onScrollStateChange(recyclerView, scrollState);
        if (scrollState != RecyclerView.SCROLL_STATE_IDLE) {
            canNotify = false;
        } else {
            canNotify = true;
        }
    }

    @Override
    public void onFolderTrackerEvent(FolderTracker.FOLDER_TRACKER_EVENT event, String path) {
        Log.e(TAG, "onFolderTrackerEvent: " + event + ", path: " + path);
        if (event == FolderTracker.FOLDER_TRACKER_EVENT.CREATE || event == FolderTracker.FOLDER_TRACKER_EVENT.DELETE) {
            loadSilent();
        }
    }

    @Override
    public boolean containsFile(File file) {
        return mAdapter != null && mAdapter.contains(file);
    }

    @Override
    public boolean canNotify() {
        return canNotify;
    }

    @Override
    public void onRenameConfirm(FileNameInputDialog.TYPE type, String text, Object attachedObject) {
        if (getActivity() != null)
            if (type == FileNameInputDialog.TYPE.CREATE_FILE) {
                create(text, false);
            } else if (type == FileNameInputDialog.TYPE.CREATE_FOLDER) {
                create(text, true);
            } else if (type == FileNameInputDialog.TYPE.RENAME) {
                if (attachedObject instanceof MFile) {
                    ((MFile) attachedObject).rename(getActivity(), text);
                }
            }
    }

    @Override
    public void onConfirmEvent(ConfirmDialog.TYPE type, Object object) {
        if (type == ConfirmDialog.TYPE.DELETE_FILE) {
            boolean delete = ((MFile) object).delete(getActivity());
            if (delete) {
                ToastHelper.show(R.string.file_delete_seccessfuly, false);
            } else {
                ToastHelper.show(R.string.cant_delete_file, true);
            }
        }
    }

    private final void create(String name, boolean folder) {
        File file = new File(getLastHistory().Path, name);
        try {
            if (folder ? file.mkdir() : file.createNewFile()) {
                ToastHelper.show(R.string.file_created, false);
            } else {
                ToastHelper.show(R.string.cant_create_file, true);
            }
        } catch (IOException e) {
            e.printStackTrace();
            ToastHelper.show(R.string.cant_create_file, true);
        }
    }

    @Override
    public void onItemClick(View v, MDropDownMenu.MDropItem dropItem) {
        if (dropItem.getId() == ID_CREATE_FILE) {
            FileNameInputDialog.makeInstance(FileNameInputDialog.TYPE.CREATE_FILE, getString(R.string.file_name), null, null)
                    .show(getChildFragmentManager(), FileNameInputDialog.TAG);
        } else if (dropItem.getId() == ID_CREATE_FOLDER) {
            FileNameInputDialog.makeInstance(FileNameInputDialog.TYPE.CREATE_FOLDER, getString(R.string.folder_name), null, null)
                    .show(getChildFragmentManager(), FileNameInputDialog.TAG);
        }

    }

    @Override
    public void prepareInfo(TextView userNamePlaceHolder, TextView sizePlaceHolder) {
        final HistoryWrite root = getRootHistory();
        final File rootFile = new File(root.Path);
        final long usedSpace = rootFile.getUsableSpace();
        final long totalSpace = rootFile.getTotalSpace() + usedSpace;
        final String usedSize = Utils.readableFileSize(usedSpace);
        final String totalSize = Utils.readableFileSize(totalSpace);
        userNamePlaceHolder.setText(root.Title);
        if (TextUtils.isEmpty(usedSize) || TextUtils.isEmpty(totalSize) || usedSize.equals("null") || totalSize.equals("null")) {
            sizePlaceHolder.setVisibility(View.GONE);
        } else {
            sizePlaceHolder.setVisibility(View.VISIBLE);
            sizePlaceHolder.setText(Utils.readableFileSize(usedSpace) + " / " + Utils.readableFileSize(totalSpace));
        }
    }

    @Override
    public void prepareIcon(ImageView placeHolder) {
        placeHolder.setVisibility(View.VISIBLE);
        getSvgHolder().applySVG(placeHolder, StorageUtils.isDownloadFolder(getRootHistory().Path) ? R.raw.ic_file_download_24px
                : StorageUtils.isBookmarksFolder(rootPath) ? R.raw.ic_bookmark_24px : R.raw.ic_folder_24px, Color.WHITE, SVGHelper.DPI * 1.5f);
    }

    @Override
    public void cancel() {

    }


    @Override
    public void onGlobalCacheEvent(GlobalCache.ActionObject object) {
        if (isEventAllow(object)) {
            if (object.Action == GlobalCache.OBJECT_ACTION.ZIP) {
                MFile file = (MFile) object.Obj;
                StringBuilder builder = new StringBuilder();
                if (file.getExtension() != null) {
                    builder.append(file.getTitle().substring(0, file.getTitle().lastIndexOf(".")));
                } else {
                    builder.append(file.getTitle());
                }
                builder.append(".zip");
                PathSelectDialog.makeInstance(file.getPath(), builder.toString(), object.Action)
                        .show(getChildFragmentManager(), PathSelectDialog.TAG);
            } else {
                object.doAction(getLastHistory().Path, null);
            }
        }
    }

}
