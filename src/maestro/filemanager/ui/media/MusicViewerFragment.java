package maestro.filemanager.ui.media;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMBlurMaker;
import com.dream.android.mim.MIMUtils;
import com.dream.android.mim.RecyclingImageView;
import com.msoft.android.helpers.MFormatter;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.filemanager.FileManagerApplication;
import maestro.filemanager.R;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.Utils;
import maestro.filemanager.utils.theme.ThemeHolder;
import maestro.support.v1.svg.SVGHelper;

import java.io.File;
import java.util.List;

/**
 * Created by Artyom on 4/30/2015.
 */
public class MusicViewerFragment extends DialogFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = MusicViewerFragment.class.getSimpleName();

    private final static String PARAM_ALBUM = "album";

    public static MusicViewerFragment makeInstance(Album album) {
        MusicViewerFragment fragment = new MusicViewerFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ALBUM, album);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setWindowAnimations(R.style.FragmentAnimation);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getResources().getBoolean(R.bool.is7inch) && !getResources().getBoolean(R.bool.is10inch)) {
            setStyle(STYLE_NORMAL, R.style.FM_Theme_Overlay);
        }
    }

    private ListView mList;
    private RecyclingImageView mBackgroundImage;
    private ColorDrawable mToolbarBackground;
    private View mBarView;
    private Album mAlbum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.music_viewer_fragment, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setOnItemClickListener(this);
        mBarView = v.findViewById(R.id.bar_view);
        ImageView mIcon = (ImageView) v.findViewById(R.id.icon);
        mIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getSvgHolder().applySVG(mIcon, R.raw.ic_close, Color.WHITE);
        mToolbarBackground = new ColorDrawable(ThemeHolder.getPrimaryColor());
        mToolbarBackground.setAlpha(0);
        mBarView.setBackgroundDrawable(mToolbarBackground);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAlbum = getArguments().getParcelable(PARAM_ALBUM);

        final View mHeaderView = View.inflate(getActivity(), R.layout.music_viewer_header_view, null);
        final TextView mTitle = (TextView) mHeaderView.findViewById(R.id.title);
        final TextView mAuthor = (TextView) mHeaderView.findViewById(R.id.author);
        final TextView mAdditional = (TextView) mHeaderView.findViewById(R.id.additional);

        mBackgroundImage = (RecyclingImageView) mHeaderView.findViewById(R.id.background_image);
        mBackgroundImage.setLoadObject(MIM.by(Utils.MIM_ICON_KEY).to(mBackgroundImage, mAlbum.Title + "_big").object(mAlbum).postMaker(new MIMBlurMaker()));

        mTitle.setTypeface(Typefacer.rMedium);
        mAuthor.setTypeface(Typefacer.rMedium);
        mAdditional.setTypeface(Typefacer.rMedium);

        mTitle.setText(mAlbum.Title);
        mAuthor.setText(mAlbum.author);

        mList.addHeaderView(mHeaderView);
        mList.setAdapter(new MusicItemAdapter(getActivity(), MediaStoreHelper.getSongs(getActivity(), mAlbum.Id)));

        final int bigCoverHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 360, getResources().getDisplayMetrics());

        mList.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mHeaderView != null && mHeaderView.getTop() > -bigCoverHeight) {
                    mBackgroundImage.setTranslationY(mHeaderView.getTop() * -1 * 0.5f);
                    float percent = (float) mHeaderView.getTop() * -1 / bigCoverHeight;
                    if (percent > 0.9) {
                        percent = 1.0f;
                    }
                    mToolbarBackground.setAlpha((int) (255 * percent));
                }else {
                    mToolbarBackground.setAlpha(255);
                }
            }
        });

        mAdditional.setText(new StringBuilder().append(mList.getCount()).append(" ").append(getString(R.string.tracks)));

    }

    private SVGHelper.SVGHolder getSvgHolder() {
        return ((FileManagerApplication) getActivity().getApplication()).getSvgHolder();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Song song = (Song) parent.getItemAtPosition(position);
        if (song != null) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                File file = new File(song.Path);
                intent.setDataAndType(Uri.fromFile(file), "audio/*");
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.processClick(getActivity(), new MJavaFile(new File(song.Path)));
            }
        }
    }

    public final class MusicItemAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private List<Song> mSongs;

        public MusicItemAdapter(Context context, List<Song> songs) {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
            mSongs = songs;
        }

        @Override
        public int getCount() {
            return mSongs != null ? mSongs.size() : 0;
        }

        @Override
        public Song getItem(int position) {
            return mSongs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mSongs.get(position).Id;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            Holder holder;
            if (v == null) {
                v = mInflater.inflate(R.layout.music_viewer_item_view, null);
                holder = new Holder(v);
            } else {
                holder = (Holder) v.getTag();
            }

            Song song = mSongs.get(position);
            holder.Title.setText(song.Title);
            holder.Duration.setText(MFormatter.formatTimeFromMillis(song.Duration));

            return v;
        }

        class Holder {

            TextView Title;
            TextView Duration;
            ImageButton MoreOptions;

            Holder(View v) {
                Title = (TextView) v.findViewById(R.id.title);
                Duration = (TextView) v.findViewById(R.id.additional);
                MoreOptions = (ImageButton) v.findViewById(R.id.more_options_button);

                Title.setTypeface(Typefacer.rRegular);
                Duration.setTypeface(Typefacer.rRegular);

                getSvgHolder().applySVG(MoreOptions, R.raw.more, Color.parseColor("#aaaaaa"));
                v.setTag(this);
            }
        }

        private SVGHelper.SVGHolder getSvgHolder() {
            if (mContext instanceof Activity) {
                return ((FileManagerApplication) ((Activity) mContext).getApplication()).getSvgHolder();
            }
            return null;
        }

    }

}
