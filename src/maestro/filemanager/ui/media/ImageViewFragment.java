package maestro.filemanager.ui.media;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMUtils;
import com.dream.android.mim.RecyclingImageView;
import maestro.filemanager.R;
import maestro.filemanager.utils.Utils;

/**
 * Created by Artyom on 3/19/2015.
 */
public class ImageViewFragment extends DialogFragment {

    public static final String TAG = ImageViewFragment.class.getSimpleName();

    private static final String PARAM_POSITION = "param_position";

    public static ImageViewFragment makeInstance(int position) {
        ImageViewFragment fragment = new ImageViewFragment();
        Bundle args = new Bundle(1);
        args.putInt(PARAM_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclingImageView imageView;
    private ProgressBar mProgressBar;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.FM_Theme_Overlay);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.image_view_fragment, null);
        imageView = (RecyclingImageView) v.findViewById(R.id.image);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        return v;
    }

    @Override
    public void onDestroy() {
        ImageLoadObject.cancel(imageView);
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadImage(false);
    }

    private void loadImage(final boolean good) {
        final Object object = getImageObject(getArguments().getInt(PARAM_POSITION));
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        MIM.by(Utils.MIM_ICON_KEY).to(imageView, Utils.getKey(object) + "_full_" + good).size(metrics.widthPixels / (good ? 2 : 6), metrics.heightPixels / (good ? 2 : 6))
                .animationEnable(!good)
                .cleanPreviouse(!good)
                .object(object).analyze(false).listener(new ImageLoadObject.OnImageLoadEventListener() {
            @Override
            public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
                if (event == IMAGE_LOAD_EVENT.FINISH) {
                    mProgressBar.animate().alpha(0).start();
                    if (loadObject.getAnalyzedColors() != null && loadObject.getAnalyzedColors().length > 0) {
                        final ColorDrawable drawable = imageView.getBackground() != null ? (ColorDrawable) imageView.getBackground() : new ColorDrawable();
                        final int startColor = drawable.getColor();
                        final int finalColor = loadObject.getAnalyzedColors()[loadObject.getAnalyzedColors().length - 1];
                        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
                        animator.setStartDelay(150);
                        animator.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                                if (imageView.getBackground() == null) {
                                    imageView.setBackgroundDrawable(drawable);
                                }
                            }
                        });
                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                ((ColorDrawable) imageView.getBackground()).setColor(MIMUtils.blendColors(startColor, finalColor, 1f - valueAnimator.getAnimatedFraction()));
                            }
                        });
                        animator.start();
                    }
                    if (loadObject.getWidth() < metrics.widthPixels / 2) {
                        loadImage(true);
                    }
                }
            }
        }).async();
    }

    private Object getImageObject(int position) {
        if (getParentFragment() instanceof ImageViewerFragment) {
            return ((ImageViewerFragment)getParentFragment()).getImageObject(position);
        }
        return null;
    }

}
