package maestro.filemanager.ui.media;

import android.app.Dialog;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.VideoView;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;

/**
 * Created by artyom on 4/23/15.
 */
public class VideoViewFragment extends BaseFragment implements View.OnClickListener, OnSeekBarChangeListener {

    public static final String TAG = VideoViewFragment.class.getSimpleName();

    public static final String PARAM_PATH = "param_path";

    public static final VideoViewFragment makeInstance(String path) {
        VideoViewFragment fragment = new VideoViewFragment();
        Bundle args = new Bundle(1);
        args.putString(PARAM_PATH, path);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.FM_Theme_Overlay);
    }

    private VideoView mVideoView;
    private SeekBar mSeekBar;
    private ImageView mPrev, mToggle, mNext;
    private boolean isSeekBarTouched = false;

    private static final int MSG_HIDE_NAVIGATION = 1;
    private static final int MSG_UPDATE_PROGRESS = 2;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_HIDE_NAVIGATION:

                    break;
                case MSG_UPDATE_PROGRESS:
                    if (mVideoView != null && mSeekBar != null && !isSeekBarTouched) {
                        mSeekBar.setProgress(mVideoView.getCurrentPosition());
                        if (mVideoView.isPlaying()) {
                            notifyProgress(true);
                        }
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.video_view_fragment, null);
        mVideoView = (VideoView) v.findViewById(R.id.video_view);
        mSeekBar = (SeekBar) v.findViewById(R.id.seek_bar);
        mPrev = (ImageView) v.findViewById(R.id.prev);
        mToggle = (ImageView) v.findViewById(R.id.play_toggle);
        mNext = (ImageView) v.findViewById(R.id.next);
        mSeekBar.setOnSeekBarChangeListener(this);

        getSvgHolder().applySVG(mPrev, R.raw.ic_rewind, Color.WHITE);
        getSvgHolder().applySVG(mNext, R.raw.ic_forward, Color.WHITE);
        getSvgHolder().applySVG(mToggle, R.raw.ic_pause, Color.WHITE);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mVideoView.setVideoPath(getArguments().getString(PARAM_PATH));
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mSeekBar.setMax(mVideoView.getDuration());
                mVideoView.start();
                notifyProgress(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        final int step = (int) (mVideoView.getDuration() * 0.05);
        switch (v.getId()) {
            case R.id.prev:
                if (mVideoView.getCurrentPosition() > step) {
                    mVideoView.seekTo(mVideoView.getCurrentPosition() - step);
                } else {
                    mVideoView.seekTo(0);
                }
                notifyProgress(false);
                break;
            case R.id.next:
                if (mVideoView.getCurrentPosition() + step < mVideoView.getDuration()) {
                    mVideoView.seekTo(mVideoView.getCurrentPosition() + step);
                } else {
                    mVideoView.seekTo(mVideoView.getDuration());
                }
                notifyProgress(false);
                break;
            case R.id.play_toggle:
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                } else {
                    mVideoView.start();
                }
                break;
        }
    }

    private void notifyProgress(boolean delayed) {
        if (!delayed && mHandler.hasMessages(MSG_UPDATE_PROGRESS))
            mHandler.removeMessages(MSG_UPDATE_PROGRESS);
        if (delayed)
            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, 1000);
        else
            mHandler.sendEmptyMessage(MSG_UPDATE_PROGRESS);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (isSeekBarTouched)
            mVideoView.seekTo(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeekBarTouched = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeekBarTouched = false;
        notifyProgress(false);
    }
}
