package maestro.filemanager.ui.media;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;

import java.util.List;

/**
 * Created by artyom on 3.4.15.
 */
public class ImageViewerFragment extends BaseFragment {

    public static final String TAG = ImageViewerFragment.class.getSimpleName();

    private static final String PARAM_POSITION = "param_position";
    private ViewPager pager;
    private Toolbar mToolbar;
    private static List<Object> mSharedObjects;

    public static ImageViewerFragment makeInstance(int position, List<Object> objects) {
        ImageViewerFragment fragment = new ImageViewerFragment();
        Bundle args = new Bundle(1);
        args.putInt(PARAM_POSITION, position);
        mSharedObjects = objects;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.FM_Theme_Overlay);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.image_viewer_fragment, null);
        pager = (ViewPager) v.findViewById(R.id.pager);
        pager.setOffscreenPageLimit(4);

        mToolbar = (Toolbar) v.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(getSvgHolder().getDrawable(R.raw.ic_back, Color.WHITE));
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int startPosition = getArguments().getInt(PARAM_POSITION);
        pager.setAdapter(new ViewerAdapter(getChildFragmentManager()));
        pager.setCurrentItem(startPosition, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSharedObjects = null;
    }

    public final Object getImageObject(int position){
        return mSharedObjects.get(position);
    }

    private class ViewerAdapter extends FragmentStatePagerAdapter {

        public ViewerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return ImageViewFragment.makeInstance(i);
        }

        @Override
        public int getCount() {
            return mSharedObjects != null ? mSharedObjects.size() : 0;
        }
    }

}
