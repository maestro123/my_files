package maestro.filemanager.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.Loader;
import android.view.View;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.OneDriveManager;
import maestro.filemanager.files.MJavaFile;
import maestro.filemanager.files.onedrive.BaseOneDriveObject;
import maestro.filemanager.files.onedrive.OneDriveAlbum;
import maestro.filemanager.files.onedrive.OneDriveFolder;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.Utils;
import maestro.support.v1.menu.MDropDownMenu;

import java.io.File;

/**
 * Created by Artyom on 3/18/2015.
 */
public class OneDriveFragment extends BaseDriveFragment<BaseOneDriveObject> {

    public static String TAG = OneDriveFragment.class.getSimpleName();


    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public Loader getSearchLoader(String query) {
        return null;
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        super.getBaseLoader(id, args);
        return new Loaders.OneDriveLoader(getActivity(), getLastHistory().Path + "/files");
    }

    @Override
    public BaseDriveManager getDriveManager() {
        return OneDriveManager.getInstance();
    }

    @Override
    public String getKey() {
        return TAG;
    }

}
