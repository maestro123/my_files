package maestro.filemanager.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.files.MFile;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.utils.Typefacer;
import maestro.filemanager.utils.Utils;

/**
 * Created by Artyom on 25.09.2014.
 */
public class FileInfoDialog extends BaseFragment {

    public static final String TAG = FileInfoDialog.class.getSimpleName();

    private static final String PARAM_ITEM = "item";

    public static FileInfoDialog makeInstance(MFile file) {
        FileInfoDialog dialog = new FileInfoDialog();
        Bundle args = new Bundle();
        args.putParcelable(PARAM_ITEM, file);
        dialog.setArguments(args);
        return dialog;
    }

    private ImageView imgIcon;
    private TextView txtTitle, txtSize;
    private MFile mFile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.file_info_dialog_view, null);
        imgIcon = (ImageView) view.findViewById(R.id.image);
        txtTitle = (TextView) view.findViewById(R.id.title);
        txtSize = (TextView) view.findViewById(R.id.additional);
        txtTitle.setTypeface(Typefacer.rRegular);
        txtSize.setTypeface(Typefacer.rLight);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFile = getArguments().getParcelable(PARAM_ITEM);
        getSvgHolder().applySVG(imgIcon, R.raw.ic_file_24px, Utils.getIconColor(mFile));
        txtTitle.setText(mFile.getTitle());
        txtSize.setText(mFile.getReadableSize());
    }
}
