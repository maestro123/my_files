package maestro.filemanager.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import maestro.filemanager.R;
import maestro.filemanager.ui.base.BaseFragment;
import maestro.filemanager.utils.theme.ThemeHolder;

/**
 * Created by artyom on 4/10/15.
 */
public class ConfirmDialog extends BaseFragment implements View.OnClickListener {

    public static final String TAG = ConfirmDialog.class.getSimpleName();

    private static final String PARAM_TYPE = "type";
    private static final String PARAM_MESSAGE = "msg";
    private static final String PARAM_CONFIRM_TEXT = "confirm_text";
    private static final String PARAM_CANCEL_TEXT = "cancel_text";

    private static Object mSharedObject;

    public static ConfirmDialog makeInstance(TYPE type, String message, Object object) {
        ConfirmDialog dialog = new ConfirmDialog();
        Bundle args = new Bundle(2);
        args.putSerializable(PARAM_TYPE, type);
        args.putString(PARAM_MESSAGE, message);
        mSharedObject = object;
        dialog.setArguments(args);
        return dialog;
    }

    public static ConfirmDialog makeInstance(TYPE type, String message, String confirmText, String cancelText, Object object) {
        ConfirmDialog dialog = makeInstance(type, message, object);
        dialog.getArguments().putString(PARAM_CONFIRM_TEXT, confirmText);
        dialog.getArguments().putString(PARAM_CANCEL_TEXT, cancelText);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (mListener != null) {
                    mListener.onConfirmEvent((TYPE) getArguments().getSerializable(PARAM_TYPE), mSharedObject);
                }
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    public enum TYPE {
        DELETE_FILE
    }

    private OnConfirmDialogEventListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.confirm_dialog_view, null);
        final TextView title = (TextView) view.findViewById(R.id.title);
        final Button cancel = (Button) view.findViewById(R.id.btn_cancel);
        final Button ok = (Button) view.findViewById(R.id.btn_ok);

        if (getArguments().containsKey(PARAM_CONFIRM_TEXT)) {
            ok.setText(getArguments().getString(PARAM_CONFIRM_TEXT));
        }
        if (getArguments().containsKey(PARAM_CANCEL_TEXT)) {
            cancel.setText(getArguments().getString(PARAM_CANCEL_TEXT));
        }
        title.setText(getArguments().getString(PARAM_MESSAGE));

        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);

        ThemeHolder.applyDialogButtons(view);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSharedObject = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mListener == null && getParentFragment() != null && getParentFragment() instanceof OnConfirmDialogEventListener) {
            mListener = (OnConfirmDialogEventListener) getParentFragment();
        }
    }

    public void setOnConfirmDialogEventListener(OnConfirmDialogEventListener listener) {
        mListener = listener;
    }

    public interface OnConfirmDialogEventListener {

        public void onConfirmEvent(TYPE type, Object object);

    }

}
