package maestro.filemanager.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMUtils;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Image;
import com.msoft.android.mplayer.lib.models.MediaBucket;
import maestro.filemanager.R;
import maestro.filemanager.Settings;
import maestro.filemanager.ui.base.BaseMediaFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.ui.media.ImageViewerFragment;
import maestro.filemanager.utils.Loaders;
import maestro.filemanager.utils.SpaceItemDecoration;
import maestro.filemanager.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 12/25/14.
 */
public class MediaImageFragment extends BaseMediaFragment implements LoaderManager.LoaderCallbacks<ArrayList<MediaBucket>>,
        BaseUpdateAdapter.OnItemClickActionListener<MediaBucket> {

    public static final String TAG = MediaImageFragment.class.getSimpleName();

    private RecyclerView mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private ImageAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.media_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setHasFixedSize(true);
        mList.addItemDecoration(new SpaceItemDecoration(computeSpanCount(), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()),
                getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material), false));
        mLayoutManager = new GridLayoutManager(getActivity(), computeSpanCount());
        mList.setLayoutManager(mLayoutManager);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new ImageAdapter(getActivity(), computeSpanCount()));
        mAdapter.setOnItemClickAction(new BaseUpdateAdapter.OnItemClickActionListener() {
            @Override
            public void onItemClick(Object item) {
                MediaBucket mediaBucket = (MediaBucket) item;
                List<Image> imageList = MediaStoreHelper.getImages(getActivity(), mediaBucket.Id);
                ImageViewerFragment.makeInstance(0, (List) imageList).show(getChildFragmentManager(), ImageViewerFragment.TAG);
            }

            @Override
            public boolean onItemLongClick(Object item) {
                return false;
            }
        });
        load();
    }

    public int computeSpanCount() {
        final boolean is7inch = getResources().getBoolean(R.bool.is7inch);
        final boolean is10inch = getResources().getBoolean(R.bool.is10inch);
        final boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        return !is7inch && !is10inch && !isLandscape ? 2 : is7inch ? isLandscape ? 5 : 3 : is10inch ? isLandscape ? 6 : 4 : 2;
    }

    @Override
    public void load() {
        getLoaderManager().initLoader(TAG.hashCode(), null, this);
    }

    @Override
    public Loader<ArrayList<MediaBucket>> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.ImageBucketsLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MediaBucket>> loader, ArrayList<MediaBucket> mediaBuckets) {
        mAdapter.update(mediaBuckets);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MediaBucket>> loader) {

    }

    @Override
    public void onSettingChange(String pref) {
        super.onSettingChange(pref);
        if (Settings.isSortPreference(pref)) {
            load();
        }
    }

    @Override
    public void onItemClick(MediaBucket item) {

    }

    @Override
    public boolean onItemLongClick(MediaBucket item) {
        return false;
    }

    public final class ImageAdapter extends BaseUpdateAdapter<MediaBucket, ImageAdapter.Holder> {

        private int size;

        public ImageAdapter(Context context, int spanCount) {
            super(context, spanCount);
            size = getResources().getDisplayMetrics().widthPixels / spanCount;
        }

        @Override
        public Holder createItemHolder(LayoutInflater inflater) {
            View v = inflater.inflate(R.layout.album_item_view, null);
            Holder holder = new Holder(v);
            holder.Image.getLayoutParams().width = size;
            holder.Image.getLayoutParams().height = size;
            return holder;
        }

        @Override
        public void onBindItemHolder(final Holder holder, MediaBucket item, int position) {
            holder.AlbumItemParent.setBackgroundColor(Color.TRANSPARENT);

            holder.Title.setText(item.Title);
            holder.Author.setText(item.getChildCount() + " "+ getString(R.string.pictures));
            MIM.by(Utils.MIM_ICON_KEY).to(holder.Image, "mediaBucket:" + item.Title).object(item)
                    .analyze(true).listener(new ImageLoadObject.OnImageLoadEventListener() {
                @Override
                public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
                    if (event == ImageLoadObject.OnImageLoadEventListener.IMAGE_LOAD_EVENT.FINISH) {
                        if (loadObject.getAnalyzedColors() != null && loadObject.getAnalyzedColors().length > 0) {
                            ColorDrawable drawable = (ColorDrawable) holder.AlbumItemParent.getBackground();
                            if (drawable == null) {
                                drawable = new ColorDrawable();
                                holder.AlbumItemParent.setBackgroundDrawable(drawable);
                            }
                            drawable.setColor(MIMUtils.blendColors(loadObject.getAnalyzedColors()[loadObject.getAnalyzedColors().length - 1],
                                    Color.BLACK, 0.75f));
                            ObjectAnimator.ofInt(drawable, "alpha", 0, 255).start();
                        }
                    }
                }
            }).size(size, size).async();

        }

        @Override
        public void ensureCheckMask(RecyclerView.ViewHolder holder, boolean inSelectionMode, boolean checked) {

        }

        class Holder extends RecyclerView.ViewHolder {

            ImageView Image;
            TextView Title;
            TextView Author;
            LinearLayout AlbumItemParent;

            public Holder(View itemView) {
                super(itemView);
                Image = (ImageView) itemView.findViewById(R.id.image);
                Title = (TextView) itemView.findViewById(R.id.title);
                Author = (TextView) itemView.findViewById(R.id.author);
                AlbumItemParent = (LinearLayout) itemView.findViewById(R.id.album_item_parent);
            }

        }

    }

}
