package maestro.filemanager.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.Loader;
import android.view.View;
import maestro.filemanager.Settings;
import maestro.filemanager.drives.BaseDriveManager;
import maestro.filemanager.drives.YandexDriveManager;
import maestro.filemanager.drives.yandexdrive.ListItem;
import maestro.filemanager.ui.base.BaseDriveFragment;
import maestro.filemanager.ui.base.BaseUpdateAdapter;
import maestro.filemanager.utils.Loaders;
import maestro.support.v1.menu.MDropDownMenu;

import java.io.File;

/**
 * Created by Artyom on 3/18/2015.
 */
public class YandexDriveFragment extends BaseDriveFragment<ListItem> {

    public static String TAG = YandexDriveFragment.class.getSimpleName();

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public Loader getSearchLoader(String query) {
        return null;
    }

    @Override
    public Loader getBaseLoader(int id, Bundle args) {
        super.getBaseLoader(id, args);
        return new Loaders.YandexDriveLoader(getActivity(), getLastHistory().Path);
    }

    @Override
    public BaseDriveManager getDriveManager() {
        return YandexDriveManager.getInstance();
    }

    @Override
    public String getKey() {
        return TAG;
    }

}
