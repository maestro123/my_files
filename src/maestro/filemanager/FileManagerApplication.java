package maestro.filemanager;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.util.TypedValue;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMCirclePostMaker;
import com.dream.android.mim.MIMColorAnalyzer;
import com.dream.android.mim.MIMManager;
import maestro.filemanager.drives.data.DrivesDatabase;
import maestro.filemanager.drives.onedrive.LiveAuthClient;
import maestro.filemanager.drives.onedrive.LiveConnectClient;
import maestro.filemanager.drives.onedrive.LiveConnectSession;
import maestro.filemanager.utils.*;
import maestro.filemanager.utils.data.FileManagerDatabase;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by artyom on 9/11/14.
 */
public class FileManagerApplication extends Application {

    private LiveAuthClient mAuthClient;
    private LiveConnectClient mConnectClient;
    private LiveConnectSession mSession;
    private SVGHelper.SVGHolder svgHolder;

    @Override
    public void onCreate() {
        super.onCreate();
        SVGHelper.init(this);
        svgHolder = new SVGHelper.SVGHolder(getResources());
        new DrivesDatabase(this);
        new FileManagerDatabase(this);
        Settings.initialize(this);
        Typefacer.initialize(this);
        ToastHelper.initialize(this);

        final int iconSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56, getResources().getDisplayMetrics());
        MIMManager.getInstance().addMIM(Utils.MIM_ICON_KEY, new MIM(this).maker(new MIconMaker(this)).analyzer(new MIMColorAnalyzer() {
            @Override
            public Integer[] analyze(Bitmap bitmap) {
                Palette palette = Palette.generate(bitmap);
                Palette.Swatch swatch = palette.getVibrantSwatch();
                if (swatch == null)
                    swatch = palette.getDarkVibrantSwatch();
                return swatch != null ? new Integer[]{swatch.getRgb()} : super.analyze(bitmap);
            }
        }));
        MIMManager.getInstance().addMIM(Utils.MIM_STORAGE_ICON, new MIM(this).maker(new MStorageIconMaker(true)).size(iconSize, iconSize).postMaker(new MIMCirclePostMaker()));
    }

    public SVGHelper.SVGHolder getSvgHolder() {
        return svgHolder;
    }

    public LiveAuthClient getAuthClient() {
        return mAuthClient;
    }

    public LiveConnectClient getConnectClient() {
        return mConnectClient;
    }

    public LiveConnectSession getSession() {
        return mSession;
    }

    public void setAuthClient(LiveAuthClient authClient) {
        mAuthClient = authClient;
    }

    public void setConnectClient(LiveConnectClient connectClient) {
        mConnectClient = connectClient;
    }

    public void setSession(LiveConnectSession session) {
        mSession = session;
    }

}
