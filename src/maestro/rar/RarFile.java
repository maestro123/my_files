/*
 * ComicsReader is an Android application to read comics
 * Copyright (C) 2011-2015 Cedric OCHS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package maestro.rar;

import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RarFile {

    public static final String TAG = RarFile.class.getSimpleName();

    protected String mName;
    protected List<RarEntryFile> mEntries = new ArrayList<RarEntryFile>();
    protected static String mVersion;
    static protected boolean sLoaded = false;

    // implemented by libunrar-jni.so
    private static native String[] nativeGetEntries(String filename);

    private static native Object[] listEntries(String filename);

    private static native byte[] nativeGetData(String filename, String entry);

    private static native String nativeGetVersion();

    private static native void nativeTests();

    private static native void nativeInit();

    private static native void nativeDestroy();

    public static boolean isLoaded() {
        return sLoaded;
    }

    public static void destroy() {
        nativeDestroy();
    }

    public static String getVersion() {
        if (mVersion == null && sLoaded) {
            mVersion = nativeGetVersion();
            nativeTests();
        }
        return mVersion;
    }

    public RarFile(File file) throws IOException {
        if (file == null || !open(file.getAbsolutePath())) {
            throw new IOException();
        }
    }

    public RarFile(String filename) throws IOException {
        open(filename);
    }

    private boolean open(String filename) throws IOException {
        if (filename == null) {
            throw new IOException();
        }

        mName = filename;

        return true;
    }

    public void close() {
        mEntries = null;
        mName = null;
    }

    public List<RarEntryFile> list() {
        if (sLoaded && mEntries.isEmpty()) {
            RarEntryFile[] entries = (RarEntryFile[]) listEntries(mName);
            if (entries != null) {
                mEntries = Arrays.asList(entries);
            }
            for (RarEntryFile entry : entries) {
                Log.e(TAG, "entry:" + entry);
            }
        }
        return mEntries;
    }

    public byte[] getBytes(String entry) {
        if (!sLoaded) return null;

        try {
            return nativeGetData(mName, entry);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "Out of memory while getting file " + entry + " from " + mName);
        }

        return null;
    }

    public InputStream getInputStream(String entry) {
        return new ByteArrayInputStream(getBytes(entry));
    }

    public String getName() {
        return mName;
    }

    public int size() {
        return mEntries.size();
    }

    // load our native library
    static {
        try {
            System.loadLibrary("unrar-jni");
            sLoaded = true;
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "Unrar library can't be loaded, RAR support is disabled");
        }
    }
}
