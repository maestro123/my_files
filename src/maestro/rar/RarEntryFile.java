package maestro.rar;

public class RarEntryFile {

    private String name;
    private long packedSize;
    private long unpackedSize;
    private long timestamp;
    private boolean isDirectory;

    public RarEntryFile(String name, long packedSize, long unpackedSize, long timestamp, boolean isDirectory) {
        this.name = name;
        this.packedSize = packedSize;
        this.unpackedSize = unpackedSize;
        this.timestamp = timestamp;
        this.isDirectory = isDirectory;
    }

    public String getName() {
        return name;
    }

    public long getPackedSize() {
        return packedSize;
    }

    public long getUnpackedSize() {
        return unpackedSize;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    @Override
    public String toString() {
        return RarEntryFile.class.getSimpleName() + ": " + name + ", " + packedSize + ", " + unpackedSize + ", " + timestamp + ", " + isDirectory();
    }
}
